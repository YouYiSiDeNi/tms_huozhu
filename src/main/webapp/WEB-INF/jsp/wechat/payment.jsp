<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/8/22
  Time: 11:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>确认支付</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no, email=no" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/common.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/wechat/css/personalCenter.css"/>
    <script src="${pageContext.request.contextPath}/assets/wechat/lib/js/jquery-2.1.4.js"></script>

    <style>
        .weui-btn{
            position: relative;
            display: block;
            margin-left: auto;
            margin-right: auto;
            padding-left: 14px;
            padding-right: 14px;
            box-sizing: border-box;
            font-size: 18px;
            text-align: center;
            text-decoration: none;
            color: #FFFFFF;
            line-height: 2.55555556;
            border-radius: 5px;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            overflow: hidden;
        }
        .weui-btn_primary{
            background-color:#1AAD19;
            margin-top: 0.6rem;
            width: 7rem;

        }
        .page{
            padding:0 15px;
        }

    </style>
</head>
<body>
<div class="find_loginPwd_title header" style="background: #1AAD19;">
    <a href="javascript:history.go(-1)">
        <i class="icon"></i>
    </a>
    <span>微信支付小票</span>
</div>

<section class="find_loginPwd_wrap">
    <ul class="list">
        <li class="list_item">
            <label>
                <span>付款项：易航oTMS观看视频时长</span>
            </label>
        </li>
        <li class="list_item">
            <label>
                <span>支付金额：</span>
            </label>
        </li>
        <li class="list_item">
            <label>
                <span>订单号：</span>
            </label>
        </li>
        <li class="list_item">
            <label>
                <span>如有疑问请联系客服:400123456</span>
            </label>
        </li>
    </ul>
    <button class="weui-btn weui-btn_primary" onclick="callpay()">确认付款</button>
</section>

</body>

<script type="text/javascript">
    function callpay(){
        WeixinJSBridge.invoke('getBrandWCPayRequest',{
            "appId" : "<%=request.getAttribute("appId")%>","timeStamp" : "<%=request.getAttribute("timeStamp")%>", "nonceStr" : "<%=request.getAttribute("nonceStr")%>", "package" : "<%=request.getAttribute("package")%>","signType" : "MD5", "paySign" : "<%=request.getAttribute("paySign")%>"
        },function(res){
            WeixinJSBridge.log(res.err_msg);
// 				alert(res.err_code + res.err_desc + res.err_msg);
            if(res.err_msg == "get_brand_wcpay_request:ok"){
                alert("微信支付成功!");
                window.location.href='orderList.html';
            }else if(res.err_msg == "get_brand_wcpay_request:cancel"){
                alert("用户取消支付!");
            }else{
                alert("支付失败!");
            }
        })
    }
</script>

</html>
