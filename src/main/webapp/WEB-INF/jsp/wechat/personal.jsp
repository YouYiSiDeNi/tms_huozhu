<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/8/22
  Time: 11:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>用户中心</title>
    <meta content="用户中心" name="Keywords">
    <meta content="用户中心" name="description">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no, email=no" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/common.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/wechat/css/index.css?v=1.2.1"/>
    <script src="${pageContext.request.contextPath}/assets/wechat/lib/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<!--页面头部-->
<div class="my_header">

    <div class="my_headercenter">
        <!--<p class="account_type"></p>
        <p style="font-size: 0.36rem;">个人中心</p>
        <p class="num"><span id="balance_num"></span><span id="balance_dec" class="num_deci"></span></p>-->
    </div>
    <div class="my_headerbottom">
        <div class="header-center" style="text-align: center;">
            <p style="font-size: 0.6rem;font-weight: 700;">皖彦思9988</p>
            <p style="font-size: 0.3rem;margin-top: 30px;">15955889669</p>
        </div>
    </div>
</div>
<!--页面主体内容部分-->
<div class="my_content">
    <div class="my_content_top">
        <img class="ct_bg" src="${pageContext.request.contextPath}/assets/wechat/img/ct_bg.png" alt="" />
        <div class="header-center" style="text-align: center;">
            <p style="position: relative;color: #727272;font-size: 0.4rem;">当前剩余：<b>5999</b>秒</p>
        </div>

    </div>
    <div class="my_content_bottom">
        <ul>
            <li class="item">
                <a href="password.do">
                    <div>
								<span class="icon_wrap">
									<i class="icon icon_tzjl"></i>
								</span>
                        <span class="item_detail">修改密码</span>
                        <i class="icon icon_zhankai"></i>
                    </div>
                </a>
            </li>
            <li class="item">
                <a href="help.do">
                    <div>
								<span class="icon_wrap">
									<i class="icon_tzmx"></i>
								</span>
                        <span class="item_detail">使用帮助</span>
                        <i class="icon icon_zhankai"></i>
                    </div>
                </a>
            </li>
            <li class="item">
                <a href="about2.do">
                    <div>
								<span class="icon_wrap">
									<i class="icon icon_jlyh"></i>
								</span>
                        <span class="item_detail">关于我们  </span>
                        <span class="couponNum"></span>
                        <i class="icon icon_zhankai"></i>
                    </div>
                </a>
            </li>
            <li class="item" id="addBank">
                <a href="tel:4001234567" >
                    <div>
								<span class="icon_wrap">
									<i class="icon icon_card"></i>
								</span>
                        <span class="item_detail">联系客服</span>
                        <i class="icon icon_zhankai"></i>
                    </div>
                </a>
            </li>

        </ul>
    </div>
    <div style="height: 1rem;">

    </div>
</div>
<!--	页面底部部分-->
<ul class="footer" style="background: #fff;">
    <li class="footer-item">
        <a href="index.do" class="">
            <i class="icon icon_index"></i>
            <p class="icon_title">首页</p>
        </a>
    </li>
    <li class="footer-item">
        <a href="watchRecord.do" class="">
            <i class="icon icon_invest"></i>
            <p class="icon_title">费用</p>
        </a>
    </li>

    <li class="footer-item">
        <a href="#" class="">
            <i class="icon icon_my active"></i>
            <p class="icon_title" style="color: #00a0e9">我的</p>
        </a>
    </li>
</ul>
</body>


</html>
