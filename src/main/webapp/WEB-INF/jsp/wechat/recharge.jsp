<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/8/22
  Time: 11:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>消息中心</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0"/>
    <meta name="format-detection" content="telephone=no, email=no"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/common.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/wechat/css/index.css?v=1.2.2"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/personalCenter.css?v=1.2.2" type="text/css">

    <script src="${pageContext.request.contextPath}/assets/wechat/lib/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>

    <style type="text/css">
        .header{
            background:#0197ff;
        }
        .container>section:first-of-type{
            top: 0.8rem;
        }
        .tbgg{
            position: relative;
        }
        .tbgg:after{
            content: "";
            position: absolute;
            height: 0.34rem;
            border-left: 1px solid #dedede;
            right: 0;
            top: 0.23rem;
        }
        .wrapper1{
            width: 100%;
            height: 100%;
            position: absolute;
        }
        .wrapper{
            width: 100%;
            position: absolute;
            top: 0;
            bottom: 0;
            overflow-y: auto;
        }
        .notification_list{
            overflow: hidden;
        }
        .list{
            background: #fff;
            height: 90%;
        }
        .public_listnodata{
            background: url(../img/ewma.png) no-repeat center center;
            background-size: 2.67rem 2.78rem;
            background-color: #fff
        }
        .returntop{
            position: fixed;
            z-index: 99999;
            width: 1.78rem;
            right: 0.3rem;
            bottom: 1.4rem;
        }
        .notification_list .to_detail {
            width: 0.2rem;
            position: absolute;
            right: 0.3rem;
            top: 0.35rem;
        }
        .mescroll-downwarp .downwarp-tip{
            margin-top: 30px;
        }
        .nodata{
            text-align: center;
        }
        .nodata img{width: 2.42rem;height:1.7rem;margin-top: 45%;}
        .nodata p{line-height: 0.6rem;}
    </style>

    <style>

        /*分页*/
        .pagination{
            margin-bottom: 20px;
            text-align: center;
            font-family: sans-serif;
            font-weight: 300;
            font-size: 22px;
            color: #545454;
        }
        .pageSelect{
            border: 1px solid #cecece;
            background:#f7f7f7;
            cursor: pointer;
            padding: 5px 10px 5px 10px;
        }
        .pageSelect:hover{
            border: 1px solid #3574ca;
            color: cornflowerblue;
        }
        /*充值记录分页*/
        .barcon {
            width: 100%;
            margin: 0 auto;
            text-align: center;
        }

        .barcon1 {
            font-size: 17px;
            float: left;
            margin-top: 20px;
            margin-left: 20px;
            color: #838383;
            font-size: 16px;
        }

        .barcon2 {
            float: right;
            margin-right: 20px;
        }

        .barcon2 ul {
            margin: 20px 0;
            padding-left: 0;
            list-style: none;
            text-align: center;
        }

        .barcon2 li {
            display: inline;
        }

        .barcon2 a {
            font-size: 16px;
            font-weight: normal;
            display: inline-block;
            padding: 5px;
            padding-top: 0;
            color: #838383;
            border: 1px solid #ddd;
            background-color: #fff;
        }

        .barcon2 a:hover{
            background-color: #eee;
        }

        .ban {
            opacity: .4;
        }
    </style>
</head>
<body class="notificationPage" onload="goPage(1,40);">
<a href="pay.html"><img class="returntop" src="${pageContext.request.contextPath}/assets/wechat/img/icon_returntop.png"/></a>
<div class="wrap">
    <section class="container">
        <!--<header class="header notification_title" style="display:none;">
            <span>费用中心</span>
        </header>-->
        <div class="top_titles clearfix">
            <span><a href="watchRecord.html" class="">观看记录</a></span>
            <span  class="active tbgg">充值记录</span>
        </div>
        <div class="main-center">
            <div class="main-wrap" style="margin-top: 1.5rem;">

                <!--中间右边内容-->
                <div class="main-right">

                    <div class="deposit-out" id="recahrgeTable">
                        <table cellpadding="0" cellspacing="0" class="deposit-box" width="100%" style="align:left;">
                            <tr class="deposit-title-list">

                                <th class="deposit-title">充值日期</th>
                                <th class="deposit-title">时间</th>
                                <th class="deposit-title">金额（元）</th>
                            </tr>
                            <tbody id="adminTbody">
                            <c:forEach var="item" items="${reChargeList}" varStatus="status">

                                <tr class="deposit-main-list">

                                    <td class="deposit-item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:formatDate type="time" value="${item.retime}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>

                                    <td class="deposit-item">${item.resecond}秒</td>
                                    <td class="deposit-item">${item.remoney}</td>
                                </tr>

                            </c:forEach>
                            </tbody>
                        </table>
                        <div id="barcon" class="barcon" >
                            <div id="barcon1" class="barcon1"></div>
                            <div id="barcon2" class="barcon2">
                                <ul>
                                    <li><a href="###" id="firstPage">首页</a></li>
                                    <li><a href="###" id="prePage">上一页</a></li>
                                    <li><a href="###" id="nextPage">下一页</a></li>
                                    <li><a href="###" id="lastPage">尾页</a></li>
                                    <!--<li><select id="jumpWhere"> </select></li>
                                    <li><a href="###" id="jumpPage" onclick="jumpPage()">跳转</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div style="height: 100px;text-align: center;display:none;"id="noneData">
                        <h2 style="margin-top: 2.0rem">暂无充值记录</h3>
                    </div>

                </div>
                <!--中间右边内容 end-->
                <div class="clear"></div>
            </div>
        </div>

    </section>


    <!-- 底部 -->
    <section id="sp_footer">
        <ul class="footer">
            <li class="footer-item">
                <a href="index.do" class="">
                    <i class="icon icon_index"></i>
                    <p class="icon_title" >首页</p>
                </a>
            </li>
            <li class="footer-item">
                <a href="watchRecord.do" class="">
                    <i class="icon icon_invest  active"></i>
                    <p class="icon_title" style="color: #00a0e9">费用</p>
                </a>
            </li>

            <li class="footer-item">
                <a href="personal.do" class="">
                    <i class="icon icon_my"></i>
                    <p class="icon_title">我的</p>
                </a>
            </li>
        </ul>
    </section>


</div>
</body>

<script>
    /**
     * 分页函数
     * pno--页数
     * psize--每页显示记录数
     * 分页部分是从真实数据行开始，因而存在加减某个常数，以确定真正的记录数
     * 纯js分页实质是数据行全部加载，通过是否显示属性完成分页功能
     **/

    var pageSize=0;//每页显示行数
    var currentPage_=1;//当前页全局变量，用于跳转时判断是否在相同页，在就不跳，否则跳转。
    function goPage(pno,psize){
        var itable = document.getElementById("adminTbody");
        var num = itable.rows.length;//表格所有行数(所有记录数)



        if(num==1){
            document.getElementById( "recahrgeTable").style.display= "none";
            document.getElementById( "noneData").style.display= "block";
        }else {
            document.getElementById( "noneData").style.display= "none";
            document.getElementById( "recahrgeTable").style.display= "block";
        }




        pageSize = psize;//每页显示行数
        //总共分几页
        if(num/pageSize > parseInt(num/pageSize)){
            totalPage=parseInt(num/pageSize)+1;
        }else{
            totalPage=parseInt(num/pageSize);
        }
        var currentPage = pno;//当前页数
        currentPage_=currentPage;
        var startRow = (currentPage - 1) * pageSize+1;
        var endRow = currentPage * pageSize;
        endRow = (endRow > num)? num : endRow;
        //遍历显示数据实现分页
        for(var i=1;i<(num+1);i++){
            var irow = itable.rows[i-1];
            if(i>=startRow && i<=endRow){
                irow.style.display = "";
            }else{
                irow.style.display = "none";
            }
        }
        var tempStr = "共"+num+"条观看记录 分"+totalPage+"页 当前第"+currentPage+"页";
        document.getElementById("barcon1").innerHTML = tempStr;

        if(currentPage>1){
            $("#firstPage").on("click",function(){
                goPage(1,psize);
            }).removeClass("ban");
            $("#prePage").on("click",function(){
                goPage(currentPage-1,psize);
            }).removeClass("ban");
        }else{
            $("#firstPage").off("click").addClass("ban");
            $("#prePage").off("click").addClass("ban");
        }

        if(currentPage<totalPage){
            $("#nextPage").on("click",function(){
                goPage(currentPage+1,psize);
            }).removeClass("ban")
            $("#lastPage").on("click",function(){
                goPage(totalPage,psize);
            }).removeClass("ban")
        }else{
            $("#nextPage").off("click").addClass("ban");
            $("#lastPage").off("click").addClass("ban");
        }
        var tempOption="";
        for(var i=1;i<=totalPage;i++)
        {
            tempOption+='<option value='+i+'>'+i+'</option>'
        }
        $("#jumpWhere").html(tempOption);
        $("#jumpWhere").val(currentPage);
    }


    function jumpPage()
    {
        var num=parseInt($("#jumpWhere").val());
        console.log(pageSize);
        if(num!=currentPage_)
        {
            goPage(num,pageSize);
        }
    }

</script>
</html>

