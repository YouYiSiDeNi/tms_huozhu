<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/8/22
  Time: 11:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>修改密码</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no, email=no" />

    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/common.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/wechat/css/personalCenter.css"/>

    <script src="${pageContext.request.contextPath}/assets/wechat/lib/js/jquery-2.1.4.js"></script>

    <style>
        .newer_detail{
            display: block;
            width: 6.8rem;
            height: 0.82rem;
            line-height: 0.8rem;
            margin: 0rem auto 0.38rem;
            font-size: 0.34rem;
            border-radius: 0.5rem;
            text-align: center;
            color: #fff;
            background: -webkit-linear-gradient(left, #ff0037, #f82424); /* Safari 5.1 - 6.0 */
            background: -o-linear-gradient(left, #ff0037, #f82424); /* Opera 11.1 - 12.0 */
            background: -moz-linear-gradient(left, #ff0037, #f82424); /* Firefox 3.6 - 15 */
            background: linear-gradient(left, #ff0037, #f82424); /* 标准的语法 */
            box-shadow: 0px 5px 20px #ff9090;
        }
        .deposit-wrap{
            background: #fff;
            padding-top: 1px;
            padding-bottom: 30px;
        }
        .main-center{
            width: 100%;
            padding: 0px 0 0px;
        }
        .main-wrap{
            width:100%;
            margin: 0 auto;
        }
        /*********************************************9立即充值*************************************************/
        .deposit-wrap{
            background: #fff;
            padding-top: 1px;
            padding-bottom: 30px;
        }
        .go-deposit{
            width: 100%;
            margin: 0 auto;
        }

        .go-deposit-box2{
            margin-top: 10px;
            background: #f9f9f9;
            height: 260px;
        }
        .go-deposit-title{
            padding: 15px 18px;
            font-size: 14px;
            color: #575757;
        }

        .pay-fee{
            position: relative;
            display: inline-block;
            width:80px;
            height: 45px;
            cursor: pointer;
        }


        .pay_fee1{
            margin-left: 0.5rem;
            margin-top: 0.3rem;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/fee_1.png) no-repeat center center;
        }
        .pay_fee5{
            margin-left: 0.5rem;
            margin-top: 0.3rem;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/fee_5.png) no-repeat center center;
        }
        .pay_fee10{
            margin-left: 0.5rem;
            margin-top: 0.3rem;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/fee_10.png) no-repeat center center;
        }
        .pay_fee20{
            margin-left: 0.5rem;
            margin-top: 0.3rem;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/fee_20.png) no-repeat center center;
        }
        .pay_fee30{
            margin-left: 0.5rem;
            margin-top: 0.3rem;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/fee_30.png) no-repeat center center;
        }
        .pay_fee50{
            margin-left: 0.5rem;
            margin-top: 0.3rem;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/fee_50.png) no-repeat center center;
        }
        .pay_fee100{
            margin-left: 0.5rem;
            margin-top: 0.3rem;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/fee_100.png) no-repeat center center;
        }

        .number-inout{
            padding-bottom: 10px;

        }
        .number-inout2{
            padding-bottom: 10px;
            margin-left: 0.5rem;
        }
        .pay-left-click{

            box-sizing: border-box;
            border: 1px solid #0095d9;
        }
        .pay-left-click .check-active{
            position: absolute;
            bottom: 0px;
            right: 0px;
            display: block;
            width:15px;
            height: 15px;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/check_icon2.png) no-repeat center center;
        }

        .pay-fee-click{
            border: 1px solid #0095d9;
        }
        .pay-fee-click .check-active2{
            position: absolute;
            bottom: 0px;
            right: 0px;
            display: block;
            width:15px;
            height: 15px;
            background: url(${pageContext.request.contextPath}/assets/wechat/img/login/check_icon2.png) no-repeat center center;
        }

        .go-deposit-submit{
            height: 120px;
            padding: 11px 0 16px 0.5rem;
            border-top: 1px solid #ebebeb;
            text-align: left;
        }
        .go-deposit-btn{
            width:237px;
            height:46px;
            margin: 0 auto;
            font-size: 18px;
            color: #fff;
            line-height: 46px;
            text-align: center;
            background: #ff2d2d;
            border-radius: 5px;
            cursor: pointer;
        }
        .dethr-inp {

            display: inline-block;

            height: 40px;
            border: 1px solid #bdbdbd;
            text-align: center;
            border-radius: 2px;

            font-size: 16px;
            font-weight: 600;

        }
    </style>
</head>
<body>
<div class="find_loginPwd_title header">
    <a href="javascript:history.go(-1)">
        <i class="icon"></i>
    </a>
    <span>充值中心</span>
</div>
<section class="find_loginPwd_wrap">
    <div class="main-center">
        <div class="img" ><img src="${pageContext.request.contextPath}/assets/wechat/img/banner01.png" style="width: 100%;"/></div>
    </div>
    <div class="main-center">
        <div class="main-wrap deposit-wrap">
            <!--中间左边菜单列表-->
            <div class="go-deposit">

                <div class="go-deposit-box2">
                    <h5 class="go-deposit-title">请选择充值金额</h5>
                    <div class="number-inout">
                        <input type="radio" name="payfee" id="pay_fee1" checked="checked" style="display:none"/><label class="pay-fee pay_fee1 pay-fee-click"><i class="check-active2"></i></label>
                        <input type="radio" name="payfee" id="pay_fee5" style="display:none"/><label class="pay-fee pay_fee5"><i class="check-active2"></i></label>
                        <input type="radio" name="payfee" id="pay_fee20" style="display:none"/><label class="pay-fee pay_fee20"><i class="check-active2"></i></label>
                        <input type="radio" name="payfee" id="pay_fee30"  style="display:none"/><label class="pay-fee pay_fee30"><i class="check-active2"></i></label>
                        <input type="radio" name="payfee" id="pay_fee50"  style="display:none"/><label class="pay-fee pay_fee50"><i class="check-active2"></i></label>
                        <input type="radio" name="payfee" id="pay_fee100"  style="display:none"/><label class="pay-fee pay_fee100"><i class="check-active2"></i></label>
                    </div>
                    <div class="number-inout2">
                                                <span class="number-inout-box">
                                                    <input class="dethr-inp" type="number" value="1" placeholder="自定义"><span style="margin-left: 5px;font-size: 0.3rem;">元（1元 =5分钟）</span>
                                                </span>
                    </div>
                </div>
            </div>

            <div class="clear"></div>
        </div>
        <div class="newer_detail" type="button" id="payBtn" value="提交" onclick="topay()"/>提交订单</div>

</section>

</body>


<!--头部-->
<script src="${pageContext.request.contextPath}/assets/wechat/lib/js/RainbowBridge.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

    $(function(){
        $(".pay-left").click(function(){
            $(".pay-left").removeClass("pay-left-click");
            $(this).addClass("pay-left-click");
        })

        $(".pay_fee1").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('1');
        })

        $(".pay_fee5").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('5');
        })
        $(".pay_fee20").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('20');
        })
        $(".pay_fee30").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('30');
        })
        $(".pay_fee50").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('50');
        })
        $(".pay_fee100").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('100');
        })



        $(".dethr-inp").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
        })

        //	限定输入必须为数字
        $(".dethr-inp").keypress(function(b) {
            var keyCode = b.keyCode ? b.keyCode : b.charCode;
            if (keyCode != 0 && (keyCode < 48 || keyCode > 57) && keyCode != 8 && keyCode != 37 && keyCode != 39)
            {return false;
            }
            else {
                return true;
            }
        }).keyup(function(e) {
            var keyCode = e.keyCode ? e.keyCode : e.charCode;
            console.log(keyCode);
            if (keyCode != 8) {
                var numVal = parseInt($(".dethr-inp").val()) || 0;
                numVal = numVal < 1 ? 1 : numVal;
                $(".dethr-inp").val(numVal);
            }
        }).blur(function() {
            var numVal = parseInt($(".dethr-inp").val()) || 0;
            numVal = numVal < 1 ? 1 : numVal;
            $(".dethr-inp").val(numVal);
        });

    })


    //支付提交操作
    function topay() {
        var radios = document.getElementsByName("payment");
        var num = $(".dethr-inp").val();
//      document.location.href = "towxpay.do?minute=" + num;
        document.location.href = "payment.html";


    }

</script>

</html>
