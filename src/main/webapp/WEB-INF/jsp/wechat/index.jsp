<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/8/22
  Time: 11:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no, email=no" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/common.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/invest.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/wechat/lib/js/jquery-2.1.4.js"></script>
    <title>船主端</title>


</head>
<style>

    .video-head-pic {
        display: block;
        width: 100%;
        height: 40%;
        background-image: url("../img/video_bg.jpg") ;
        position: absolute;
        background-size: 100%;
    }
    .newer_detail{
        display: block;
        width: 6.8rem;
        height: 0.72rem;
        line-height: 0.72rem;
        margin: 0.48rem auto 0.38rem;
        font-size: 0.34rem;
        border-radius: 0.5rem;
        text-align: center;
        color: #fff;
        background: -webkit-linear-gradient(left, #008dff, #45bbff); /* Safari 5.1 - 6.0 */
        background: -o-linear-gradient(left, #008dff, #45bbff); /* Opera 11.1 - 12.0 */
        background: -moz-linear-gradient(left, #008dff, #45bbff); /* Firefox 3.6 - 15 */
        background: linear-gradient(left, #008dff, #45bbff); /* 标准的语法 */
        box-shadow: 0px 5px 20px #b3cde2;
    }
    .newer_detail2{
        display: block;
        width: 6.8rem;
        height: 0.72rem;
        line-height: 0.72rem;
        margin: 0.48rem auto 0.38rem;
        font-size: 0.34rem;
        border-radius: 0.5rem;
        text-align: center;
        color: #fff;
        background: -webkit-linear-gradient(left, #f45a3f, #ff923c); /* Safari 5.1 - 6.0 */
        background: -o-linear-gradient(left, #f45a3f, #ff923c); /* Opera 11.1 - 12.0 */
        background: -moz-linear-gradient(left, #f45a3f, #ff923c); /* Firefox 3.6 - 15 */
        background: linear-gradient(left, #f45a3f, #ff923c); /* 标准的语法 */
        box-shadow: 0px 5px 20px #ffae95;
    }
</style>
<body style="overflow: hidden">
<div class="wrap">
    <!-- 头部 -->
    <section id="sp_header">
        <ul class="sp_header_navbar" id="sp_header_navbar">
            <li id="newcomer" class="navbar navbar_active" var="btn1" onclick="switchVideo(devicenumber,shipid)">主设备视频</li>
            <li id="transfer_place" style="border: 0;" class="navbar" onclick="switchVideo(devicenumbertwo,shipid)">辅设备视频</li>
        </ul>
    </section>
    <!-- 中部 -->
    <section style="width: 100%;height: 100%;overflow: hidden;margin-top: 0.96rem">
        <div class="video-head-pic" id="swtcenter" ></div>
        <div class="newer_detail" style="margin-top: 75%;"  id="openBtn" >播放视频</div>
        <div class="newer_detail2" style="display:none;margin-top: 75%;" id="leaveBtn" onclick="leavevideo()">
            <span id="timeFei"></span>停止计费
        </div>

    </section>
    <!-- 底部 -->
    <section id="sp_footer">
        <ul class="footer">
            <li class="footer-item">
                <a href="index.do" class="">
                    <i class="icon icon_index  active"></i>
                    <p class="icon_title" style="color: #00a0e9">首页</p>
                </a>
            </li>
            <li class="footer-item">
                <a href="watchRecord.do" class="">
                    <i class="icon icon_invest"></i>
                    <p class="icon_title">费用</p>
                </a>
            </li>

            <li class="footer-item">
                <a href="personal.do" class="">
                    <i class="icon icon_my"></i>
                    <p class="icon_title">我的</p>
                </a>
            </li>
        </ul>
    </section>
</div>
</body>






<script>
    /*选择播放设备*/
    $(function() {
        $(".navbar").click(function () {
            $(".navbar").removeClass("navbar_active");
            $(this).addClass("navbar_active");
        })
    })


</script>

<script type="text/javascript">

    var seconds = ${resecond};
    var flag = false;

    var c=0;
    var t;

    function openvideo(){
        $("#openBtn").hide();
        $("#leaveBtn").show();

        document.getElementById('timeFei').innerText = "("+ c +")";
        c=c+1;
        t=setTimeout("openvideo()",1000);

//看视频（仅限3代设备）
        $.ajax({
            url: 'selectDeviceVoidurl.do?devicenumber='+ devicenumber,
            type: 'post',
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.ret==0){
                    $("#openBtn").onclick(openvideo2(data.html5url,ship_id));
//                  alert(data.html5url)
                }else{
                    alert("当前设备不在线！");
                }
            }
        });
    }
    //观看视频并显示
    function openvideo2(html5url,ship_id){
        if (seconds<=0){
            swal({
                title: '观看余额不足',
                text: '请充值后再返回观看！',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '立即充值',
            }).then(function(){
                window.location.href='payment.html';
                );
        });

    }else {
        if (!flag){
//              document.getElementById("openBtn").disabled = true;
//              document.getElementById("leaveBtn").disabled = false;
            settime(ship_id);
            $.ajax({
                url:'startVideo.do?shipid='+ship_id,
                type:'post',
                dataType:'json',
                async:true,
                contentType: "application/json; charset=utf-8",
                success:  function(data) {
                    flag = true;
                    var dataStr = data + "";
                    if(dataStr == "true"){
                        var inserthtml = '<iframe id="swtframe" src="' + html5url + '" name="swtframe" width="560" height="485"  frameborder="0" scrolling="no" ></iframe>';
                        $("#swtcenter").append(inserthtml);
                    }
                },
                error  :  function(XMLHttpRequest, textStatus, errorThrown) {
                    // debugger;
                    alert('网络不好，请稍后尝试...');
                }
            });


        }else {
            alert('正在播放视频，请先关闭...');
        }
    }
    }


    //停止计费
    function leavevideo(){
        clearTimeout(t);
        $("#openBtn").show();
        $("#leaveBtn").hide();
        c=0
        document.getElementById('timeFei').innerText = "("+ 0 +")";
        return false;
    }


    //切换视频
    function switchVideo(devicenumber,ship_id){
        if(devicenumber == null || devicenumber == "" || devicenumber.length == 0){
            alert("序列号为空");
        }else {
            openvideo(devicenumber,ship_id);
        }
    }

</script>


</html>