<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/8/22
  Time: 11:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>修改密码</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no, email=no" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/common.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/wechat/css/personalCenter.css"/>
    <script src="${pageContext.request.contextPath}/assets/wechat/lib/js/jquery-2.1.4.js"></script>

    <style>
        .newer_detail{
            display: block;
            width: 6.8rem;
            height: 0.72rem;
            line-height: 0.72rem;
            margin: 0.48rem auto 0.38rem;
            font-size: 0.34rem;
            border-radius: 0.5rem;
            text-align: center;
            color: #fff;
            background: -webkit-linear-gradient(left, #008dff, #45bbff); /* Safari 5.1 - 6.0 */
            background: -o-linear-gradient(left, #008dff, #45bbff); /* Opera 11.1 - 12.0 */
            background: -moz-linear-gradient(left, #008dff, #45bbff); /* Firefox 3.6 - 15 */
            background: linear-gradient(left, #008dff, #45bbff); /* 标准的语法 */
            box-shadow: 0px 5px 20px #b3cde2;
        }
    </style>
</head>
<body>
<div class="find_loginPwd_title header">
    <a href="javascript:history.go(-1)">
        <i class="icon"></i>
    </a>
    <span>修改密码</span>
</div>
<section class="find_loginPwd_wrap">
    <ul class="list">
        <li class="list_item">
            <label>
                <span class="item_text">请输入原密码</span><input type="text" name="phone" id="phone" maxlength="11" minlength="11" >
            </label>
        </li>
        <li class="list_item">
            <label>
                <span class="item_text">请输入新密码</span><input type="text" name="phone" id="phone" maxlength="11" minlength="11" >
            </label>
        </li>
        <li class="list_item">
            <label>
                <span class="item_text">确认新密码</span><input type="text" name="phone"  id="phone" maxlength="11" minlength="11" >
            </label>
        </li>
    </ul>
    <a href="javascript:;" class="newer_detail" id="nextstep">保存</a>

</section>

</body>


</html>

