
 <%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/8/22
  Time: 11:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>易航OTMS船主登录</title>
    <meta content="易航OTMS,易航" name="Keywords">
    <meta content="欢迎使用易航OTMS船主端" name="description">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0"/>
    <meta name="format-detection" content="telephone=no, email=no"/>
    <meta property="wb:webmaster" content="xxxxxxx"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/wechat/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/wechat/css/index.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/wechat/lib/css/layer.css"/>
    <script src="${pageContext.request.contextPath}/assets/wechat/lib/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
    <script src="${pageContext.request.contextPath}/assets/wechat/js/wapframwork.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<div class="login_bg">
    <img src="${pageContext.request.contextPath}/assets/wechat/img/login_bg.png">
    <div>
        <div class="login_title" style="display: none;">
        </div>

        <!--登录内容部分-->
        <div class="login_content">
            <div class="username">
                <i class="icon icon_user" style="margin-top: 18px"></i>
                <input type="text" id="user" placeholder="用户名/手机号"/>
            </div>
            <div class="password">
                <i class="icon icon_pwd"  style="margin-top: 18px"></i>
                <input type="password" id="pwd" placeholder="密码"/>
                <img class="icon_eye" src="${pageContext.request.contextPath}/assets/wechat/img/bkj.png"/>
            </div>
            <span class="btn login_btn">登录</span>
            <p class="forget_btn">
                <a href="forgetPwd.do">忘记密码?</a>
            </p>

        </div>
    </div>
</div>
</body>

<script src="${pageContext.request.contextPath}/assets/wechat/lib/js/layer.js" type="text/javascript" charset="utf-8"></script>
<!--头部-->
<script src="${pageContext.request.contextPath}/assets/wechat/lib/js/RainbowBridge.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    setTitle(".login_title", {'title': '登录'});
</script>

<script src="${pageContext.request.contextPath}/assets/wechat/js/login.js?v=1.2.2" type="text/javascript" charset="utf-8"></script>
<script>
    $(".thirdPartyLoginWrap").on("click", "li", function () {
        weChatLoginFn(wechatLoginObj);
    });
</script>
</html>

