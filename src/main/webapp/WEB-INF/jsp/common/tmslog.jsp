<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/21
  Time: 13:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>更新日志</title>

    <style type="text/css">
        body, html {font-family:"微软雅黑" !important;}
    </style>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

    <!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
    <link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <%@include file="../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../common/top.jsp"%>

        <div class="content">
            <div class="container-fluid">

                <div class ="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="row"  style="margin:0 10 0; margin-top: -10px">.
                                <h4> 易航oTMS 船舶管理系统</h4>

                                <div class="row"  style="margin:0 10 0; margin-top: -10px">.
                                    <h5><b>v1.0.3</b>&nbsp;2018-07-10</h5>
                                    <p>
                                    <h5>-优化地图呈现方式</h5>
                                    </p>
                                    <p>
                                    <h5>-即时视频适配行舟仪G210</h5>
                                    </p>
                                </div>
                                <hr>

                                <div class="row"  style="margin:0 10 0; margin-top: -10px">.
                                    <h5><b>v1.0.2</b>&nbsp;2018-06-29</h5>
                                    <p>
                                    <h5>-优化地图呈现方式</h5>
                                    </p>
                                    <p>
                                    <h5>-调整部分页面用户体验</h5>
                                    </p>
                                    <p>
                                    <h5>-新增充值记录</h5>
                                    </p>
                                </div>
                                <hr>

                                <h5><b>v1.0.1</b>&nbsp;2018-06-09</h5>
                                <p>
                                <h5>-界面优化</h5>
                                </p>
                                <p>
                                <h5>-新增微信充值</h5>
                                </p>
                                <p>
                                <h5>-已知Bugs修复</h5>
                                </p>
                            </div>
                            <hr>
                            <div class="row"  style="margin:0 10 0; margin-top: -10px">.
                            <h5><b>v1.0.0</b>&nbsp;2018-05-29</h5>
                            <p>
                                <h5>-优化运输计划呈现方式</h5>
                            </p>
                             <p>
                                <h5>-新增充值记录</h5>
                            </p>
                            </div>
                            <hr>

                             <div class="row"  style="margin:0 10 0; margin-top: -10px">.
                            <h5><b>v0.1.0</b>&nbsp;2018-05-10</h5>
                            <p>
                                <h5>-第一次发版</h5>
                            </p>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
939996
    <%@include file="../common/modal.jsp"%>
</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>
<script>
    document.getElementById("crumbs").innerHTML="系统更新日志";
</script>



</html>
