<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/5/15
  Time: 11:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h2 class="modal-title" id="myModalLabel1">
                    修改登录密码
                </h2>
            </div>
            <div class="modal-body">
                <div class="bs-example bs-example-form" >
                    <div class="input-group">
                        <span class="input-group-addon">请输入原密码</span>
                        <input type="text" class="form-control" placeholder="请输入旧密码" id="old-password">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">请输入新密码</span>
                        <input type="text" class="form-control" placeholder="请输入新密码" id="new-password">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon">确认新密码</span>
                        <input type="text" class="form-control" placeholder="确认新密码" id="algin-password">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="checkFinish()">
                    确定修改
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal99" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h2 class="modal-title" id="myModalLabel">
                    确定要退出吗？
                </h2>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="window.location.href='/'" >
                    确定退出
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<%--修改密码--%>
<script type="text/javascript">
    function checkFinish(){
        var regpsw = /^[0-9a-zA-Z]{6,16}$/;
        var oPassword = document.getElementById("old-password").value;
        var nPassword = document.getElementById("new-password").value;
        var aPassword = document.getElementById("algin-password").value;
        if(oPassword == ''){
            alert("请输入旧密码");
            return false;
        }else if(!(regpsw.test(oPassword))){
            alert("旧密码由6~16位字母或数字组成");
            return false;
        }else if(nPassword == ''){
            alert("请输入新密码");
            return false;
        }else if(!(regpsw.test(nPassword))){
            alert("新密码由6~16位字母或数字组成");
            return false;
        }else if(aPassword == ''){
            alert("请再次输入新密码");
            return false;
        }else if(aPassword != nPassword ){
            alert("两次密码输入不一致");
            return false;
        }

        $.ajax({
            type : 'post',
            url : 'updatePassword.do?password='+oPassword + '&newpwd=' + nPassword,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function (data) {
                alert(data);
            }
        })
    }
</script>