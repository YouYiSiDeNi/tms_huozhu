<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/5/15
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<div class="sidebar" data-color="blue" data-image="${pageContext.request.contextPath}/assets/img/sidebar-5.jpg">


    <div class="sidebar-wrapper">
        <input type="hidden" id="username" value="${sessionScope.username}">
        <div class="logo">
            <a  class="simple-text">
                 <img src="http://d.yihangotms.com/images/leftlogo_${sessionScope.username}.png">
                <%--<img src="http://download2.huizhaochuan.com.cn/leftlogo_${sessionScope.username}.png">--%>
            </a>
        </div>

        <ul class="nav">
            <li id="active1"  style="display:none;">
                <a href="dashboard.do">
                    <i class="pe-7s-graph"></i>
                    <p>概况</p>
                </a>
            </li>
            <li id="active2"  style="display:none;">
                <a href="searchShip.do?emtiy=2">
                    <i class="pe-7s-user"></i>
                    <p>运力管理</p>
                </a>
            </li>
            <li id="active3"    style="display:none;">
                <a href="searchTransport.do">
                    <i class="pe-7s-note2"></i>
                    <p>运输管理</p>
                </a>
            </li>

            <li id="active4" style="display:none;">
                <a href="toReCharge.do">
                    <i class="pe-7s-credit"></i>
                    <p>充值记录</p>
                </a>
            </li>

            <li id="active5"  style="display:none;">
                <a href="toShipManager.do">
                    <i class="pe-7s-helm"></i>
                    <p>船舶管理</p>
                </a>
            </li>

            <li class="active-pro" id="active-pro" style="display:none;">
                <a href="maps.do">
                    <i class="pe-7s-rocket"></i>
                    <p>地图模式</p>
                </a>
            </li>


        </ul>
    </div>
</div>
<script>
    $(function () {
        var username =$("#username").val();
        if (username=="admin"){
            $("#active5").show();
            $("#active1").hide();
            $("#active2").hide();
            $("#active3").hide();
            $("#active4").hide();
            $("#active-pro").hide();
        }
        else {
            $("#active5").hide();
            $("#active1").show();
            $("#active2").show();
            $("#active3").show();
            $("#active4").show();
            $("#active-pro").show();
        }

    })
</script>