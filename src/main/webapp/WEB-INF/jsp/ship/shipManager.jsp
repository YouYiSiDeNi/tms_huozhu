<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/23
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>易航oTMS</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

    <!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
    <link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- layui -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">

</head>
<body>

<div class="wrapper">
    <%@include file="../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../common/top.jsp"%>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content">
                            <div class="site-demo-button" id="layerDemo" style="margin-bottom: 0;">
                                <div class="layui-inline">
                                    <label class="layui-form-label">船名</label>
                                    <div class="layui-input-inline">
                                        <input type="text" name="shipname2" id="shipname2" lay-verify="required" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label" style="width: 100px">运输状态</label>
                                    <div class="layui-input-inline">
                                        <select name="isemtiy2" id="isemtiy2" lay-verify="required" lay-search="" style="margin-top:8px">
                                            <option value="">全部</option>
                                            <option value="0">空载</option>
                                            <option value="1">满载</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label"  style="width: 100px">是否在线</label>
                                    <div class="layui-input-inline">
                                        <select name="isonline2" id="isonline2" lay-verify="required" lay-search="" style="margin-top:8px">
                                            <option value="">全部</option>
                                            <option value="0">在线</option>
                                            <option value="1">离线</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="layui-input-inline " style="width: 90px">
                                    <button class="layui-btn" id="demoTable" data-type="reload">
                                        <i class="layui-icon" style="font-size: 20px; "></i> 搜索
                                    </button>
                                </div>
                                <div class="layui-input-inline" style="position: absolute;right: 2%">
                                    <input type="hidden" id="page"value="">
                                    <input type="hidden" id="shipname" value="">
                                    <button data-method="offset" data-type="auto" class="layui-btn layui-btn-normal" onclick="searchShips($('#page').val(),$('#shipname').val())">待审核运力</button>
                                    <button class="btn btn-info" type="button"  onclick="resetpwd()">重置密码</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <table class="layui-hide" id="shipinfo" lay-filter="demo"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--添加运力--%>
<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    船舶运力信息
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" id="pages" value="">
                            <input type="text" id="shipnames" class="form-control" >
                        </div>
                        <div class="col-md-4 pull-left">
                            <button class="btn btn-info" type="button"  onclick="searchShips($('#pages').val(),$('#shipnames').val())">搜索船舶</button>
                        </div>

                    </div>

                </div>
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>用户名</th>
                        <th>船名</th>
                        <th>申请时间</th>
                        <th>审核状态</th>
                    </tr>
                    </thead>
                    <tbody id="tbody"></tbody>
                </table>

                <div class="row">
                    <div class="col-md-6">
                        <span id="summary"></span>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" id="pagenum"/>
                        <input type="hidden" id="pagesize"/>
                        <ul id="pagination" class="pagination">
                            <li id="01"><a href="#" class="btn btn-primary">首页</a></li>
                            <li id="02"><a href="#" class="btn btn-primary" style="margin-left: 5px">上一页</a></li>
                            <li id="03"><a href="#" class="btn btn-primary" style="margin-left: 5px">下一页</a></li>
                            <li id="04"><a href="#" class="btn btn-primary" style="margin-left: 5px">最后一页</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->



<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title" id="myModalLabel1">
                    重置登录密码
                </h3>
            </div>
            <div class="modal-body">
                <div class="bs-example bs-example-form" >
                    <div class="input-group">
                        <span class="input-group-addon">请输入账号</span>
                        <input type="text" class="form-control" placeholder="请输入账号" id="account">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="checkresetPWD()">
                    确定修改
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<%@include file="../common/modal.jsp"%>


</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>

<!-- layui -->


<script>
    document.getElementById("active5").className = "active";
    document.getElementById("crumbs").innerHTML="船舶管理";
    layui.use('table', function(){
        var table = layui.table;
        table.render({
            elem: '#shipinfo'
            ,url:'selectShipInfosManagerByQuery.do'
            ,cellMinWidth: 10
            ,cols: [[
                { field:'shipid',type:'checkbox'}
                ,{field:'shipname',  title: '船名'}
                ,{field:'shipownername',  title: '船主姓名',event:'setName'}
                ,{field:'shipownerphone',  title: '船主电话',event:'setPhone'}
                ,{field:'isonline',  title: '在线状态',event:'setSign',templet:"#isonline"}
                ,{field:'ton',  title: '船舶吨位', sort: true}
                ,{field:'isemtiy',  title: '运输状态',templet:"#isemtiy"}
//                ,{field:'edit',  title: '操作', toolbar: '#barDemo'}
            ]]
            ,skin: 'row' //表格风格
            ,even: true
            ,page: true //是否显示分页
            ,limits: [10,20,30,50]
            ,limit: 10 //每页默认显示的数量
        });
        table.on('tool(demo)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                    ,layEvent = obj.event; //获得 lay-event 对应的值
           if(layEvent === 'edit'){
              shipinfosEdit(data.shipid);
              // table.reload('shipinfo');
            }
            if (layEvent==='setSign'){
               updateIsonline(data.shipid,data.deviceInfo.isonline);
               // table.reload('shipinfo');
            }
            if (layEvent==='setName'){
                layer.prompt({
                    formType: 0
                    ,title: '修改 ID 为 ['+ data.shipname +'] 的船主姓名'
                    ,value: data.shipownername
                }, function(value, index){
                    //这里一般是发送修改的Ajax请求
                    updateShipownername(data,value,index,obj);

                });
            }
            if (layEvent==='setPhone'){
                layer.prompt({
                    formType: 0
                    ,title: '修改 ID 为 ['+ data.shipname +'] 的船主电话'
                    ,value: data.shipownerphone
                }, function(value, index){
                    //这里一般是发送修改的Ajax请求
                    updateShipownerphone(data,value,index,obj);

                });
            }
        });

        var $ = layui.$, active = {
            reload: function(){
                //执行重载
                table.reload('shipinfo', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    ,where: {
                        shipname:$('#shipname2').val(),
                        isemtiy:$('#isemtiy2').val(),
                        isonline:$('#isonline2').val()
                    }
                });
            }
        };


        $('#demoTable').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

    });

    function updateShipownername(data,value,index,obj){
        $.ajax({
            type : 'post',
            url : 'updateShipownerNameByShipid.do?shipownername='+value+'&shipid='+data.shipid,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function (data) {

                if(data==true){
                    layer.close(index);
                    obj.update({
                        shipownername: value
                    });
                    layer.msg("修改成功", {icon: 6});
                }else{
                    layer.msg("修改失败", {icon: 5});

                }
            }
        })
    }




    function updateShipownerphone(data,value,index,obj){
        $.ajax({
            type : 'post',
            url : 'updateShipownerPhoneByShipid.do?shipownerphone='+value+'&shipid='+data.shipid,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function (data) {

                if(data==true){
                    layer.close(index);
                    obj.update({
                        shipownerphone: value
                    });
                    layer.msg("修改成功", {icon: 6});
                }else{
                    layer.msg("修改失败", {icon: 5});

                }
            }
        })
    }


   function updateIsonline (shipid,isonline) {
       $.ajax({
           type : 'post',
           url : 'updateIsonlineByShipid.do?isonline='+isonline+'&shipid='+shipid,
           dataType : 'json',
           contentType: "application/json; charset=utf-8",
           success : function (data) {
               if(data==true){
                   layer.msg("修改成功", {icon: 6});
               }else{
                   layer.msg("修改失败", {icon: 5});
               }
           }
       })
   }
</script>
<script type="text/html" id="isemtiy">
    {{# if(d.isemtiy==0){ }}
    空载
    {{# }else{ }}
    满载
    {{# } }}
</script>

<script type="text/html" id="isonline">
    {{# if(d.deviceInfo.isonline==0){ }}
    <%--{{# if(d.isemtiy==0){ }}--%>
    <%--空载--%>
    <%--{{# }else{ }}--%>
    <%--满载--%>
    <%--{{# } }}--%>
    在线
    {{# }else{ }}
    离线
    {{# } }}
</script>


<script type="text/html" id="shipspeed">
    {{ d.deviceInfo.shipspeed}}
</script>

<script>
    function formatDate(date){
        var newTime = new Date(date);
        var year = newTime.getFullYear();
        var mon = newTime.getMonth() + 1;
        var day = newTime.getDate();
        var hour = newTime.getHours();
        var min = newTime.getMinutes();
        var sec = newTime.getSeconds();
        var newTimes = year + "-" + (mon < 10 ? ('0' + mon) : mon) + "-"
            + (day < 10 ? ('0' + day) : day);
        return newTimes;
    }
    function updateUserAndShipByUstats(shipid,userid) {
        var page= $('#pagenum').val();
        var shipname =$('#shipnames').val();
        $.ajax({
            type : 'post',
            url : 'updateUserAndShipByUstats.do?shipid='+shipid+'&userid='+userid,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if (data==true){
                    alert("审核通过");
                    searchShips(page,shipname)
                }else{
                    alert("审核失败");
                }
            }
        });
    }
function searchShips(page,shipname) {

    $.ajax({
        type : 'post',
        url : 'selectUserinfoAndShipinfo.do?page='+page+'&shipname='+shipname,
        dataType : 'json',
        contentType: "application/json; charset=utf-8",
        success : function(data) {
            var array =data.userAndShipList;
            var str ="";
            $.each(array,function (i) {
                if(array[i].ustats==1){
                    var ustats="申请审核中";
                }
                else if(array[i].ustats==2){
                    var ustats="已审核通过";
                }
                if(array[i].addtime==null){
                    var addtime="未填写";
                }
                else{
                    var addtime=formatDate(array[i].addtime);
                }
                str+="<tr><td>"+array[i].userInfo.username+"</td><td>"+ array[i].shipInfo.shipname+"</td><td>"+addtime+"</td><td><a href='javascript:;' onclick='updateUserAndShipByUstats(" +array[i].shipInfo.shipid+","+array[i].userInfo.userid+")' id='spanone' class='btn btn-sm btn-info'>"+ustats+"</a></td></tr>";

                })
                $("#tbody").html(str);

                displayFooter(data.pageInfo);

            }

        });
        $('#myModal7').modal('show');

    }
function displayFooter(pageInfo) {
<%--当前第 ${pageInfo.pageNum} 页.总共 ${pageInfo.pages} 页.一共 ${pageInfo.total} 条记录--%>
    var newText = '共' + pageInfo.total + '条记录，' + '当前第' + pageInfo.pageNum + '页，' + '总共' + pageInfo.pages + '页';
    $("#summary").text(newText);
    $("#pagenum").val(pageInfo.pageNum);
    $("#pagesize").val(pageInfo.pages);
};

$("#01").click(function(){
    var pagenum = 1;
    searchShips(pagenum,$('#shipnames').val());
});

$("#04").click(function(){
    var pagenum =$("#pagenum").val();
    var pagesize =$("#pagesize").val();
    pagenum = pagesize;
    searchShips(pagenum, $('#shipnames').val());
});

$("#02").click(function(){
    var pagenum =$("#pagenum").val();
    if(pagenum == 1){
        return false;
    } else{
        pagenum--;
        searchShips(pagenum, $('#shipnames').val());
    }
});

$("#03").click(function(){
    var pagenum =$("#pagenum").val();
    var pagesize =$("#pagesize").val();
    if(pagenum == pagesize){
        return false;
    } else{
        pagenum++;
    searchShips(pagenum, $('#shipnames').val());
    }
});



function resetpwd() {
    $("#myModal3").modal("show");
}
function checkresetPWD(){
    var account =$("#account").val();
    if(account == ''){
        alert("请输入账号");
        return false;
    }
    $.ajax({
        type : 'post',
        url : 'resetPassword.do?username='+account,
        dataType : 'json',
        contentType: "application/json; charset=utf-8",
        success : function (data) {
            alert(data);
        }
    })
}

</script>
</html>
