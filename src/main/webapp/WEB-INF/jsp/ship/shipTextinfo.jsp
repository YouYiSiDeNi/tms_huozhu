<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/23
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>TMS | ${shipInfo.shipname}详情</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>

</head>
<body >

<div class="detail-right">
       <div class="detail-cont-out">
           <p class="list-cont-title2"><span class="source-till"> 船舶名称:</span>${shipInfo.shipname}</p>
           <p class="list-cont-title2"><span class="source-till">船舶吨位:</span>${shipInfo.ton}吨</p>
           <p class="list-cont-title2"><span class="source-till">船舶尺寸:</span>长${shipInfo.shiplong}米，宽${shipInfo.shipwide}米</p>
           <p class="list-cont-title2"><span class="source-till">船舱尺寸:</span>长${shipInfo.cabinlong}米，宽${shipInfo.cabinwide}米</p>
            <p class="list-cont-title2"><span class="source-till">船舱深度:</span>${shipInfo.cabinheight}米</p>
           <p class="list-cont-title2"><span class="source-till">重载吃水深度:</span>${shipInfo.loaddraft}米</p>
            <p class="list-cont-title2"><span class="source-till">实时位置:</span>经度:${shipInfo.deviceInfo.longitude}，纬度:${shipInfo.deviceInfo.latitude}</p>
           <p class="list-cont-title2"><span class="source-till">数据最后上传时间:</span><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${shipInfo.deviceInfo.uptime}" /></p>
                <c:choose>
                       <c:when test="${shipInfo.shipownerphone == '暂无'}">
                              <p class="list-cont-title2"><span class="source-till">船主电话:</span>数据收集中</p>
                       </c:when>
                <c:otherwise>
                        <p class="list-cont-title2"><span class="source-till">船主电话:</span>${shipInfo.shipownerphone}</p>
                </c:otherwise>
                </c:choose>
        </div>
 </div>
</body>
</html>
