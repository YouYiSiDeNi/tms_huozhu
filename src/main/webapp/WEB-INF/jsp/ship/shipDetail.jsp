<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/23
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>TMS | ${shipInfo.shipname}详情</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <style type="text/css">
        body, html,#allmap {width: 100%;height: 100%;overflow: hidden;margin:0;font-family:"微软雅黑";}
    </style>
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <%--原网站支付--%>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=WnAUIrAmQK8oUf0a5tgKxqRfpAA59iCc"></script>
    <style>
        #detail_map_wrapper {
            height: 480px;
            background-color: #fff;
            font-size:12px;
            text-shadow: white 0 1px 0;
        }
    </style>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

    <!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
    <link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />


    <!-- layui -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">


</head>
<body onload="onStartMet(${shipInfo.shipid})">

<div class="wrapper" >
    <%@include file="../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../common/top.jsp"%>
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="main-center home-center">
                                <div class="main-wrap">
                                    <!--中间内容-->

                                    <div class="issue-detail-box">

                                        <div class="detail-left"  id="12player" style="display: none">
                                            <div id="swtcenter" class="video-head-pic"  style="z-index:4;"></div>
                                            <div class="detail-left">
                                                <div class="detail-head-shadow6">
                                                    <ul>
                                                        <li><a title="切换视频设备"></a>
                                                            <ul>
                                                                <li><a href="#" title="货物摄像设备" onclick="switchVideo('${shipInfo.deviceInfo.devicenumber}','${shipInfo.shipid}')">主设备(默认)</a></li>
                                                                <li><a href="#" title="驾驶舱实时视频" onclick="switchVideo('${shipInfo.deviceInfo.devicenumbertwo}','${shipInfo.shipid}')">辅设备</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <input class="detail-head-shadow2" title="点击播放视频" type="button" id="openBtn" onclick="openvideo('${shipInfo.deviceInfo.devicenumber}','${shipInfo.shipid}')" />
                                                <input class="detail-head-shadow3" title="停止播放视频" type="button" id="leaveBtn" disabled="disabled" onclick="leavevideo('${shipInfo.shipid}')" />
                                            </div>
                                        </div>



                                        <%--新设备--%>

                                            <div class="detail-left" id="3player" style="display: none">
                                                <div style="width: 100%; text-align: center;background-color: black">
                                                    <div style="margin-left: auto; margin-right: auto; width: 501px; background-color:black; text-align: center;">
                                                        <table style="margin:0px;padding:0px;border:none;">
                                                            <tr>
                                                                <td>
                                                                    <div id="main_video"
                                                                         style="margin-left: 0px;margin-top:0px; width: 501px; height: 330px; display: inline-block;">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <!-- 云台控制按钮 -->
                                                                    <div class="cloudDivCss">

                                                                        <div style="text-align:center;width:100%;" style="display: none">
                                                                            <table>
                                                                                <td align = "center">
                                                                                    <a class="BtnCss" onmousedown="SetServer('47.100.188.228', 39100)" style="display: none">连接平台</a>
                                                                                    <a class="BtnCss" onmousedown="ViewerLogin('wholeally', 'czFYScb5pAu+Ze7rXhGh//x9gYTzRxlHykybiYXfmlUG5hUR8MkXH/MBaOy+I6hqSFLfYeF/Q/M89wJitc018nNXswgYb2jNZv8EsCI3VJc=')" style="display: none">登录平台</a>
                                                                                    <%--<a class="BtnCss" onmousedown="StartView('${shipInfo.shipid}','${shipInfo.deviceInfo.devicenum}')" >观看设备</a>--%>
                                                                                    <%--<a class="BtnCss" onmousedown="StopView('${shipInfo.shipid}')">停止观看</a>--%>
                                                                                </td>
                                                                            </table>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div id="stdout" style="display: none"></div>
                                                                    <div class="video-loading" id="video-loading" style="position: absolute;top: 45%;right: 45%;display: none"><img src="http://download2.huizhaochuan.com.cn/5-121204193955-50.gif"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="detail-left">
                                                    <div class="detail-head-shadow6">
                                                        <ul>
                                                            <li><a title="切换视频设备"></a>
                                                                <ul>
                                                                    <li><a href="#" title="主设备视频" onclick="switchVideo('${shipInfo.deviceInfo.devicenumber}','${shipInfo.shipid}')">主设备(默认)</a></li>
                                                                    <li><a href="#" title="辅设备视频" onclick="switchVideo('${shipInfo.deviceInfo.devicenumbertwo}','${shipInfo.shipid}')">辅设备</a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <button class="detail-head-shadow2" title="点击播放视频" type="button" id="openBtn3" onclick="openvideo('${shipInfo.deviceInfo.devicenumber}','${shipInfo.shipid}')" /></button>
                                                    <button class="detail-head-shadow3" title="停止播放视频" type="button" id="leaveBtn3" disabled="disabled" onclick="StopView('${shipInfo.shipid}')" /></button>
                                                </div>
                                            </div>





                                        <div class="detail-right">
                                            <p class="detail-right-head">
                                                <i class="result-down-icon detail-down-icon"></i>船舶名称：${shipInfo.shipname}
                                            </p>

                                            <div class="detail-cont-out">

                                                <p class="list-cont-title2"><span class="source-till">船舶吨位:</span>${shipInfo.ton}吨</p>
                                                <p class="list-cont-title2"><span class="source-till">船舶尺寸:</span>长${shipInfo.shiplong}米，宽${shipInfo.shipwide}米</p>
                                                <p class="list-cont-title2"><span class="source-till">船舱尺寸:</span>长${shipInfo.cabinlong}米，宽${shipInfo.cabinwide}米</p>
                                                <p class="list-cont-title2"><span class="source-till">重载吃水深度:</span>${shipInfo.cabinheight}米</p>
                                                <p class="list-cont-title2"><span class="source-till">实时位置:</span>经度:${shipInfo.deviceInfo.longitude}，纬度:${shipInfo.deviceInfo.latitude}</p>
                                                <p class="list-cont-title2"><span class="source-till">数据最后上传时间:</span><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${shipInfo.deviceInfo.uptime}" /></p>
                                               <c:choose>
                                                <c:when test="${shipInfo.shipownerphone == '暂无'}">
                                                    <p class="list-cont-title2"><span class="source-till">船主电话:</span>数据收集中</p>
                                                </c:when>
                                                   <c:otherwise>
                                                       <p class="list-cont-title2"><span class="source-till">船主电话:</span>${shipInfo.shipownerphone}</p>
                                                   </c:otherwise>
                                               </c:choose>
                                                <p class="list-cont-title2" style="margin-top: 20px;font-size: 12px;">温馨提示：视频观看后请及时点击视频右下方<b>“停止”</b>按钮</p>


                                            </div>
                                        </div>

                                    </div>
                                    <div class="home-check4" id="guiji" style="margin-top: 40px">
                                        <label class="change-title">地图详情</label>
                                        <div class="change-pass-right2">
                                            <span class="shipmaptest shipmaptest-click" title="船舶现在的位置及风速等" onclick="showShipLocation()" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-map-marker"></i> 当前位置&nbsp;&nbsp;</span>
                                            <span class="shipmaptest" onclick="onday('1')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近1天&nbsp;&nbsp;</span>
                                            <span class="shipmaptest" onclick="onday('7')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近7天&nbsp;&nbsp;</span>
                                            <span class="shipmaptest" onclick="onday('10')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近10天&nbsp;&nbsp;</span>
                                            <span class="shipmaptest" onclick="onday('15')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近15天&nbsp;&nbsp;</span>
                                            <span class="shipmaptest" onclick="onday('30')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近30天&nbsp;&nbsp;</span>
                                        </div>
                                    </div>
                                    <div class="home-map" id="allmap"></div>
                                    <!--中间内容 end-->
                                    <div class="clear"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="../common/modal.jsp"%>


<!--申请续时弹框-->
<div class="alert-shadow">
    <div class="alert-login-box alert-play-box" style="display: none;">
        <p class="alert-login-title">观看时间不足，请充值!</p>
        <p class="alert-king-icon"></p>
        <p class="go-add-table">
            <span class="go-add-money" onclick="torecharge()">立即充值</span>
            <span class="go-add-money go-cancel-money">取消</span>
        </p>
    </div>
    <!--点击立即充值后弹窗-->
    <div class="alert-login-box3 alert-view-pay" style="display: none;">
        <p class="alert-login-title">请在打开新页面完成付款！</p>
        <p class="alert-king-icon"></p>
        <p class="go-add-table">
            <span class="go-add-money" onclick="window.location.reload();">我已付款</span>
            <span class="go-add-money go-cancel-money"><a href="http://www.huizhaochuan.com.cn/ship/toNews6.xhtml"> 付款遇到问题</a></span>
        </p>
    </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>

<!-- layui -->

<script type="text/javascript">
    document.getElementById("crumbs").innerHTML="船舶详情";
    $(function() {
        $(".shipmaptest").click(function () {
            $(".shipmaptest").removeClass("shipmaptest-click");
            $(this).addClass("shipmaptest-click");
        })
    })
    $(function(){
        //  申请续时按钮显示弹框
        $(".add-time-btn").click(function(){
            $(".alert-shadow").show();
            $(".alert-login-box").hide();
            $(".alert-apply-box").show();
        })
        //	关闭按钮隐藏弹框
        $(".alert-close").click(function(){
            $(".alert-shadow").hide();
        })
        //  立即申请续时按钮隐藏弹框
        $(".alert-login-btn").click(function(){
            $(".alert-shadow").hide();
        })
        //  点击播放视频按钮显示弹框
//        $(".detail-left").click(function(){
//            $(".alert-shadow").show();
//            $(".alert-login-box").hide();
//            $(".alert-login-box2").hide();
////            $(".alert-play-box").show();
//        })
        //  取消按钮隐藏弹框
        $(".go-cancel-money").click(function(){
            $(".alert-shadow").hide();
        })
        //  选择申请天数
        $(".day-item").click(function(){
            $(".day-item").removeClass("day-item-on");
            $(this).addClass("day-item-on");
        })
//        //点击查看按钮弹框
//        $(".detail-green-btn").click(function(){
//            $(".alert-shadow").show();
//            $(".alert-login-box").hide();
//            $(".alert-view-area").show();
//        })
    });

    function formatDate(date){
        var newTime = new Date(date);
        var year = newTime.getFullYear();
        var mon = newTime.getMonth() + 1;
        var day = newTime.getDate();
        var hour = newTime.getHours();
        var min = newTime.getMinutes();
        var sec = newTime.getSeconds();
        var newTimes = year + "-" + (mon < 10 ? ('0' + mon) : mon) + "-"
            + (day < 10 ? ('0' + day) : day) + " "
            +"\n"+ (hour < 10 ? ('0' + hour) : hour) + ":"
            + (min < 10 ? ('0' + min) : min) + ":"
            + (sec < 10 ? ('0' + sec) : sec);
        return newTimes;
    }



</script>

<script>


    function timetrans(date){
        var date = new Date(date);//如果date为13位不需要乘1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
        var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
        return Y+M+D+h+m+s;
    }
    // 定义一个有返回值的ajax函数供调用
    function ajaxCustom(url, data, method, async) {
        var result = null;
        $.ajax({
            url : url,
            data : data,
            type : method,
            async : async,
            contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            success : function(responseText) {
                result = responseText;
            },
            error : function(msg) {
                console.log(msg);
            },
            complete : function(XHR, TS) {
                XHR = null;
            }
        });
        return result;
    }

    var map = new BMap.Map("allmap");

    var point = new BMap.Point(${shipInfo.deviceInfo.longitude},${shipInfo.deviceInfo.latitude});
    map.centerAndZoom(point, 13);  // 编写自定义函数，创建标注
    map.addControl(new BMap.NavigationControl());
    map.enableScrollWheelZoom();//启动鼠标滚轮缩放地图
    map.enableKeyboard();//启动键盘操作地图
    //    去除公路网
    map.setMapStyle({
        styleJson:[
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": {
                    "color": "#ffffff",
                    "visibility": "off"
                }
            },

        ]
    });


    // 定义自定义覆盖物的构造函数
    function SquareOverlay(center, length){
        this._center = center;
        this._length = length;
    }
    // 继承API的BMap.Overlay
    SquareOverlay.prototype = new BMap.Overlay();

    // 实现初始化方法
    SquareOverlay.prototype.initialize = function(map){

        // 保存map对象实例
        this._map = map;
        // 外层容器
        var shipMainWrapper = document.createElement("div");
        shipMainWrapper.style.height    = this._length + "px";
        shipMainWrapper.style.width     = this._length + "px";
        shipMainWrapper.style.textAlign = "center";
        shipMainWrapper.style.position  = "absolute";
        //船舶图片容器
        var shipImgWrapper = document.createElement("div");
        var shipImg = document.createElement("img");
        shipImg.src = "${pageContext.request.contextPath}/assets/img/kong.png";
        if (this._center.isEmpty == "1")
        {
            shipImg.src = "${pageContext.request.contextPath}/assets/img/man.png";
        }
        shipImgWrapper.appendChild(shipImg);
        //船舶名
        var shipName = document.createElement("div");
        shipName.style.fontSize   = "14px";
        shipName.style.position   = "absolute";
        shipName.style.left       = "-73px";
        shipName.style.top        = "35px";
        shipName.style.height     = "16px";
        shipName.style.lineHeight = "16px";
        shipName.style.width      = "180px";
        shipName.innerText = this._center.shipName;
        if (this._center.addDate)
        {
            shipName.innerText =timetrans(this._center.addDate) ;
        }

        //组装
        shipMainWrapper.appendChild(shipImgWrapper);
        shipMainWrapper.appendChild(shipName);
        //显示更多船舶信息
//        if (this._center.moreDetail == "1")
//        {
//            var shipDetailWrapper = document.createElement("div");
//            shipDetailWrapper.style.padding         = "10px";
//            shipDetailWrapper.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
//            shipDetailWrapper.style.textAlign       = "left";
//            shipDetailWrapper.style.color           = "#fff";
//            shipDetailWrapper.style.fontSize        = "14px";
//            shipDetailWrapper.style.position        = "absolute";
//            shipDetailWrapper.style.left            = "-83px";
//            shipDetailWrapper.style.top             = "51px";
//            shipDetailWrapper.style.width           = "200px";
//            shipDetailWrapper.innerHTML = "船舶名称：" + this._center.shipName +  "<br/>经度：" + this._center.lng + "<br/>纬度：" + this._center.lat;
//            shipMainWrapper.appendChild(shipDetailWrapper);
//        }

        // 将div添加到覆盖物容器中
        map.getPanes().markerPane.appendChild(shipMainWrapper);
        // 保存div实例
        this._div = shipMainWrapper;
        // 需要将div元素作为方法的返回值，当调用该覆盖物的show、
        // hide方法，或者对覆盖物进行移除时，API都将操作此元素。
        return shipMainWrapper;

    }

    // 实现绘制方法
    SquareOverlay.prototype.draw = function(){
        // 根据地理坐标转换为像素坐标，并设置给容器
        var position = this._map.pointToOverlayPixel(this._center);
        this._div.style.left = position.x - this._length / 2 + "px";
        this._div.style.top = position.y - this._length / 2 + "px";
    }

    // 实现显示方法
    SquareOverlay.prototype.show = function(){
        if (this._div){
            this._div.style.display = "";
        }
    }
    // 实现隐藏方法
    SquareOverlay.prototype.hide = function(){
        if (this._div){
            this._div.style.display = "none";
        }
    }

    // 显示船舶位置
    function showShipLocation()
    {
        map.clearOverlays();
        var currShip = {};

        currShip.shipName   = "${shipInfo.shipname}";
        currShip.isEmpty    = "${shipInfo.isemtiy}";
        currShip.lng        = ${shipInfo.deviceInfo.longitude};
        currShip.lat        = ${shipInfo.deviceInfo.latitude};
        currShip.moreDetail = "1"; //显示更多信息

        var mySquare = new SquareOverlay(currShip, 35);
        map.addOverlay(mySquare);
        map.centerAndZoom(point, 13);  // 编写自定义函数，创建标注
    }

    // 显示航行轨迹
    function onday(x){
        map.clearOverlays();
        var ships = [];
        var ships_base_info = [];
        ship_id = "";
        var shipid =  '${shipInfo.shipid}';
        var accuontUrl = 'drawTrackhis.do?shipid='+ shipid + '&day=' + x;
        var accuontData = '';
        var accuontJson = $.parseJSON(ajaxCustom(accuontUrl,accuontData, 'POST', false));


        for (accuont in accuontJson) {

            ships.push(
                    new BMap.Point(accuontJson[accuont]['longitude'], accuontJson[accuont]['latitude'])
            );
            ships_base_info.push({
                lat:accuontJson[accuont]['latitude'],
                lng:accuontJson[accuont]['longitude'],
                isEmpty:accuontJson[accuont]['isemtiy'],
                shipName:accuontJson[accuont]['shipname'],
                addDate:accuontJson[accuont]['adddate'],
                moreDetail:"0"
            });
        }
        //显示小船
        for (var i = 0; i < ships_base_info.length; i++) {
            var mySquare = new SquareOverlay(ships_base_info[i], 35);
            map.addOverlay(mySquare);
        }
        //创建折线
        var polyline = new BMap.Polyline(ships, {strokeColor:"red", strokeWeight:1, strokeOpacity:0.8});
        map.addOverlay(polyline);
    }

    $(function(){
        showShipLocation();
    })

</script>


<script>
    function torecharge() {
        $(".alert-play-box").hide();
        $(".alert-view-pay").show();
        window.open("toReChargePay.do");
    }


</script>

<%--播放视频iframe JS代码--%>
<script>
    var seconds = ${resecond};
    var flag = false;

    function openvideo(devicenumber,ship_id) {
        $.ajax({
            url: 'selectDeviceIsthreegeneration.do?shipid='+ship_id,
            type: 'post',
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data == 3){
                    openvideo3(devicenumber,ship_id);
                }else{
                    openvideo2(devicenumber,ship_id);
                }
            }
        });
    }

    function openvideo3(devicenumber,ship_id) {

        $.ajax({
            url: 'selectDeviceVoidurl.do?devicenumber='+ devicenumber,
            type: 'post',
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.ret==0){
                    StartView(data.devicenum,ship_id);
                }else{
                    alert("当前设备不在线！");
                }
            }
        });
    }

    // 播放视频
    function openvideo2(devicenumber,ship_id) {
        $.ajax({
            url: 'deviceisonline.do?devicenumber='+ devicenumber+'&shipid='+ship_id,
            type: 'post',
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data==0){
                    openvideo1(devicenumber,ship_id);
                }else{
                    alert("当前设备不在线！")
                }
            }
        });
    }


    function openvideo1(devicenumber,ship_id){
        if (seconds<=0){
            $(".alert-shadow").show();
            $(".alert-login-box").hide();
            $(".alert-login-box2").hide();
            $(".alert-login-box3").hide();
            $(".alert-play-box").show();
        }else {
            if (!flag){
                document.getElementById("openBtn").disabled = true;
                document.getElementById("leaveBtn").disabled = false;
                settime(ship_id);
                $.ajax({
                    url:'startVideo.do?shipid='+ship_id,
                    type:'post',
                    dataType:'json',
                    async:true,
                    contentType: "application/json; charset=utf-8",
                    success:  function(data) {
                        flag = true;
                        var dataStr = data + "";
                        if(dataStr == "true"){
                            var inserthtml = '<iframe id="swtframe" src="http://121.196.195.35:8070/index.html?UID=' + devicenumber + '" name="swtframe" width="560" height="485"  frameborder="0" scrolling="no" ></iframe>';
                            $("#swtcenter").append(inserthtml);
                        }
                    },
                    error  :  function(XMLHttpRequest, textStatus, errorThrown) {
                        // debugger;
                        alert('网络不好，请稍后尝试...');
                    }
                });

            }else {
                alert('正在播放视频，请先关闭...');
            }
        }
    }

    /**
     *开始观看设备
     */
    function StartView(devicenum,ship_id){
        var seconds = ${resecond};
        var flag = false;
        if (seconds<=0){
            $(".alert-shadow").show();
            $(".alert-login-box").hide();
            $(".alert-login-box2").hide();
            $(".alert-login-box3").hide();
            $(".alert-play-box").show();
        }else {
            if (!flag){
                document.getElementById("openBtn3").disabled = true;
                document.getElementById("leaveBtn3").disabled = false;
                $("#video-loading").show();
                settime(ship_id);
                $.ajax({
                    url:'startVideo.do?shipid='+ship_id,
                    type:'post',
                    dataType:'json',
                    async:true,
                    contentType: "application/json; charset=utf-8",
                    success:  function(data) {
                        flag = true;
                        var dataStr = data + "";
                        if(dataStr == "true"){
                            player = swfobject.getObjectById('player');
                            player.StartView(devicenum);
                        }
                    },
                    error  :  function(XMLHttpRequest, textStatus, errorThrown) {
                        // debugger;
                        alert('网络不好，请稍后尝试...');
                    }
                });

            }else {
                alert('正在播放视频，请先关闭...');
            }
        }


    }


    /**
     *停止观看设备
     */
    function StopView(ship_id){
        $("#video-loading").hide();
        document.getElementById("openBtn3").disabled = false;
        document.getElementById("leaveBtn3").disabled = true;
        player = swfobject.getObjectById('player');
        player.StopView();
        //
        $.ajax({
            url:'endVideo.do?shipid=' + ship_id,
            type:'post',
            dataType:'json',
            async:true,
            contentType: "application/json; charset=utf-8",
            success:  function(data) {
                flag = false;
                seconds = parseInt(data, 10);
            },
            error  :  function(XMLHttpRequest, textStatus, errorThrown) {
                // debugger;
                alert('error');
            }
        });



    }



    // 关闭视频
    function leavevideo(ship_id){
        document.getElementById("openBtn").disabled = false;
        document.getElementById("leaveBtn").disabled = true;
        $("#swtcenter :first-child").remove();

        //
        $.ajax({
            url:'endVideo.do?shipid=' + ship_id,
            type:'post',
            dataType:'json',
            async:true,
            contentType: "application/json; charset=utf-8",
            success:  function(data) {
                flag = false;
                seconds = parseInt(data, 10);
            },
            error  :  function(XMLHttpRequest, textStatus, errorThrown) {
                // debugger;
                alert('error');
            }
        });
    }

    //计时
    function settime(ship_id) {
        if (seconds <= 0) {
            leavevideo(ship_id);
            return;
        } else {
            seconds--;
        }
        setTimeout(function() {
            settime(ship_id)
        },1000)
    }
    //切换视频
    function switchVideo(devicenumber,ship_id){
        if(devicenumber == null || devicenumber == "" || devicenumber.length == 0){
            alert("序列号为空");
        }else {
            openvideo(devicenumber,ship_id);
        }
    }

    //切换视频3带设备
    //    function switchVideo3(ship_id,chnID){
    //        if(chnID == null || chnID == "" || chnID.length == 0){
    //            alert("序列号为空");
    //        }else {
    //            StartView(ship_id,chnID);
    //        }
    //    }
</script>

<script type="text/javascript">
    function turnUp() {
        clickBtn(1);

        ctrlPTZ(0,6);
    }
    function turnLeft() {
        clickBtn(2);

        ctrlPTZ(0,8);
    }
    function turnRight() {
        clickBtn(3);

        ctrlPTZ(0,9);
    }
    function turnDown() {
        clickBtn(4);

        ctrlPTZ(0,7);
    }
    function stopMove() {
        ctrlPTZ(0,14);
    }
</script>

<script src="${pageContext.request.contextPath}/assets/video/js/swfobject.js"></script>
<script type="text/javascript">
    function onStartMet(ship_id){
//        判断几代设备
        $.ajax({
            url: 'selectDeviceIsthreegeneration.do?shipid='+ship_id,
            type: 'post',
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data == 3){
                    $("#12player").hide();
                    $("#3player").show();

                }else{
                    $("#12player").show();
                    $("#3player").hide();
                }
            }
        });


        //这里还要添加flash
        var flashvars = {
            playerID:'8'
            ///*下面的五个参数，如果有传的话，页面是直接打开画面的，但是要传的对
            //*/
        };
        var params = {
            allowScriptAccess:'always'
            ,allowFullscreen: "true"
            ,scale: "exactFit"   //填充模式，以后大小由网页来调整
        };
        var attributes = { id:'player', name:'player' };
        var divObj = document.getElementById("main_video");
        var wVideo = document.getElementById("main_video").style.width;
        var hVideo = document.getElementById("main_video").style.height;


        swfobject.embedSWF('.${pageContext.request.contextPath}/assets/video/SLPlayer.swf?ver='+Math.random(), 'main_video', wVideo,hVideo, '9.0.0','.${pageContext.request.contextPath}/assets/video/expressInstall.swf', flashvars, params, attributes);
    }
    var player;
    /**************************************************************下面是flash会调用js脚本的所有方法***********************************************************************/
    /**
     *flash播放器内部初始化完就会调用这个js接口
     */
    function onIniOk(){//初始化播放器结束，如果有本方法，播放会在初始化结束之后调用这里的js方法
        player = swfobject.getObjectById('player');
        player.setDebugVisible(false);  //打印连接调试状态
        player.SetServer("47.100.188.228",39100);
        player.ViewerLogin("wholeally", "czFYScb5pAu+Ze7rXhGh//x9gYTzRxlHykybiYXfmlUG5hUR8MkXH/MBaOy+I6hqSFLfYeF/Q/M89wJitc018nNXswgYb2jNZv8EsCI3VJc=");
        Print("播放器初始化结束开始连接平台。。。");
    }

    /**
     *flash播放器在连接平台之后每次的服务变化都会调用这里,例如：第一次登录成功，会调用这里，以后断开、重连成功都会调用这里
     */
    function onServiceChange(jsonParam){//
        var  param = JSON.parse((jsonParam));
        Print("平台服务变化："+param.result);
    }

    function onViewBackResult(jsonParam){//申请回放结果，如果中间连接断开是会调用其他js方法（比如回放转发断开）
        player = swfobject.getObjectById('player');
        var  param = JSON.parse((jsonParam));
        Print("申请回放返回结果："+param.result);
    }
    function onLoginFail(){
        Print("登录平台失败！");
    }
    function onViewFail(){
        Print("观看设备失败！");
    }
    function onViewClose(){
        Print("观看会话关闭！");
    }

    function onViewShow(){
        $("#video-loading").hide();
        Print("画面即将出来，可以隐藏连接动画了！");
    }

    function onViewBackFail(){
        Print("回放录像失败！");
    }
    function onViewBackClose(){
        Print("回放会话关闭！");
    }
    function onViewBackResult(jsonParam){
        var  param = JSON.parse((jsonParam));
        Print("申请回放结果："+param.result);
    }
    function onViewBackResult(jsonParam){
        var  param = JSON.parse((jsonParam));
        Print("申请回放结果："+param.result);
    }
    function onQueryDayIndexResult(jsonParam){
        var  param = JSON.parse((jsonParam));
        Print("查询天索引结果："+param.result);
        Print("查询天索引内容："+param.dayInfo);
    }
    function onQuery24HIndexResult(jsonParam){
        var  param = JSON.parse((jsonParam));
        Print("查询24小时索引结果："+param.result);
        Print("查询24小时索引内容："+param.indx24HInfo);
    }
    function onPlayCtrlResult(jsonParam){
        var  param = JSON.parse((jsonParam));
        Print("播放控制结果："+param.result);
    }

    /*************************************************************************************************************************************/

    /**************************************下面是flash提供js脚本调用的所有方法示例（跟按钮上的方法是一对一的）**********************************************/
    /**
     *设定平台地址
     */
    function SetServer(ip,port){
        player = swfobject.getObjectById('player');
        player.SetServer(ip, port);
    }
    /**
     *观看端登录平台
     */
    function ViewerLogin(appID,userAuth){
        player = swfobject.getObjectById('player');
        player.ViewerLogin(appID, userAuth);
    }


    /**
     *云台操作
     * type  操作类型：
     * ctrlCmd  操作指令
     */
    function CtrlPtz(type,ctrlCmd){
        player = swfobject.getObjectById('player');
        player.CtrlPtz(type,ctrlCmd);
    }
    /**
     *设定播放器缓存时间（单位秒）
     */
    function SetBufferTime(bufferTime){
        player = swfobject.getObjectById('player');
        player.SetBufferTime(bufferTime);
    }

    /**
     * 开始录像回放
     * @param   chnID
     * @param   cloud    是否从云存中取  0:从设备端读取,1:从云存读取
     */
    function StartViewBack(chnID,cloud){
        player = swfobject.getObjectById('player');
        player.StartViewBack(chnID,cloud);
    }

    /**
     *停止录像回放
     */
    function StopViewBack(){
        player = swfobject.getObjectById('player');
        player.StopViewBack();
    }
    /**
     * 查询指定子设备的天概要索引
     * @param	chnID   子设备号
     * @param	year    年
     * @param	month   月
     * @param	t_zone  时区
     * @param	cloud   是否从云存中取  0:从设备端读取,1:从云存读取
     */
    function QueryDayIndex(chnID,year,month,t_zone,cloud){
        player = swfobject.getObjectById('player');
        player.QueryDayIndex(chnID,year,month,t_zone,cloud);
    }
    /**
     * 查询24小时索引文件信息
     * @param	chnID  子设备id
     * @param	year   年
     * @param	month  月
     * @param	day    日
     * @param	hour   小时
     * @param	minute  分钟
     * @param	sec     秒
     */
    function Query24HIndex(chnID,year,month,day,huor,minute,sec){
        player = swfobject.getObjectById('player');
        player.Query24HIndex(chnID,year,month,day,huor,minute,sec);
    }

    /**
     * 回放控制：
     * @param	year    年
     * @param	month   月
     * @param	day     日
     * @param	hour    小时
     * @param	min     分钟
     * @param	sec     秒
     * @param	playType 1播放;0停止;2暂停; 3继续
     */
    function PlayBackCtrl(year, month, day, hour, min, sec, playType){
        player = swfobject.getObjectById('player');
        player.PlayBackCtrl(year, month, day, hour, min, sec, playType);
    }


    /**
     *显示关闭调试面板
     */
    function setDebugVisible(flag){
        player = swfobject.getObjectById('player');
        player.setDebugVisible(flag);
    }

    /*************************************************************************************************************************************/
</script>
<script language=javascript>
    function clickBtn(idex)
    {
        var curId = "btn"+idex;
        var obj = document.getElementById(curId);
        if(obj == undefined || obj == null){return;}

        // reset
        resetBtnState();

        if(idex == 0)
        {
            obj.style.background='url("assets/video/imgs/start_btn_pressed.png")';
            if (open == true) {
                obj.style.background='url("assets/video/imgs/start_btn_normal.png")';
            }

        }
        else if(idex == 1)
        {
            obj.style.background='url("assets/video/imgs/up_pressed.png")';
        }
        else if(idex == 2)
        {
            obj.style.background='url("assets/video/imgs/left_pressed.png")';
        }
        else if(idex == 3)
        {
            obj.style.background='url("assets/video/imgs/right_pressed.png")';
        }
        else if(idex == 4)
        {
            obj.style.background='url("assets/video/imgs/down_pressed.png")';
        }
    }
    function resetBtnState()
    {
        var btn0 = document.getElementById("btn0");
        var btn1 = document.getElementById("btn1");
        var btn2 = document.getElementById("btn2");
        var btn3 = document.getElementById("btn3");
        var btn4 = document.getElementById("btn4");

        btn0.style.background='url("assets/video/imgs/start_btn_normal.png")';
        btn1.style.background='url("assets/video/imgs/up_normal.png")';
        btn2.style.background='url("assets/video/imgs/left_normal.png")';
        btn3.style.background='url("assets/video/imgs/right_normal.png")';
        btn4.style.background='url("assets/video/imgs/down_normal.png")';
    }

    var open;
    function startOrClose() {
        clickBtn(0);

        if (open == false || open == undefined) {
            play();
            changeShowToClose();
        } else {
            stop();
            changeShowToStart();
        }
    }
    function changeShowToClose()
    {
        var obj = document.getElementById("btn0");
        obj.innerHTML="开/关";
    }
    function changeShowToStart()
    {
        var obj = document.getElementById("btn0");
        obj.innerHTML="开/关";
    }

    function Print(str) {
        var stdout = document.getElementById('stdout');
        stdout.innerHTML += str + '<br>';
    }
</script>


</html>
