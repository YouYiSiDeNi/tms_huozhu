<%--
  Created by haojianlei.
  User: Administrator
  Date: 2018/4/23
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>TMS | ${shipInfo.shipname}视频播放</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>

</head>
<body bgcolor="#202020">
<div class="new-video">
    <div class="detail-left2">
        <div id="swtcenter" class="video-head-pic"  style="z-index:4;"></div>
        <div class="detail-left2">
            <div class="detail-head-shadow6">
                <ul>
                    <li><a title="切换视频设备"></a>
                        <ul>
                            <li><a href="#" title="主设备视频" onclick="switchVideo('${shipInfo.deviceInfo.devicenumber}','${shipInfo.shipid}')">主设备(默认)</a></li>
                            <li><a href="#" title="辅设备视频" onclick="switchVideo('${shipInfo.deviceInfo.devicenumbertwo}','${shipInfo.shipid}')">辅设备</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <button class="detail-head-shadow2" title="点击播放视频" type="button" id="openBtn" onclick="openvideo('${shipInfo.deviceInfo.devicenumber}','${shipInfo.shipid}')" /></button>
            <button class="detail-head-shadow3" title="停止播放视频" type="button" id="leaveBtn" disabled="disabled" onclick="leavevideo('${shipInfo.shipid}')" /></button>
        </div>
    </div>
    <div class="video-title">${shipInfo.shipname}</div>
</div>

<!--申请续时弹框-->
<div class="alert-shadow">
    <div class="alert-login-box alert-play-box" style="display: none;">
        <div class="alert-close"></div>
        <p class="alert-login-title">观看时间不足，请充值!</p>
        <p class="alert-king-icon"></p>
        <p class="go-add-table">
            <span class="go-add-money" onclick="torecharge()">立即充值</span>
            <span class="go-add-money go-cancel-money">取消</span>
        </p>
    </div>
    <!--点击立即充值后弹窗-->
    <div class="alert-login-box3 alert-view-pay" style="display: none;">
        <div class="alert-close"></div>
        <p class="alert-login-title">请在打开新页面完成付款！</p>
        <p class="alert-king-icon"></p>
        <p class="go-add-table">
            <span class="go-add-money" onclick="window.location.reload();">我已付款</span>
            <span class="go-add-money go-cancel-money"><a href="http://www.huizhaochuan.com.cn/ship/toNews6.xhtml"> 付款遇到问题</a></span>
        </p>
    </div>
    <div class="alert-login-box3 alert-video-play" style="display: none;">
        <div class="alert-close"></div>
        <p class="alert-login-title">页面查看时间过长已为您自动关闭！</p>
        <p class="alert-king-icon2"></p>
        <p class="go-add-table">
            <span class="go-add-money" onclick="window.location.reload();">返回观看</span>
            <span class="go-add-money go-cancel-money" onclick="window.close()">关闭页面</span>
        </p>
    </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>


<script type="text/javascript">

    $(function(){
        //  申请续时按钮显示弹框
        $(".add-time-btn").click(function(){
            $(".alert-shadow").show();
            $(".alert-login-box").hide();
            $(".alert-apply-box").show();
        })
        //	关闭按钮隐藏弹框
        $(".alert-close").click(function(){
            $(".alert-shadow").hide();
        })
        //  立即申请续时按钮隐藏弹框
        $(".alert-login-btn").click(function(){
            $(".alert-shadow").hide();
        })

        //  取消按钮隐藏弹框
        $(".go-cancel-money").click(function(){
            $(".alert-shadow").hide();
        })
        //  选择申请天数
        $(".day-item").click(function(){
            $(".day-item").removeClass("day-item-on");
            $(this).addClass("day-item-on");
        })

    });

    function formatDate(date){
        var newTime = new Date(date);
        var year = newTime.getFullYear();
        var mon = newTime.getMonth() + 1;
        var day = newTime.getDate();
        var hour = newTime.getHours();
        var min = newTime.getMinutes();
        var sec = newTime.getSeconds();
        var newTimes = year + "-" + (mon < 10 ? ('0' + mon) : mon) + "-"
                + (day < 10 ? ('0' + day) : day) + " "
                +"\n"+ (hour < 10 ? ('0' + hour) : hour) + ":"
                + (min < 10 ? ('0' + min) : min) + ":"
                + (sec < 10 ? ('0' + sec) : sec);
        return newTimes;
    }

</script>

<script>
    function torecharge() {
        $(".alert-play-box").hide();
        $(".alert-view-pay").show();
        window.open("toReChargePay.do");
    }


</script>

<%--播放视频iframe JS代码--%>
<script>

    var seconds = ${resecond};
    var flag = false;


    function openvideo(devicenumber,ship_id) {

        $.ajax({
            url: 'selectDeviceIsthreegeneration.do?shipid='+ship_id,
            type: 'post',
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data == 3){
                    openvideo3(devicenumber,ship_id);
                }else{
                    openvideo2(devicenumber,ship_id);
                }
            }
        });
    }


    function openvideo3(devicenumber,ship_id) {

        $.ajax({
            url: 'selectDeviceVoidurl.do?devicenumber='+ devicenumber,
            type: 'post',
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if(data.ret==0){
                    openvideo4(data.html5url,ship_id);
//                  alert(data.html5url)
                }else{
                    alert("当前设备不在线！");
                }
            }
        });
    }



    // 播放视频
    function openvideo2(devicenumber,ship_id) {
        $.ajax({
            url: 'deviceisonline.do?devicenumber='+ devicenumber+'&shipid='+ship_id,
            type: 'post',
            dataType: 'json',
            async: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data==0){
                    openvideo1(devicenumber,ship_id);
                }else{
                    alert("当前设备不在线！")
                }
            }
        });
    }


    function openvideo1(devicenumber,ship_id){
        if (seconds<=0){
            $(".alert-shadow").show();
            $(".alert-login-box").hide();
            $(".alert-login-box2").hide();
            $(".alert-login-box3").hide();
            $(".alert-play-box").show();
        }else {
            if (!flag){
                document.getElementById("openBtn").disabled = true;
                document.getElementById("leaveBtn").disabled = false;
                settime(ship_id);
                $.ajax({
                    url:'startVideo.do?shipid='+ship_id,
                    type:'post',
                    dataType:'json',
                    async:true,
                    contentType: "application/json; charset=utf-8",
                    success:  function(data) {
                        flag = true;
                        var dataStr = data + "";
                        if(dataStr == "true"){
                            var inserthtml = '<iframe id="swtframe" src="http://121.196.195.35:8070/index.html?UID=' + devicenumber + '" name="swtframe" width="560" height="485"  frameborder="0" scrolling="no" ></iframe>';
                            $("#swtcenter").append(inserthtml);
                        }
                    },
                    error  :  function(XMLHttpRequest, textStatus, errorThrown) {
                        // debugger;
                        alert('网络不好，请稍后尝试...');
                    }

                });


            }else {
                alert('正在播放视频，请先关闭...');
            }
        }
    }

    function openvideo4(html5url,ship_id){
        if (seconds<=0){
            $(".alert-shadow").show();
            $(".alert-login-box").hide();
            $(".alert-login-box2").hide();
            $(".alert-login-box3").hide();
            $(".alert-play-box").show();
        }else {
            if (!flag){
                document.getElementById("openBtn").disabled = true;
                document.getElementById("leaveBtn").disabled = false;
                settime(ship_id);
                $.ajax({
                    url:'startVideo.do?shipid='+ship_id,
                    type:'post',
                    dataType:'json',
                    async:true,
                    contentType: "application/json; charset=utf-8",
                    success:  function(data) {
                        flag = true;
                        var dataStr = data + "";
                        if(dataStr == "true"){
                            var inserthtml = '<iframe id="swtframe" src="' + html5url + '" name="swtframe" width="560" height="485"  frameborder="0" scrolling="no" ></iframe>';
                            $("#swtcenter").append(inserthtml);
                        }
                    },
                    error  :  function(XMLHttpRequest, textStatus, errorThrown) {
                        // debugger;
                        alert('网络不好，请稍后尝试...');
                    }
                });


            }else {
                alert('正在播放视频，请先关闭...');
            }
        }
    }



    // 关闭视频
    function leavevideo(ship_id){
        document.getElementById("openBtn").disabled = false;
        document.getElementById("leaveBtn").disabled = true;
        $("#swtcenter :first-child").remove();

        //
        $.ajax({
            url:'endVideo.do?shipid=' + ship_id,
            type:'post',
            dataType:'json',
            async:true,
            contentType: "application/json; charset=utf-8",
            success:  function(data) {
                flag = false;
                seconds = parseInt(data, 10);
            },
            error  :  function(XMLHttpRequest, textStatus, errorThrown) {
                // debugger;
                alert('error');
            }
        });
    }

    //计时
    function settime(ship_id) {
        if (seconds <= 0) {
            leavevideo(ship_id);
            return;
        } else {
            seconds--;
        }
        setTimeout(function() {
            settime(ship_id)
        },1000)
    }
    //切换视频
    function switchVideo(devicenumber,ship_id){
        if(devicenumber == null || devicenumber == "" || devicenumber.length == 0){
            alert("序列号为空");
        }else {
            openvideo(devicenumber,ship_id);
        }
    }
</script>

</html>
