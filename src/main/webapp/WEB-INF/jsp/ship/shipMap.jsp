<%--
  Created by haojianlei.
  User: Administrator
  Date: 2018/4/23
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>TMS | ${shipInfo.shipname}详情</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <style type="text/css">
        body, html,#allmap {width: 100%;height:100%;overflow: hidden;margin:0;font-family:"微软雅黑";}
    </style>


    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=WnAUIrAmQK8oUf0a5tgKxqRfpAA59iCc"></script>
    <style>
        #detail_map_wrapper {
            height: 600px;
            background-color: #fff;
            font-size:12px;
            text-shadow: white 0 1px 0;
        }
    </style>

</head>
<body>
      <div class="change-pass-right2">
           <span class="shipmaptest shipmaptest-click" title="船舶现在的位置及风速等" onclick="showShipLocation()" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-map-marker"></i> 当前位置&nbsp;&nbsp;</span>
           <span class="shipmaptest" onclick="onday('1')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近1天&nbsp;&nbsp;</span>
           <span class="shipmaptest" onclick="onday('7')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近7天&nbsp;&nbsp;</span>
          <span class="shipmaptest" onclick="onday('10')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近10天&nbsp;&nbsp;</span>
            <span class="shipmaptest" onclick="onday('15')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近15天&nbsp;&nbsp;</span>
           <span class="shipmaptest" onclick="onday('30')" style="font-weight: 600">&nbsp;&nbsp;<i class="pe-7s-clock"></i>最近30天&nbsp;&nbsp;</span>
       </div>
            <div class="home-map" id="allmap"></div>
      <div class="clear"></div>

</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $(".shipmaptest").click(function () {
            $(".shipmaptest").removeClass("shipmaptest-click");
            $(this).addClass("shipmaptest-click");
        })
    })

    function formatDate(date){
        var newTime = new Date(date);
        var year = newTime.getFullYear();
        var mon = newTime.getMonth() + 1;
        var day = newTime.getDate();
        var hour = newTime.getHours();
        var min = newTime.getMinutes();
        var sec = newTime.getSeconds();
        var newTimes = year + "-" + (mon < 10 ? ('0' + mon) : mon) + "-"
                + (day < 10 ? ('0' + day) : day) + " "
                +"\n"+ (hour < 10 ? ('0' + hour) : hour) + ":"
                + (min < 10 ? ('0' + min) : min) + ":"
                + (sec < 10 ? ('0' + sec) : sec);
        return newTimes;
    }



</script>

<script>


    function timetrans(date){
        var date = new Date(date);//如果date为13位不需要乘1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
        var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
        return Y+M+D+h+m+s;
    }
    // 定义一个有返回值的ajax函数供调用
    function ajaxCustom(url, data, method, async) {
        var result = null;
        $.ajax({
            url : url,
            data : data,
            type : method,
            async : async,
            contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            success : function(responseText) {
                result = responseText;
            },
            error : function(msg) {
                console.log(msg);
            },
            complete : function(XHR, TS) {
                XHR = null;
            }
        });
        return result;
    }

    var map = new BMap.Map("allmap");

    var point = new BMap.Point(${shipInfo.deviceInfo.longitude},${shipInfo.deviceInfo.latitude});
    map.centerAndZoom(point, 13);  // 编写自定义函数，创建标注
    map.addControl(new BMap.NavigationControl());
    map.enableScrollWheelZoom();//启动鼠标滚轮缩放地图
    map.enableKeyboard();//启动键盘操作地图
    //    去除公路网
    map.setMapStyle({
        styleJson:[
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": {
                    "color": "#ffffff",
                    "visibility": "off"
                }
            },

        ]
    });


    // 定义自定义覆盖物的构造函数
    function SquareOverlay(center, length){
        this._center = center;
        this._length = length;
    }
    // 继承API的BMap.Overlay
    SquareOverlay.prototype = new BMap.Overlay();

    // 实现初始化方法
    SquareOverlay.prototype.initialize = function(map){

        // 保存map对象实例
        this._map = map;
        // 外层容器
        var shipMainWrapper = document.createElement("div");
        shipMainWrapper.style.height    = this._length + "px";
        shipMainWrapper.style.width     = this._length + "px";
        shipMainWrapper.style.textAlign = "center";
        shipMainWrapper.style.position  = "absolute";
        //船舶图片容器
        var shipImgWrapper = document.createElement("div");
        var shipImg = document.createElement("img");
        shipImg.src = "${pageContext.request.contextPath}/assets/img/kong.png";
        if (this._center.isEmpty == "1")
        {
            shipImg.src = "${pageContext.request.contextPath}/assets/img/man.png";
        }
        shipImgWrapper.appendChild(shipImg);
        //船舶名
        var shipName = document.createElement("div");
        shipName.style.fontSize   = "14px";
        shipName.style.position   = "absolute";
        shipName.style.left       = "-73px";
        shipName.style.top        = "35px";
        shipName.style.height     = "16px";
        shipName.style.lineHeight = "16px";
        shipName.style.width      = "180px";
        shipName.innerText = this._center.shipName;
        if (this._center.addDate)
        {
            shipName.innerText =timetrans(this._center.addDate) ;
        }

        //组装
        shipMainWrapper.appendChild(shipImgWrapper);
        shipMainWrapper.appendChild(shipName);
        //显示更多船舶信息
//        if (this._center.moreDetail == "1")
//        {
//            var shipDetailWrapper = document.createElement("div");
//            shipDetailWrapper.style.padding         = "10px";
//            shipDetailWrapper.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
//            shipDetailWrapper.style.textAlign       = "left";
//            shipDetailWrapper.style.color           = "#fff";
//            shipDetailWrapper.style.fontSize        = "14px";
//            shipDetailWrapper.style.position        = "absolute";
//            shipDetailWrapper.style.left            = "-83px";
//            shipDetailWrapper.style.top             = "51px";
//            shipDetailWrapper.style.width           = "200px";
//            shipDetailWrapper.innerHTML = "船舶名称：" + this._center.shipName +  "<br/>经度：" + this._center.lng + "<br/>纬度：" + this._center.lat;
//            shipMainWrapper.appendChild(shipDetailWrapper);
//        }

        // 将div添加到覆盖物容器中
        map.getPanes().markerPane.appendChild(shipMainWrapper);
        // 保存div实例
        this._div = shipMainWrapper;
        // 需要将div元素作为方法的返回值，当调用该覆盖物的show、
        // hide方法，或者对覆盖物进行移除时，API都将操作此元素。
        return shipMainWrapper;

    }

    // 实现绘制方法
    SquareOverlay.prototype.draw = function(){
        // 根据地理坐标转换为像素坐标，并设置给容器
        var position = this._map.pointToOverlayPixel(this._center);
        this._div.style.left = position.x - this._length / 2 + "px";
        this._div.style.top = position.y - this._length / 2 + "px";
    }

    // 实现显示方法
    SquareOverlay.prototype.show = function(){
        if (this._div){
            this._div.style.display = "";
        }
    }
    // 实现隐藏方法
    SquareOverlay.prototype.hide = function(){
        if (this._div){
            this._div.style.display = "none";
        }
    }

    // 显示船舶位置
    function showShipLocation()
    {
        map.clearOverlays();
        var currShip = {};

        currShip.shipName   = "${shipInfo.shipname}";
        currShip.isEmpty    = "${shipInfo.isemtiy}";
        currShip.lng        = ${shipInfo.deviceInfo.longitude};
        currShip.lat        = ${shipInfo.deviceInfo.latitude};
        currShip.moreDetail = "1"; //显示更多信息

        var mySquare = new SquareOverlay(currShip, 35);
        map.addOverlay(mySquare);
        map.centerAndZoom(point, 13);  // 编写自定义函数，创建标注
    }

    // 显示航行轨迹
    function onday(x){
        map.clearOverlays();
        var ships = [];
        var ships_base_info = [];
        ship_id = "";
        var shipid =  '${shipInfo.shipid}';
        var accuontUrl = 'drawTrackhis.do?shipid='+ shipid + '&day=' + x;
        var accuontData = '';
        var accuontJson = $.parseJSON(ajaxCustom(accuontUrl,accuontData, 'POST', false));


        for (accuont in accuontJson) {

            ships.push(
                    new BMap.Point(accuontJson[accuont]['longitude'], accuontJson[accuont]['latitude'])
            );
            ships_base_info.push({
                lat:accuontJson[accuont]['latitude'],
                lng:accuontJson[accuont]['longitude'],
                isEmpty:accuontJson[accuont]['isemtiy'],
                shipName:accuontJson[accuont]['shipname'],
                addDate:accuontJson[accuont]['adddate'],
                moreDetail:"0"
            });
        }
        //显示小船
        for (var i = 0; i < ships_base_info.length; i++) {
            var mySquare = new SquareOverlay(ships_base_info[i], 35);
            map.addOverlay(mySquare);
        }
        //创建折线
        var polyline = new BMap.Polyline(ships, {strokeColor:"red", strokeWeight:1, strokeOpacity:0.8});
        map.addOverlay(polyline);
    }

    $(function(){
        showShipLocation();
    })

</script>



</html>
