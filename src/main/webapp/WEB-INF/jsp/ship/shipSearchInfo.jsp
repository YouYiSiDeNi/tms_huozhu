<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/23
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>易航oTMS</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

    <!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
    <link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- layui -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">


</head>
<body>

<div class="wrapper">
    <%@include file="../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../common/top.jsp"%>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content">
                            <div class="site-demo-button" id="layerDemo" style="margin-bottom: 0;">
                                <input type="hidden" id="emtiy"  value="${emtiy}">
                                <input type="hidden" id="page"value="">
                                <input type="hidden" id="shipname" value="">
                                <button data-method="offset" data-type="auto" class="layui-btn layui-btn-normal" onclick="searchShips($('#page').val(),$('#shipname').val())">添加运力</button>
                                <button data-method="offset" data-type="auto" class="layui-btn layui-btn-normal" onclick="searchUserAndShips($('#page').val(),$('#shipname').val())">已申请运力</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <table class="layui-hide" id="shipinfo" lay-filter="demo"></table>
                            <script type="text/html" id="barDemo">
                                <%--<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">详情</a>--%>
                                <%--<a class="layui-btn layui-btn-xs" lay-event="detail">详情</a>--%>
                                <a class="layui-btn layui-btn-xs" lay-event="maps">航行轨迹</a>
                                <a class="layui-btn layui-btn-xs" lay-event="video">实时视频</a>
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--添加运力--%>
<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title">
                    船舶运力信息
                </h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" id="pages" value="">
                            <input type="text" id="shipnames" class="form-control" >
                        </div>
                        <div class="col-md-2 pull-left">
                            <button class="btn btn-info" type="button"  onclick="searchShips($('#pages').val(),$('#shipnames').val())">搜索船舶</button>
                        </div>
                    </div>

                </div>
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>船名</th>
                        <th>实时位置</th>
                        <th>船舶吨位</th>
                        <th>运输状态</th>
                        <th>航行状态(节)</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="tbody"></tbody>
                </table>

                <div class="row">
                    <div class="col-md-6">
                        <span id="summary"></span>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" id="pagenum"/>
                        <input type="hidden" id="pagesize"/>
                        <ul id="pagination" class="pagination">
                            <li id="01"><a href="#" class="btn btn-primary">首页</a></li>
                            <li id="02"><a href="#" class="btn btn-primary" style="margin-left: 5px">上一页</a></li>
                            <li id="03"><a href="#" class="btn btn-primary" style="margin-left: 5px">下一页</a></li>
                            <li id="04"><a href="#" class="btn btn-primary" style="margin-left: 5px">最后一页</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->


<%--已申请运力--%>
<div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title">
                    船舶运力申请信息
                </h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" id="page1" value="">
                            <input type="text" id="shipname1" class="form-control" >
                        </div>
                        <div class="col-md-2 pull-left">
                            <button class="btn btn-info" type="button"  onclick="searchUserAndShips($('#page1').val(),$('#shipname1').val())">搜索船舶</button>
                        </div>
                    </div>

                </div>
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>船名</th>
                        <th>实时位置</th>
                        <th>船舶吨位</th>
                        <th>运输状态</th>
                        <th>航行状态(节)</th>
                        <th>申请审核状态</th>
                    </tr>
                    </thead>
                    <tbody id="tbody1"></tbody>
                </table>

                <div class="row">
                    <div class="col-md-6">
                        <span id="summary1"></span>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" id="pagenum1"/>
                        <input type="hidden" id="pagesize1"/>
                        <ul id="pagination1" class="pagination">
                            <li id="page01"><a href="#" class="btn btn-primary">首页</a></li>
                            <li id="page02"><a href="#" class="btn btn-primary" style="margin-left: 5px">上一页</a></li>
                            <li id="page03"><a href="#" class="btn btn-primary" style="margin-left: 5px">下一页</a></li>
                            <li id="page04"><a href="#" class="btn btn-primary" style="margin-left: 5px">最后一页</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->



<%@include file="../common/modal.jsp"%>


</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>

<!-- layui -->

<script>
    document.getElementById("active2").className = "active";
    document.getElementById("crumbs").innerHTML="运力管理";

    function searchShips(page,shipname) {
        $.ajax({
            type : 'post',
            url : 'searchShipInfoNotByUserid.do?page='+page+'&shipname='+shipname,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                var array =data.shipInfo;
                var str ="";
                $.each(array,function (i) {
//                    缺少船舶在线的判断

                    if(array[i].isemtiy==0){
                        var emptystatu="空载";
                    }
                    else{
                        var emptystatu="满载";
                    }

                    if(array[i].deviceInfo.shipspeed==null){
                        var shipspeed="未知";
                    }
                    else{
                        var shipspeed=array[i].deviceInfo.shipspeed;
                    }
                    str+="<tr><td>"+ array[i].shipname+"</td><td>"+array[i].deviceInfo.cname+"</td><td>"+array[i].ton+"</td><td>"+emptystatu+"</td><td>"+shipspeed+"</td><td><a href='javascript:;' onclick='addShipinfo(" +array[i].shipid+")' class='btn btn-sm btn-info'>申请添加运力</a></td></tr>";

                })

                $("#tbody").html(str);

                displayFooter(data.pageInfo);

            }

        });
        $('#myModal7').modal('show');

    }
    function displayFooter(pageInfo) {
        <%--当前第 ${pageInfo.pageNum} 页.总共 ${pageInfo.pages} 页.一共 ${pageInfo.total} 条记录--%>
        var newText = '共' + pageInfo.total + '条记录，' + '当前第' + pageInfo.pageNum + '页，' + '总共' + pageInfo.pages + '页';
        $("#summary").text(newText);
        $("#pagenum").val(pageInfo.pageNum);
        $("#pagesize").val(pageInfo.pages);
    };

    $("#01").click(function(){
        var pagenum = 1;
        searchShips(pagenum,$('#shipnames').val());
    });

    $("#04").click(function(){
        var pagenum =$("#pagenum").val();
        var pagesize =$("#pagesize").val();
        pagenum = pagesize;
        searchShips(pagenum, $('#shipnames').val());
    });

    $("#02").click(function(){
        var pagenum =$("#pagenum").val();
        if(pagenum == 1){
            return false;
        } else{
            pagenum--;
            searchShips(pagenum, $('#shipnames').val());
        }
    });

    $("#03").click(function(){
        var pagenum =$("#pagenum").val();
        var pagesize =$("#pagesize").val();
        if(pagenum == pagesize){
            return false;
        } else{
            pagenum++;
            searchShips(pagenum, $('#shipnames').val());
        }
    });

</script>

<script>
    layui.use('table', function(){
        var table = layui.table;
        table.render({
            elem: '#shipinfo'
            ,url:'searchShipInfos.do?emtiy='+$("#emtiy").val()
            ,cellMinWidth: 80
            ,cols: [[
//                { field:'shipid',type:'checkbox'}
                {field:'shipname',  title: '船名'}
                ,{field:'cname',  title: '实时位置',templet:"#cname"}
                ,{field:'ton',  title: '船舶吨位', sort: true}
                ,{field:'isemtiy',  title: '运输状态',templet:"#isemtiy"}
                ,{field:'shipspeed',  title: '航行速度(节)',templet:"#shipspeed"}
                ,{field:'xiangqing',  title: '操作', toolbar: '#barDemo'}
            ]]
            ,skin: 'row' //表格风格
            ,even: true
            ,page: true //是否显示分页
            ,limits: [10,20,30,50]
            ,limit: 10 //每页默认显示的数量
        });
        table.on('tool(demo)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                ,layEvent = obj.event; //获得 lay-event 对应的值
            if(layEvent === 'detail'){
                layer.open({
                    type: 2,
                    title: '船舶信息',
                    area: ['560px', '400px'],
                    skin: 'layui-layer-rim', //加上边框
                    content: ["shiptextinfo.do?shipid="+data.shipid, 'no']
                });
            } else if(layEvent === 'maps'){
                window.open("shipvideo.do?shipid="+data.shipid+"#allmap");
//                layer.open({
//                    type: 2,
//                    title: '船舶地图',
//                    area: ['1200px', '600px'],
//                    skin: 'layui-layer-rim', //加上边框
//                    content: ["shipvideo.do?shipid="+data.shipid+"#guiji"]
//                });
            } else if(layEvent === 'video'){
                window.open("shipvideo.do?shipid="+data.shipid);
            }
        });

    });

    function searchUserAndShips(page,shipname){
        $.ajax({
            type : 'post',
            url : 'searchUserAndShipinfo.do?page='+page+'&shipname='+shipname,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if (data==false){
                    alert("暂无数据！");
                }else{
                    var array =data.shipInfo;
                    var str ="";
                    $.each(array,function (i) {
//                    缺少船舶在线的判断

                        if(array[i].isemtiy==0){
                            var emptystatu="空载";
                        }
                        else{
                            var emptystatu="满载";
                        }

                        if(array[i].deviceInfo.shipspeed==null){
                            var shipspeed="未知";
                        }
                        else{
                            var shipspeed=array[i].deviceInfo.shipspeed;
                        }
                        str+="<tr><td>"+ array[i].shipname+"</td><td>"+array[i].deviceInfo.cname+"</td><td>"+array[i].ton+"</td><td>"+emptystatu+"</td><td>"+shipspeed+"</td><td>申请审核中</td></tr>";

                    })
                    $("#tbody1").html(str);

                    displayFooter1(data.pageInfo);

                    $('#myModal8').modal('show');

                }
            }

        });
    }
    function displayFooter1(pageInfo) {
        <%--当前第 ${pageInfo.pageNum} 页.总共 ${pageInfo.pages} 页.一共 ${pageInfo.total} 条记录--%>
        var newText = '共' + pageInfo.total + '条记录，' + '当前第' + pageInfo.pageNum + '页，' + '总共' + pageInfo.pages + '页';
        $("#summary1").text(newText);
        $("#pagenum1").val(pageInfo.pageNum);
        $("#pagesize1").val(pageInfo.pages);
    };

    $("#page01").click(function(){
        var pagenum = 1;
        searchUserAndShips(pagenum,$('#shipname1').val());
    });

    $("#page04").click(function(){
        var pagenum =$("#pagenum1").val();
        var pagesize =$("#pagesize1").val();
        pagenum = pagesize;
        searchUserAndShips(pagenum, $('#shipname1').val());
    });

    $("#page02").click(function(){
        var pagenum =$("#pagenum1").val();
        if(pagenum == 1){
            return false;
        } else{
            pagenum--;
            searchUserAndShips(pagenum, $('#shipname1').val());
        }
    });

    $("#page03").click(function(){
        var pagenum =$("#pagenum1").val();
        var pagesize =$("#pagesize1").val();
        if(pagenum == pagesize){
            return false;
        } else{
            pagenum++;
            searchUserAndShips(pagenum, $('#shipname1').val());
        }
    });




    function addShipinfo(shipid) {
        var page= $('#pagenum').val();
        var shipname =$('#shipnames').val();
        $.ajax({
            type : 'post',
            url : 'addUserAndShip.do?shipid='+shipid,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if (data == true){
                    alert("申请成功！");
                    searchShips(page,shipname);
                }else{
                    alert("申请失败！");
                }
            }
        });
    }

</script>
<script type="text/html" id="isemtiy">
    {{# if(d.deviceInfo.isonline==0){ }}
    {{# if(d.isemtiy==0){ }}
    空载
    {{# }else{ }}
    满载
    {{# } }}
    {{# }else{ }}
    离线
    {{# } }}


</script>
<script type="text/html" id="cname">
    {{ d.deviceInfo.cname}}
</script>

<script type="text/html" id="shipspeed">
    {{# if(d.deviceInfo.shipspeed == null){ }}
    未知
    {{# }else{ }}
    {{d.deviceInfo.shipspeed}}
    {{# } }}
</script>
</html>
