<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/21
  Time: 13:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>易航oTMS</title>

    <style type="text/css">

         body, html {
            font-family: 'Microsoft YaHei', Arial, sans-serif;
        }
    </style>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>
    <%--<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>--%>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

    <!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
    <link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<sql:setDataSource  var="snapshot"  driver="com.mysql.jdbc.Driver"
                    url="jdbc:mysql://localhost:3306/tmsdb?useUnicode=true&characterEncoding=utf-8"
                    user="root"    password="ok"/>
<div class="wrapper">
    <%@include file="../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../common/top.jsp"%>

        <div class="content">
            <div class="container-fluid">

                <div class ="row">
                    <div class="col-md-4" onclick="toshipinfos(2)">
                        <div class="card cardchuan">
                            <div class="header">
                                <h4 class="title">船舶总数</h4>
                            </div>
                            <div class="content">
                                <i class="pe-7s-server" style="font-size:50px;color:#ff4444"></i>
                                <span class="alum-number" id="shipCount"  style="font-size:50px;color:#828282;line-height:1;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" onclick="toshipinfos(1)">
                        <div class="card  cardchuan">
                            <div class="header">
                                <h4 class="title">满载船舶</h4>
                            </div>
                            <div class="content">
                                <i class="pe-7s-note2" style="font-size:50px;color:#2089ff"></i>
                                <span class="alum-number" id="manZai"  style="font-size:50px;color:#828282;line-height:1;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 " onclick="toshipinfos(0)">
                        <div class="card cardchuan">
                            <div class="header">
                                <h4 class="title">空载船舶</h4>
                            </div>
                            <div class="content">
                                <i class="pe-7s-photo-gallery" style="font-size:50px;color:#ff7011"></i>
                                <span class="alum-number" id="kongZai" style="font-size:50px;color:#828282;line-height:1;"></span>
                            </div>
                        </div>
                    </div>

                    <%--<div class="col-md-3"  onclick="toshipinfos(3)">--%>
                        <%--<div class="card  cardchuan">--%>
                            <%--<div class="header">--%>
                                <%--<h4 class="title">离线船舶</h4>--%>
                            <%--</div>--%>
                            <%--<div class="content">--%>
                                <%--<i class="pe-7s-plug" style="font-size:50px;color:#399619"></i>--%>
                                <%--<span class="alum-number"  id="isonline" style="font-size:50px;color:#828282;line-height:1;"></span>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">货物类型分布</h4>
                                <span  style="float: right">
                                    <select id="goodsselect1" >
                                        <option value="2018"  selected = "selected">2018年</option>
                                        <option value="2019">2019年</option>
                                    </select>
                                    <select  id="goodsselect2">
                                        <option value="01">1月</option>
                                        <option value="02">2月</option>
                                        <option value="03">3月</option>
                                        <option value="04">4月</option>
                                        <option value="05">5月</option>
                                        <option value="06" >6月</option>
                                        <option value="07">7月</option>
                                        <option value="08"  selected = "selected">8月</option>
                                        <option value="09">9月</option>
                                        <option value="10">10月</option>
                                        <option value="11">11月</option>
                                        <option value="12">12月</option>
                                    </select>
                                    <button onclick="selectDate1()">查看</button>
                                </span>
                                <p class="category">运输计划中添加的货物种类</p>
                            </div>
                            <div class="content" id="shuaxin1">

                                <div id="jihuanone" style="display: none">本月暂无数据</div>
                                <div id="ship-chat" class="ct-chart ct-perfect-fourth" style="height: 300px"></div>


                                <div class="footer">

                                    <!--  <hr> -->
                                    <!--   <div class="stats">
                                          <i class="fa fa-clock-o"></i> 数据为最新数据汇总
                                      </div> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">常用船舶排行</h4>
                                <span  style="float: right">
                                    <select id="shipselect">
                                        <option value="全年">全年</option>
                                        <option value="半年" selected = "selected">半年</option>
                                        <option value="本月">本月</option>
                                    </select>
                                    <button onclick="selectDate2()">查看</button>
                                </span>
                                <p class="category">船舶运力排行</p>
                            </div>
                            <div class="content">
                                <span id="paihangnone" style="display:none">本月暂无数据</span>
                                <div id="shipsumtonlist" style="height: 360px"></div>
                                <div class="footer">
                                    <!-- <hr> -->
                                    <!--   <div class="stats">
                                          <i class="fa fa-clock-o"></i> 数据为最新数据汇总
                                      </div> -->
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12" style="position:absolute;top:740px;height: 200px;width: 97.5%">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">运力趋势</h4>
                                <span  style="float: right">
                                    <%--<button onclick="selectDate4()">前 7 天</button>--%>
                                    <%--<button onclick="selectDate5()">前 15 天</button>--%>
                                    <select name="datetime" id="tonSelect1">
                                        <option value="2018" selected = "selected">2018年</option>
                                        <option value="2019">2019年</option>
                                    </select>
                                    <select  id="tonSelect2">
                                        <option value="01">1月</option>
                                        <option value="02">2月</option>
                                        <option value="03">3月</option>
                                        <option value="04">4月</option>
                                        <option value="05">5月</option>
                                        <option value="06" >6月</option>
                                        <option value="07">7月</option>
                                        <option value="08" selected = "selected">8月</option>
                                        <option value="09">9月</option>
                                        <option value="10">10月</option>
                                        <option value="11">11月</option>
                                        <option value="12">12月</option>
                                    </select>
                                    <button onclick="selectDate3()">查看</button>
                                </span>
                                <p class="category">当前运力情况分布表（吨/日期）</p>
                            </div>
                            <div class="content">
                                <span id="qushinone" style="display:none">本月暂无数据</span>
                                <div id="ship-qushi" class="ct-chart ct-perfect-fourth"></div>
                                <div class="footer">

                                    <!--   <hr> -->
                                    <!-- <div class="stats">
                                        <i class="fa fa-clock-o"></i> 数据为最新数据汇总
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
    <%@include file="../common/modal.jsp"%>
</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>

<!--baidu echats-->
<script src="${pageContext.request.contextPath}/assets/js/echarts.min.js"></script>

<script>
    document.getElementById("active1").className = "active";
    document.getElementById("crumbs").innerHTML="概况";

    function selectDate1() {
        var select=$("#goodsselect1").val()+'-'+$("#goodsselect2").val();
        $.ajax({
            type : 'post',
            url : 'goodsType.do?selecttime='+select,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if(data==''){
                    alert("暂无数据！");
                    window.location.reload();
                }else{

                    var num=0;
                    for(var i=0;i<data.length;i++){
                        num+=data[i].value;
                    }
                    console.log(data);
                    $("#jihuanone").hide();
                    var myChart = echarts.init(document.getElementById('ship-chat'));
                    var option = {
                        title : {
                            text: '总吨位:'+num+'吨',
                            x: 'right',
                            y: 'bottom',
                            textStyle: {
                                color: '#333',
                                fontSize: 20,

                            },
                        },
                        tooltip : {
                            trigger: 'item',
                            formatter: "{a} <br/>{b} : {c} 吨({d}%)"
                        },

                        series : [
                            {
                                name: '货物类型分布',
                                type: 'pie',
                                radius : ['40%','70%'],
                                center: ['50%', '50%'],
                                data:data,
                                itemStyle: {
                                    emphasis: {
                                        shadowBlur: 10,
                                        shadowOffsetX: 0,
                                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                                    }
                                }
                            }
                        ]
                    };
                    myChart.setOption(option);
                }
            },
            error : function(){
                alert('提取数据失败');
            }


        })

    }

    function selectDate2() {
        var select=$("#shipselect").val();
        $.ajax({
            type : 'post',
            url : 'shipTon.do?select='+select,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if(data == ''){
                    alert("暂无数据！");
                    window.location.reload();
                }else{
                    $("#paihangnone").hide();
                    console.log(data)
                    var str='';
                    for (var i = 1; i < data.length + 1; i++) {
                        str=str+'<p><i class="main-right-save icon-trans-'+i+'" ></i>'+data[i-1].shipname+' <span style="float:right">'+data[i-1].sumton+'吨'+'</span></p>'

                    }
                    $('#shipsumtonlist').html(str);

                }
            },
            error : function(){
                alert('提取数据失败');
            }

        })
    }

    function selectDate3() {
        var select=$("#tonSelect1").val()+'-'+$("#tonSelect2").val();
        console.log(select);
        $.ajax({
            type : 'post',
            url : 'tonChange.do?tonselect='+select,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if(data == ''){
                    alert("暂无数据！");
                    window.location.reload();
                }else{
                    $("#qushinone").hide();
                    console.log(data);
                    var time=[];
                    var num=[];
                    for (var i = 0; i < data.length; i++) {
                        time.push(data[i].time);
                        num.push(data[i].sumton);
                    }

                    // 基于准备好的dom，初始化echarts实例
                    var myChart = echarts.init(document.getElementById('ship-qushi'));

                    // 指定图表的配置项和数据
                    var option = {
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: time
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: [{
                            data: num,
                            type: 'line',
                            areaStyle: {}
                        }]
                    };

                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);

                }
            },
            error : function(){
                alert('提取数据失败');
            }

        })
    }
    function selectDate4() {
        var select='七天'
        console.log(select);
        $.ajax({
            type : 'post',
            url : 'tonChange.do?tonselect='+select,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if(data == ''){
                    alert("暂无数据！");
                    window.location.reload();
                }else{
                    $("#qushinone").hide();
                    console.log(data);
                    var time=[];
                    var num=[];
                    for (var i = 0; i < data.length; i++) {
                        time.push(data[i].time);
                        num.push(data[i].sumton);
                    }

                    // 基于准备好的dom，初始化echarts实例
                    var myChart = echarts.init(document.getElementById('ship-qushi'));

                    // 指定图表的配置项和数据
                    var option = {
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: time
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: [{
                            data: num,
                            type: 'line',
                            areaStyle: {}
                        }]
                    };

                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);

                }
            },
            error : function(){
                alert('提取数据失败');
            }

        })
    }
    function selectDate5() {
        var select='十五天'
        console.log(select);
        $.ajax({
            type : 'post',
            url : 'tonChange.do?tonselect='+select,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if(data == ''){
                    alert("暂无数据！");
                    window.location.reload();
                }else{
                    $("#qushinone").hide();
                    console.log(data);
                    var time=[];
                    var num=[];
                    for (var i = 0; i < data.length; i++) {
                        time.push(data[i].time);
                        num.push(data[i].sumton);
                    }

                    // 基于准备好的dom，初始化echarts实例
                    var myChart = echarts.init(document.getElementById('ship-qushi'));

                    // 指定图表的配置项和数据
                    var option = {
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: time
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: [{
                            data: num,
                            type: 'line',
                            areaStyle: {}
                        }]
                    };

                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);

                }
            },
            error : function(){
                alert('提取数据失败');
            }

        })
    }


</script>

<script type="text/javascript">
    <%--
    更新说明：07.17
    此处把shipCount改为shipCount2，进行前端的数字相加，改回去只需把2去掉；
    屏蔽countShip的方法

    --%>
//    function countShip() {
//        var kong = $('#kongZai').html();
//        var man = $('#manZai').html();
//        var shipCount = Number(kong) + Number(man);
//        document.getElementById('shipCount2').innerHTML= shipCount;
//    }

    $(function () {
        $("#shipCount").load("selectShipInfoCount.do");
        $("#manZai").load("selectShipInfoByIsemityCount.do?isemtiy=1");
        $("#kongZai").load("selectShipInfoByIsemityCount.do?isemtiy=0");
        $("#isonline").load("selectShipInfoByIsonlneCount.do?isonline=1");
    })

    function toshipinfos(emtiy) {
        location.href="searchShip.do?emtiy="+emtiy;
    }

    function toshipinfosbyisonline(isonline) {
        location.href="searchShip.do?isonline="+isonline;
    }
</script>


<!--baidu ecahts 1 运力分布-->
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('ship-chat'));
    var select=$("#goodsselect1").val()+'-'+$("#goodsselect2").val();
    $.ajax({
        type : 'post',
        url : 'goodsType.do?selecttime='+select,
        dataType : 'json',
        contentType: "application/json; charset=utf-8",
        success : function(data) {
            if(data==''){
                $("#jihuanone").show();
            }else{
                $("#jihuanone").hide();
                console.log(data);
                var num=0;
                for(var i=0;i<data.length;i++){
                    num+=data[i].value;
                }
                var myChart = echarts.init(document.getElementById('ship-chat'));
                var option = {
                    title : {
                        text: '总吨位:'+num+'吨',
                        x: 'right',
                        y: 'bottom',
                        textStyle: {
                            color: '#333',
                            fontSize: 20,

                        },

                    },
                    tooltip : {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} 吨({d}%)"
                    },

                    series : [
                        {
                            name: '货物类型分布',
                            type: 'pie',
                            radius : ['40%','70%'],
                            center: ['50%', '50%'],
                            data:data,
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };
                myChart.setOption(option);
            }
        },
        error : function(){
            alert('提取数据失败');
        }


    })

</script>



<!--baidu ecahts 2 船舶运输排行-->
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var select=$("#shipselect").val();
    $.ajax({
        type : 'post',
        url : 'shipTon.do?select='+select,
        dataType : 'json',
        contentType: "application/json; charset=utf-8",
        success : function(data) {
            if(data == ''){
                $("#paihangnone").show();
            }else{
                $("#paihangnone").hide();
                console.log(data)
                var str='';
                for (var i = 1; i < data.length + 1; i++) {
                    str=str+'<p><i class="main-right-save icon-trans-'+i+'" ></i>'+data[i-1].shipname+' <span style="float:right">'+data[i-1].sumton+'吨'+'</span></p>'

                }
                $('#shipsumtonlist').html(str);

            }
        },
        error : function(){
            alert('提取数据失败');
        }

    })
</script>


<!--baidu ecahts 3 运力趋势-->
<script type="text/javascript">
    var select=$("#tonSelect1").val()+'-'+$("#tonSelect2").val();
    console.log(select);
    $.ajax({
        type : 'post',
        url : 'tonChange.do?tonselect='+select,
        dataType : 'json',
        contentType: "application/json; charset=utf-8",
        success : function(data) {
            if(data == ''){
                $("#qushinone").show();
            }else{
                $("#qushinone").hide();
                console.log(data);
                var time=[];
                var num=[];
                for (var i = 0; i < data.length; i++) {
                    time.push(data[i].time);
                    num.push(data[i].sumton);
                }

                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('ship-qushi'));

                // 指定图表的配置项和数据
                var option = {
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: time
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        data: num,
                        type: 'line',
                        areaStyle: {}
                    }]
                };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);

            }
        },
        error : function(){
            alert('提取数据失败');
        }

    })
</script>

</html>
