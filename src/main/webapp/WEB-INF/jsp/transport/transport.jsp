<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/23
  Time: 9:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>易航oTMS</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

    <!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
    <link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- layui -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">

    <style type="text/css">
        body, html,#allmap {width: 100%;height:80%;overflow: hidden;margin-top: 0 !important;font-family:"微软雅黑";font-size:12px;}
        input[type="date"]::-webkit-calendar-picker-indicator {
            color: rgba(0, 0, 0, 0);
            opacity: 1;
            display: block;
            background: url(../assets/img/datetime.png) no-repeat;
            width: 20px;
            height: 20px;
            border-width: thin;
            cursor: pointer;
        }
    </style>

    <style type="text/css">
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }
        input[type="number"]{
            -moz-appearance: textfield;
        }
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=WnAUIrAmQK8oUf0a5tgKxqRfpAA59iCc"></script>


</head>
<body onload="autoload(${pageInfo.total})">

<div class="wrapper">

    <%@include file="../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../common/top.jsp"%>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-info btn-fill pull-left" onclick="addTransport()">新增计划</button>
                        <span class="pull-right" style="margin-top: 25px"><a href="torecycleplan.do"><i class="pe-7s-trash" style="color:dodgerblue;font-size: 14px;font-weight: 600"></i>回收站 </a> </span>
                    </div>
                </div>

                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12">
                        <div class="card" >

                            <div class="row" style="margin:0 10 0;color:white">.
                                <div class="row" style="margin:0 10 0;">
                                    <form action="searchTransport.do" method="post">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>计划编号</label>
                                                <input type="text" class="form-control" id="ordernumber"  name="ordernumber" placeholder="请输入计划编号" value="<%=request.getParameter("ordernumber")==null?"":request.getParameter("ordernumber")%>">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>货主名称</label>
                                                <input type="text" class="form-control" id="goodsownername" name="goodsownername" placeholder="请输入货主名称" value="<%=request.getParameter("goodsownername")==null?"":request.getParameter("goodsownername")%>">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>创建日期</label>
                                                <%--<input type="text" class="layui-input" id="selectStratTime" name="unloadtime" value="" placeholder="请选择货物卸空时间">--%>
                                                <input type="date" onkeypress="javascript:return false" class="form-control" id="start" name="start"  placeholder="请选择计划开始时间" value="<%=request.getParameter("start")==null?"":request.getParameter("start")%>">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label style="color:white;">.</label>
                                                ——<input type="date" onkeypress="javascript:return false" class="form-control" id="end" name="end" placeholder="请选择卸货日期" value="<%=request.getParameter("end")==null?"":request.getParameter("end")%>">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>筛选</label>
                                                <button type="submit" class=" form-control btn btn-info btn-fill pull-left" onclick="shaixuanOutpu(${pageInfo.total})" value="筛选">筛选</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top: 5px">
                                            <label>批量导出</label>
                                            <button type="button" class="form-control btn btn-danger btn-fill pull-left" id="execlTimeName" onclick="exportexcel()" >批量导出</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-group" id="accordion">
                                <c:forEach items="${list}" var="plan">
                                    <div class="panel panel-default  in" style="margin-top: 20px;margin-left: 5px;margin-right: 5px; box-shadow: 0px 4px 12px #d6d6d6;">
                                        <div class="panel-heading"  onclick="searchShipsAndTran(${plan.transportid})">
                                            <div class="row" style="font-weight:bold;font-size: 10px">
                                                <div class="col-md-3">计划编号：${plan.ordernumber}</div>
                                                <div class="col-md-3">货主名称:${plan.goodsInfo.goodsownername}</div>
                                                <div class="col-md-2 ">货主手机：${plan.goodsInfo.goodsphone}</div>
                                                <div class="col-md-2">计划开始时间:<fmt:formatDate pattern="yyyy-MM-dd" value="${plan.startdate}" /></div>
                                                <div class="col-md-2">计划完成时间:<fmt:formatDate pattern="yyyy-MM-dd" value="${plan.enddate}" /></div>
                                            </div>
                                            <h1 class="page-header" style="padding-bottom:10px;margin:10 0 10"></h1>
                                            <div class="row">
                                                <div class="col-md-3">航程：${plan.originating}-${plan.destination}</div>
                                                <div class="col-md-3">货物类型：${plan.goodsname}</div>
                                                <div class="col-md-2">总吨位：${plan.totaltonnage}吨</div>
                                                <c:set var="nowDate" value="<%=System.currentTimeMillis()%>"></c:set>
                                                <c:choose>
                                                    <c:when test="${nowDate-plan.enddate.getTime() > 0}">
                                                        <div class="col-md-2 pull-right"><span style="background-color:#acacac;color: white;border-radius: 3px;font-size: 12px">&nbsp;&nbsp;已完成&nbsp;&nbsp;</span></div>
                                                    </c:when>
                                                    <c:when test="${nowDate-plan.startdate.getTime() > 0}">
                                                        <div class="col-md-2 pull-right"><span style="background-color: green;color: white;border-radius: 3px;font-size: 12px">&nbsp;&nbsp;运输中&nbsp;&nbsp;</span></div>
                                                    </c:when>
                                                    <c:when test="${nowDate-plan.startdate.getTime() < 0}">
                                                        <div class="col-md-2 pull-right"><span style="background-color: #ff7300;color: white;border-radius: 3px;font-size: 12px">&nbsp;&nbsp;待装船&nbsp;&nbsp;</span></div>
                                                    </c:when>
                                                </c:choose>
                                                <div class="col-md-2"></div>
                                            </div>
                                            <h1 class="page-header" style="padding-bottom:10px;margin:10 0 10"></h1>
                                            <div class="row" style="margin-top: 5px;">
                                                <div class="col-md-3 pull-left">系统流水号：${plan.tanordernumber}</div>
                                                <div class="col-md-3">创建时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${plan.creationdate}" /></div>
                                                <div class="col-md-2">创建人：<span id="${plan.transportid}">${plan.scheduler}</span></div>
                                                    <%--<c:choose>--%>
                                                    <%--<c:when test="${plan.stats ==0}">--%>
                                                <div class="col-md-2"></div>
                                                <div class="col-md-1"><input type="button" class="btn btn-primary btn-sm btn-xs pull-right" id="editPlan${plan.ordernumber}" onclick="tansprotEdit(${plan.transportid})" value="编辑计划"></div>
                                                <div class="col-md-1"><input type="button" class="btn btn-primary btn-sm btn-xs pull-right" id="overPlan${plan.ordernumber}" onclick="transportOver(${plan.transportid})" value="中止计划"></div>
                                                    <%--</c:when>--%>
                                                    <%--<c:when test="${plan.stats ==1}">--%>
                                                    <%--<div class="col-md-1"><input type="button" class="btn btn-primary btn-sm btn-xs pull-right" id="editPlan${plan.ordernumber}" disabled="disabled" value="不可编辑"></div>--%>
                                                    <%--<div class="col-md-1"><input type="button" class="btn btn-primary btn-sm btn-xs pull-right" id="overPlan${plan.ordernumber}" disabled="disabled" value="计划已锁定"></div>--%>
                                                    <%--</c:when>--%>
                                                    <%--</c:choose>--%>
                                                    <%--<div class="col-md-1"><button class="btn btn-primary btn-sm btn-xs pull-right" onclick="transportMap(${plan.transportid})">实时位置</button></div>--%>

                                            </div>

                                            <div class="row" style="margin-top: 5px;">
                                                <div class="col-md-3 pull-left"></div>
                                                <div class="col-md-3"></div>
                                                <div class="col-md-2"></div>
                                                    <%--<c:choose>--%>
                                                    <%--<c:when test="${plan.stats ==0}">--%>
                                                <div class="col-md-2"></div>
                                                <div class="col-md-1"><button class="btn btn-primary btn-sm btn-xs pull-right" onclick="transportMap(${plan.transportid})">船队位置</button></div>
                                                <div class="col-md-1"><a class="btn  btn-danger btn-sm btn-xs pull-right" data-toggle="collapse" data-parent="#accordion" id="${plan.transportid}cdxq" href="#collapseOne${plan.transportid}">船队详情</a></div>
                                                    <%--</c:when>--%>
                                                    <%--<c:when test="${plan.stats ==1}">--%>
                                                    <%--<div class="col-md-1"><input type="button" class="btn btn-primary btn-sm btn-xs pull-right" id="editPlan${plan.ordernumber}" disabled="disabled" value="不可编辑"></div>--%>
                                                    <%--<div class="col-md-1"><input type="button" class="btn btn-primary btn-sm btn-xs pull-right" id="overPlan${plan.ordernumber}" disabled="disabled" value="计划已锁定"></div>--%>
                                                    <%--</c:when>--%>
                                                    <%--</c:choose>--%>

                                            </div>

                                        </div>
                                        <div id="collapseOne${plan.transportid}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <table class="table" id="shipinfo">
                                                    <thead>
                                                    <tr>
                                                        <th>船名</th>
                                                        <th>联系电话</th>
                                                        <th>受载/计划吨位</th>
                                                        <th>装货完成时间</th>
                                                        <th>卸货完成时间</th>
                                                        <th>实收吨位</th>
                                                        <th>实时位置</th>
                                                        <th>船舶状态</th>
                                                        <th>航速(节)</th>
                                                        <th>操作</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbody${plan.transportid}"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="card" style="height: 100px;text-align: center;"id="noneData" style="display:none;">
                            <div class="row" ><h3 style="margin-top: 40px"><i class="pe-7s-info" style="color: #01AAED"></i> 暂无数据！</h3></div>
                        </div>

                    </div>
                    <div class="row">
                        <!--文字信息-->


                        <!--点击分页-->
                        <div class="col-md-6">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">

                                    <li><a href="javascript:void(0);" onclick="chickPage(1)">首页</a></li>

                                    <!--上一页-->
                                    <li>
                                        <c:if test="${pageInfo.hasPreviousPage}">
                                            <a href="javascript:void(0);" onclick="chickPage(${pageInfo.pageNum-1})" aria-label="Previous">
                                                <span aria-hidden="true">«</span>
                                            </a>
                                        </c:if>
                                    </li>

                                    <!--循环遍历连续显示的页面，若是当前页就高亮显示，并且没有链接-->
                                    <c:forEach items="${pageInfo.navigatepageNums}" var="page_num">
                                        <c:if test="${page_num == pageInfo.pageNum}">
                                            <li class="active"><a href="#">${page_num}</a></li>
                                        </c:if>
                                        <c:if test="${page_num != pageInfo.pageNum}">
                                            <li><a href="javascript:void(0);" onclick="chickPage(${page_num})">${page_num}</a></li>
                                        </c:if>
                                    </c:forEach>

                                    <!--下一页-->
                                    <li>
                                        <c:if test="${pageInfo.hasNextPage}">
                                            <a href="javascript:void(0);" onclick="chickPage(${pageInfo.pageNum+1})"
                                               aria-label="Next">
                                                <span aria-hidden="true">»</span>
                                            </a>
                                        </c:if>
                                    </li>

                                    <li> <a href="javascript:void(0);" onclick="chickPage(${pageInfo.pages})">尾页</a></li>
                                </ul>
                            </nav>
                        </div>

                        <div class="col-md-3 pull-right" style="margin-top:30px;">
                            当前第 ${pageInfo.pageNum} 页.总共 ${pageInfo.pages} 页.一共 ${pageInfo.total} 条记录
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<!-- 模态框2查看（Modal） -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:50%">
        <div class="modal-content">
            <form action="" id="addForm" method="post" class="form-inline" target="formSubmit">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h3 class="modal-title">
                        <span id="shipnamex"></span>
                    </h3>
                    <h4 class="modal-title">
                        电话：<span id="shipphone"></span>
                    </h4>

                </div>
                <div class="modal-body">
                    <input type="hidden" id="shipid" name="shipid" >
                    <input type="hidden" id="tid" name="tid" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>计划吨位(单位:吨)</label>
                                <input type="text" class="form-control" id="planton" name="planton"  placeholder="请填写计划吨位" >
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>抵达发货地日期</label>
                                <input type="text" class="form-control" id="origporttime" name="origporttime"  placeholder="点击选择日期" oninputthithis="alert(this.value)" >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>发货地/装货港口</label>
                                <input type="text" class="form-control" id="originating1" placeholder="请填写发货地" disabled="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>开始装货时间</label>
                                <input type="text" class="form-control" id="startloadingtime" name="startloadingtime" placeholder="点击选择日期">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>装货完成时间</label>
                                <input type="text" class="form-control" id="fulltime" name="fulltime" placeholder="点击选择日期">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >受载吨位(单位:吨)</label>
                                <input type="text" class="form-control" id="loadton" name="loadton" placeholder="请填写受载吨位">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>抵达目的地日期</label>
                                <input type="text" class="form-control" id="arriveporttime" name="arriveporttime" placeholder="点击选择日期">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>目的地/装货港口</label>
                                <input type="text" class="form-control" id="destination1" placeholder="twitterhandle" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-4">
                            <div class=form-group">
                                <label>开始卸货时间</label>
                                <input type="text" class="form-control" id="startunloadingtime" name="startunloadingtime"  placeholder="点击选择日期">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>卸货完成时间</label>
                                <input type="text" class="form-control" id="unloadtime" name="unloadtime" placeholder="点击选择日期">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>实收吨位(单位:吨)</label>
                                <input type="text" class="form-control" id="realton" name="realton" placeholder="请填写实收吨位">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" onclick="hangqibianji($('#tid').val())">保存</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </form>
            <iframe name="formSubmit" style="display:none;"></iframe>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!-- 模态框3地图 -->
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: auto" data-backdrop="static">
    <div class="modal-dialog"  style="width: 90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="closeModal4()"aria-hidden="true">
                    &times;
                </button>
                <h5 class="modal-title">
                    船舶位置
                </h5>
            </div>
            <div class="modal-body">
                <div id="allmap" style="margin-top:50px"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>


<!-- 模态框4 编辑运输计划 -->
<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: auto" data-backdrop="static">
    <div class="modal-dialog"  style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title">
                    编辑计划
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>计划编号</label>
                            <input type="text" class="form-control" id="ordernumberEdit" disabled="disabled">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>计划创建人</label>
                            <input type="text" class="form-control" id="schedulerEdit" required>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <inupt type ="hidden" id="tidEdit" value=""/>
                    <input type="hidden" id="pageEdit"value=""/>
                    <input type="hidden" id="shipnameEdit" value=""/>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>货主名称</label>
                            <input type="text" class="form-control" id="goodsownernameEdit" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>货主电话</label>
                            <input type="number" class="form-control" id="goodsphoneEdit"  required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>货物类型</label>
                            <input type="text" class="form-control" id="goodsnameEdit" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>总吨位(吨)</label>
                            <input type="number"  class="form-control" id="totaltonnageEdit" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>发货地/发货港口</label>
                            <input type="text" class="form-control" id="originatingEdit" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>计划开始时间</label>
                            <%--<input type="date" onkeypress="javascript:return false" class="form-control" id="startdateEdit" required>--%>
                            <input type="text" class="form-control" id="startdateEdit" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>目的地/卸货港口</label>
                            <input type="text" class="form-control" id="destinationEdit" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>计划完成时间</label>
                            <%--<input type="date" onkeypress="javascript:return false" class="form-control" id="enddateEdit" required>--%>
                            <input type="text" class="form-control" id="enddateEdit" required>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>描述</label>
                            <textarea maxlength="300" rows="2" class="form-control" id="remarkesEdit"></textarea>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div>
                        <%--<button data-method="offset" data-type="auto" class="layui-btn layui-btn-normal" onclick="searchShips()">添加承运船舶</button>--%>
                        <button class="btn btn-primary btn-sm" onclick="searchShipsEdit($('#pageEdit').val(),$('#shipnameEdit').val())">修改承运船舶</button>
                    </div>
                </div>
                <table class="table table-bordered table-striped table table-hover" id="test_tableEdit" style="margin-top: 10px">
                    <thead id="tableHeadEdit">
                    <th>序号</th>
                    <th>船名</th>
                    <th>承运吨位（吨）</th>
                    <th class="hidden-xs">船主姓名</th>
                    <th class="hidden-xs">联系电话</th>
                    <th class="hidden-xs">实时位置</th>
                    <th class="hidden-xs">船舶状态</th>
                    <th class="hidden-xs">操作</th>
                    <th>添加调度员</th>
                    </thead>
                    <tbody id="thisBodyEdit"></tbody>
                </table>

                <button type="button" class="btn btn-info btn-fill pull-right" onclick="submitTransport($('#tidEdit').val())" >保存计划</button>
                <div class="clearfix"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>


<!-- 模态框4 新增运输计划 -->
<div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: auto" data-backdrop="static">
    <div class="modal-dialog"  style="width:80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title">
                    新增计划
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>计划编号</label>
                            <input type="text" class="form-control" id="ordernumberAdd" placeholder="请输入计划编号" value="" onblur="selectOredernumber()"><span id="msg" style="color: red;"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>计划创建人</label>
                            <input type="text" class="form-control" id="schedulerAdd" placeholder="请输入计划创建人" value="">
                        </div>
                    </div>

                </div>
                <div class="row">
                    <input type="hidden" id="tid2"value="">
                    <input type="hidden" id="page"value="">
                    <input type="hidden" id="shipname" value="">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>货主名称</label>
                            <input type="text" class="form-control" id="goodsownernameAdd" placeholder="请输入货主名称" value="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>货主电话</label>
                            <input type="number"  class="form-control" id="goodsphoneAdd" placeholder="请输入货主电话" value="">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>货物类型</label>
                            <input type="text" class="form-control" id="goodsnameAdd" placeholder="货物类型" value="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>总吨位(吨)</label>
                            <input type="number"   class="form-control" id="totaltonnageAdd" placeholder="总吨位" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>发货地/装货港口</label>
                            <input type="text" class="form-control" id="originatingAdd" placeholder="请输入发货地" value="">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>计划开始时间</label>
                            <%--<input type="date" onkeypress="javascript:return false" class="form-control" id="startdateAdd" placeholder="请选择计划开始时间">--%>
                            <input type="text" class="form-control" id="startdateAdd"  placeholder="请选择计划开始时间">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>目的地/卸货港口</label>
                            <input type="text" class="form-control" id="destinationAdd" placeholder="请输入目的地" value="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color:red">*</span>计划完成时间</label>
                            <%--<input type="date" onkeypress="javascript:return false" class="form-control" id="enddateAdd" placeholder="请选择达日期">--%>
                            <input type="text" class="form-control" id="enddateAdd"  placeholder="请选择计划开始时间">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>描述</label>
                            <textarea maxlength="300" rows="2" class="form-control" id="remarkesAdd" placeholder="在这里输入描述" value="Mike"></textarea>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div>
                        <%--<button data-method="offset" data-type="auto" class="layui-btn layui-btn-normal" onclick="searchShips()">添加承运船舶</button>--%>
                        <button class="btn btn-primary btn-sm" onclick="searchShips($('#page').val(),$('#shipname').val())">添加承运船舶</button>
                    </div>
                </div>
                <table class="table table-bordered table-striped table table-hover" id="test_table" style="margin-top: 10px">
                    <thead id="tableHead">
                    <th>序号</th>
                    <th>船名</th>
                    <th>承运吨位（吨）</th>
                    <th class="hidden-xs">船主姓名</th>
                    <th class="hidden-xs">联系电话</th>
                    <th class="hidden-xs">实时位置</th>
                    <th class="hidden-xs">船舶状态</th>
                    <th class="hidden-xs">操作</th>
                    <th>添加调度员</th>
                    </thead>
                    <tbody id="thisBody"></tbody>
                </table>

                <button type="button" class="btn btn-info btn-fill pull-right" onclick="submitTransport($('#tid2').val())" >提交计划</button>
                <div class="clearfix"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>



<%--添加承运船舶--%>
<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title">
                    船舶运力信息
                </h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" id="pages" value="">
                            <input type="text" id="shipnames" class="form-control" >
                        </div>
                        <div class="col-md-2 pull-left">
                            <button class="btn btn-info" type="button"  onclick="searchShips($('#pages').val(),$('#shipnames').val())">搜索船舶</button>
                        </div>
                        <div class="col-md-offset-4 col-md-2">
                            <button class="btn btn-danger" type="button" onclick="addShip()">添加为承运船舶</button>
                        </div>

                    </div>

                </div>
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>选择</th>
                        <th>船名</th>
                        <th>实时位置</th>
                        <th>实时吨位</th>
                        <th>运输状态</th>
                        <th>航行状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="tbody"></tbody>
                </table>

                <div class="row">
                    <div class="col-md-6">
                        <span id="summary"></span>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" id="pagenum"/>
                        <input type="hidden" id="pagesize"/>
                        <ul id="pagination" class="pagination">
                            <li id="01"><a href="javascript:;" class="btn btn-primary">首页</a></li>
                            <li id="02"><a href="javascript:;" class="btn btn-primary" style="margin-left: 5px">上一页</a></li>
                            <li id="03"><a href="javascript:;" class="btn btn-primary" style="margin-left: 5px">下一页</a></li>
                            <li id="04"><a href="javascript:;" class="btn btn-primary" style="margin-left: 5px">最后一页</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->



<%--修改添加承运船舶--%>
<div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title">
                    船舶运力信息
                </h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" id="pagesEdit" value="">
                            <input type="text" id="shipnamesEdit" class="form-control" >
                        </div>
                        <div class="col-md-2 pull-left">
                            <button class="btn btn-info" type="button"  onclick="searchShipsEdit($('#pagesEdit').val(),$('#shipnamesEdit').val())">搜索船舶</button>
                        </div>
                        <div class="col-md-offset-4 col-md-2">
                            <button class="btn btn-danger" type="button" onclick="addShipEdit()">添加为承运船舶</button>
                        </div>

                    </div>

                </div>
                <table class="layui-table" id="table_test">
                    <thead>
                    <tr>
                        <th>选择</th>
                        <th>船名</th>
                        <th>实时位置</th>
                        <th>实时吨位</th>
                        <th>运输状态</th>
                        <th>航行状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="tbodyEdit"></tbody>
                </table>
                <%--<table class="layui-table">--%>
                    <%--<tr>--%>
                        <%--<td><input type="checkbox" name="ck" value="1">选择</td>--%>
                        <%--<td><input type="checkbox" name="ck" value="2">船名</td>--%>
                        <%--<td><input type="checkbox" name="ck" value="3">实时位置</td>--%>
                        <%--<td><input type="checkbox" name="ck" value="4">实时吨位</td>--%>
                        <%--<td><input type="checkbox" name="ck" value="5">运输状态</td>--%>
                        <%--<td><input type="checkbox" name="ck" value="6">航行状态</td>--%>
                        <%--<td><input type="checkbox" name="ck" value="7">操作</td>--%>
                    <%--</tr>--%>

                <%--</table>--%>

                <div class="row">
                    <div class="col-md-6">
                        <span id="summary1"></span>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" id="pagenumEdit"/>
                        <input type="hidden" id="pagesizeEdit"/>
                        <ul id="pagination1" class="pagination">
                            <li id="edit01"><a href="javascript:;" class="btn btn-primary">首页</a></li>
                            <li id="edit02"><a href="javascript:;" class="btn btn-primary" style="margin-left: 5px">上一页</a></li>
                            <li id="edit03"><a href="javascript:;" class="btn btn-primary" style="margin-left: 5px">下一页</a></li>
                            <li id="edit04"><a href="javascript:;" class="btn btn-primary" style="margin-left: 5px">最后一页</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->





<%@include file="../common/modal.jsp"%>

</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>

<%--<!--  Checkbox, Radio & Switch Plugins -->--%>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>

<script type="text/javascript">

    document.getElementById("active3").className = "active";
    document.getElementById("crumbs").innerHTML="运输管理";

    function had(shipid) {

        var openUrl = "shipmap.do?shipid="+shipid;//弹出窗口的url
        var iWidth=1200; //弹出窗口的宽度;
        var iHeight=600; //弹出窗口的高度;
        var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
        var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;
        window.open(openUrl,"","height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft );

    }

    function addTransport() {
        $.get("addTransport.do");
        $('#myModal6').modal('show');
    }
    //    function tijiaogengaiclear() {
    //        window.location.reload();
    //    }
    function formatdata(a){
        var res=a.format("yyyy-MM-dd hh:mm:ss");
        return res;
    }

    function searchShipsAndTran(tid){
        $.ajax({
            type : 'post',
            url : 'selectShipinfosByTid.do?tid='+tid,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                var array = data.transport.shipInfoList;
                var str ="";

                $.each(array,function (i) {
                    if(array[i].isemtiy==0){
                        var emptystatus="空载";
                    }
                    else{
                        var emptystatus="满载";
                    }

                    if(array[i].shipAndTransport.fulltime==null){
                        var fulltime="未填写";
                    }
                    else{
                        var fulltime=formatDate2(array[i].shipAndTransport.fulltime);
                    }

                    if(array[i].shipAndTransport.unloadtime==null){
                        var emptytime="未填写";
                    }
                    else{
                        var emptytime=formatDate2(array[i].shipAndTransport.unloadtime);
                    }

                    if(array[i].deviceInfo.shipspeed==null){
                        var shipspeed="未知";
                    }
                    else{
                        var shipspeed=array[i].deviceInfo.shipspeed;
                    }


                    if(array[i].shipAndTransport.planton == 0) {
                        var orderEmploy = array[i].ton;
                    }
                    else {
                        var orderEmploy = array[i].shipAndTransport.planton;
                    }

                    str+="<tr style='font-size: 12px'><td>"+ array[i].shipname+"</td><td>"
                            +array[i].shipownerphone+"</td><td>"
                            +array[i].shipAndTransport.loadton+"/"+orderEmploy +"</td><td>"
                            +fulltime+"</td><td>"
                            +emptytime+"</td><td>"
                            +array[i].shipAndTransport.realton+"</td><td style='cursor:pointer;color: #00AAEE' onclick='openShipDetailmap(" +array[i].shipid+")'>"
                            +"<i class='pe-7s-map-marker' style='color: dodgerblue;'></i>"
                            +array[i].deviceInfo.cname+"</td><td>"
                            +emptystatus+"</td><td>"
                            +shipspeed+"</td><td>"
                            +"<a href='javascript:;' class='layui-btn layui-btn-xs' style='font-size: 12px' onclick='tranInfo("+array[i].shipid+","+tid+")'>航期管理</a>"
                            +"<a href='javascript:;' class='layui-btn layui-btn-primary layui-btn-xs' style='font-size: 12px' onclick='openShipDetail(" +array[i].shipid+")'  target='_blank'>监控视频</a></td></tr>";
                })
                $("#tbody"+tid).html(str);
            }

        })

    }

    function hangqibianji(tid) {
        var t1 = document.getElementById("startloadingtime").value;
        var t2 = document.getElementById("fulltime").value;
        var t3 = document.getElementById("startunloadingtime").value;
        var t4 = document.getElementById("unloadtime").value;
        var d1 = t1.replace(/[&\|\\\:^ \-]/g,"");
        var d2 = t2.replace(/[&\|\\\:^ \-]/g,"");
        var d3 = t3.replace(/[&\|\\\:^ \-]/g,"");
        var d4 = t4.replace(/[&\|\\\:^ \-]/g,"");

        if (d1 && d2) {
            if (d1>d2 || d1==d2){
                alert("开始装货时间需早于装货完成时间！");
                return false;

            }
        }

        if (d3 && d4) {
            if (d3>=d4 || d3==d4){
                alert("开始卸货时间需早于卸货完成时间！");
                return false;
            }
        }

        document.getElementById("addForm").action = "updateShipAndTransport.do";
        document.getElementById("addForm").submit();
        $("#"+tid+"cdxq").trigger("click");
        setTimeout(function(){$("#"+tid+"cdxq").trigger("click")},500);
        $('#myModal2').modal('hide');

    }

    function  chickPage(pn) {
        location.href='searchTransport.do?start='+$("#start").val() + '&end='+ $("#end").val() + '&ordernumber='+ $("#ordernumber").val()+'&pn='+pn;
    }

    //显示详情
    function tranInfo(shipid,tid) {
        $.ajax({
            type : 'post',
            url : 'selectShipAndTransport.do?tid='+tid+'&shipid='+shipid,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : showQuery1
        })
        document.getElementById('tid').value =tid;
        document.getElementById('shipid').value=shipid;

    }

    function showQuery1(data) {
        $("#shipnamex").text(data.shipInfo.shipname);
        $("#shipphone").text(data.shipInfo.shipownerphone);
        $("#originating1").val(data.transportPlan.originating);
        $("#destination1").val(data.transportPlan.destination);
        if(data.origporttime == null){
            $("#origporttime").val("");
        }else{
            $("#origporttime").val(formatDate1(data.origporttime));
        }
        if(data.arriveporttime == null){
            $("#arriveporttime").val("");
        }else{
            $("#arriveporttime").val(formatDate1(data.arriveporttime));
        }
        if(data.loadton == null){
            $("#loadton").val("未填写");
        }else{
            $("#loadton").val(data.loadton);
        }
        if(data.realton == null){
            $("#realton").val("未填写");
        }else{
            $("#realton").val(data.realton);
        }
        if(data.fulltime == null){
            $("#fulltime").val("");
        }else{
            $("#fulltime").val(formatDate1(data.fulltime));
        }
        if(data.unloadtime == null){
            $("#unloadtime").val("");
        }else{
            $("#unloadtime").val(formatDate1(data.unloadtime));
        }
        if(data.planton == 0){
            $("#planton").val(data.shipInfo.ton);
        }else{
            $("#planton").val(data.planton);
        }
        if(data.startunloadingtime == null){
            $("#startunloadingtime").val("");
        }else{
            $("#startunloadingtime").val(formatDate1(data.startunloadingtime));
        }
        if (data.startloadingtime == null) {
            $("#startloadingtime").val("");
        } else {
            $("#startloadingtime").val(formatDate1(data.startloadingtime));
        }
        $('#myModal2').modal('show');
    }

    function transportMap(tid) {
        $('#myModal4').modal('show');
        setTimeout(function(){
            shipMap(tid);
        },280);
    }

    layui.use('laydate',function () {
        var laydate = layui.laydate;
        laydate.render({
            elem: '#origporttime'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#arriveporttime'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#fulltime'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#startloadingtime'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#startunloadingtime'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#unloadtime'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#startdateEdit'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#startdateAdd'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#enddateEdit'
            ,type: 'datetime'
        });
        laydate.render({
            elem: '#enddateAdd'
            ,type: 'datetime'
        });

        laydate.render({
            elem: '#selectStratTime'
            ,range: true
        });
    })


    function openShipDetail(shipid) {
        window.open("shipvideo.do?shipid="+shipid);
    }
    function openShipDetailmap(shipid) {
        var openUrl = "shipmap.do?shipid="+shipid;//弹出窗口的url
        var iWidth=1200; //弹出窗口的宽度;
        var iHeight=600; //弹出窗口的高度;
        var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
        var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;
        window.open(openUrl,"","height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft );
    }

    function formatDate(date){
        date = new Date(date);
        var y=date.getFullYear();
        var m=date.getMonth()+1;
        var d=date.getDate();
        var h=date.getHours();
        var m1=date.getMinutes();
        var s=date.getSeconds();
        m = m<10?("0"+m):m;
        d = d<10?("0"+d):d;
//        return y+"-"+m+"-"+d+" "+h+":"+m1+":"+s;
        return y+"-"+m+"-"+d;
    }


    function formatDate1(date){
        date = new Date(date);
        var y=date.getFullYear();
        var m=date.getMonth()+1;
        var d=date.getDate();
        var h=date.getHours();
        var m1=date.getMinutes();
        var s=date.getSeconds();
        m = m<10?("0"+m):m;
        d = d<10?("0"+d):d;
        h = h<10?("0"+h):h;
        m1 = m1<10?("0"+m1):m1;
        s = s<10?("0"+s):s;
//        return y+"-"+m+"-"+d+" "+h+":"+m1+":"+s;
        return y+"-"+m+"-"+d+" "+h+":"+m1+":"+s;
    }

    function formatDate2(date){
        date = new Date(date);
        var y=date.getFullYear();
        var m=date.getMonth()+1;
        var d=date.getDate();
        var h=date.getHours();
        var m1=date.getMinutes();
        var s=date.getSeconds();
        m = m<10?("0"+m):m;
        d = d<10?("0"+d):d;
        h = h<10?("0"+h):h;
        m1 = m1<10?("0"+m1):m1;
//        s = s<10?("0"+s):s;
//        return y+"-"+m+"-"+d+" "+h+":"+m1+":"+s;
        return y+"-"+m+"-"+d+" "+h+":"+m1;
    }




    /** 导出功能  **/
    function exportexcel(){
        var ordernumber = document.getElementById("ordernumber").value;
        var goodsownername = document.getElementById("goodsownername").value;
        var start = document.getElementById("start").value;
        var end = document.getElementById("end").value;

        if(ordernumber=="" && goodsownername=="" && start=="" && end==""){
            var result = confirm('您未做条件筛选，确定导出全部数据吗？');
            if(result){
                //根据系统时间获取文件名称
                var current =  new Date();
                var desname;
                if($("#desname").val() == ""){
                    desname = "";
                }else{
                    desname = $("#desname option:selected").text();
                }
                if($('#execlTimeName').attr("disabled")!="disabled"){
                    location.href='exportShipInfoAndTransportListExcel.do?start='+$("#start").val() + '&end='+ $("#end").val() + '&ordernumber='+ $("#ordernumber").val() + '&goodsownername='+ $("#goodsownername").val();
                }

            }else{
                return false;
            }
        }else{
            //根据系统时间获取文件名称
            var current =  new Date();
            var desname;
            if($("#desname").val() == ""){
                desname = "";
            }else{
                desname = $("#desname option:selected").text();
            }
            if($('#execlTimeName').attr("disabled")!="disabled"){
                location.href='exportShipInfoAndTransportListExcel.do?start='+$("#start").val() + '&end='+ $("#end").val() + '&ordernumber='+ $("#ordernumber").val() + '&goodsownername='+ $("#goodsownername").val();
            }

        }
    }

    function closeModal4(){
        window.location.reload();
    }
    function timetrans(date){
        var date = new Date(date);//如果date为13位不需要乘1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
        var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
        return Y+M+D+h+m+s;
    }
    // 百度地图API功能
    var map = new BMap.Map("allmap");    // 创建Map实例
    //map.centerAndZoom(new BMap.Point(118.352619,31.31511), 11);  // 初始化地图,设置中心点坐标和地图级别
    // 百度地图API功能:定位当前城市
    var point = new BMap.Point(118.3677390000, 31.3325560000);
    function myFun(result){
        var cityName = result.name;
        map.setCenter(cityName);
    }
    map.centerAndZoom(point,14);  // 编写自定义函数，创建标注
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map.addControl(new BMap.NavigationControl());
    map.enableScrollWheelZoom();//启动鼠标滚轮缩放地图
    map.enableKeyboard();//启动键盘操作地图
    //    去除公路网
    map.setMapStyle({
        styleJson:[
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": {
                    "color": "#ffffff",
                    "visibility": "off"
                }
            },

        ]
    });
    var size = new BMap.Size(20, 30);//添加城市
    // 定义自定义覆盖物的构造函数
    function SquareOverlay(center, length){
        this._center = center;
        this._length = length;
    }
    //监听放大倍数
    function addClickHandler(target,poi, window) {
        map.addEventListener("zoomend", function () {
            var DiTu = this.getZoom();
            var allOverlay = map.getOverlays();
            $('a.islabel').parent()[DiTu>=13?'hide':'show']();
        });
    };
    // 继承API的BMap.Overlay
    SquareOverlay.prototype = new BMap.Overlay();

    // 实现初始化方法
    SquareOverlay.prototype.initialize = function(map){
        // 保存map对象实例
        this._map = map;
        // 外层容器
        var shipMainWrapper = document.createElement("div");
        shipMainWrapper.style.height    = (this._length + 20) + "px";
        shipMainWrapper.style.width     = (this._length + 20) + "px";
        shipMainWrapper.style.textAlign = "center";
        shipMainWrapper.style.position  = "absolute";
        shipMainWrapper.id              = this._center.ship_id;
        //船舶图片容器
        var shipImgWrapper = document.createElement("div");
        var shipImg = document.createElement("img");
        shipImg.src = "${pageContext.request.contextPath}/assets/img/kong.png";
        if (this._center.isEmpty == "1"){
            shipImg.src = "${pageContext.request.contextPath}/assets/img/man.png";
        }
        shipImgWrapper.appendChild(shipImg);
        //船舶名
        var shipName = document.createElement("div");
        shipName.innerText = this._center.shipName;
        //组装
        shipMainWrapper.appendChild(shipImgWrapper);
        shipMainWrapper.appendChild(shipName);
        // 将div添加到覆盖物容器中
        map.getPanes().markerPane.appendChild(shipMainWrapper);
        // 保存div实例
        this._div = shipMainWrapper;
        // 需要将div元素作为方法的返回值，当调用该覆盖物的show、
        // hide方法，或者对覆盖物进行移除时，API都将操作此元素。
        return shipMainWrapper;
    };

    // 实现绘制方法
    SquareOverlay.prototype.draw = function(){
        // 根据地理坐标转换为像素坐标，并设置给容器
        var position = this._map.pointToOverlayPixel(this._center);
        this._div.style.left = position.x - this._length / 2 + "px";
        this._div.style.top = position.y - this._length / 2 + "px";
    };

    // 实现显示方法
    SquareOverlay.prototype.show = function(){
        if (this._div){
            this._div.style.display = "";
        }
    };
    //    // 实现隐藏方法
    //    SquareOverlay.prototype.hide = function(){
    //        if (this._div){
    //            this._div.style.display = "none";
    //        }
    //    }
    //自定义覆盖物添加事件方法
    SquareOverlay.prototype.addEventListener = function(event,fun){
        console.log(this._div);
        this._div['on'+event] = fun;
    };
    //公用的ajax
    function ajaxCustom(url, data, method, async) {
        var i;
        var result = null;
        $.ajax({
            url : url,
            data : data,
            type : method,
            async : async,
            contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            success : function(responseText) {
                result = responseText;
            },
            error : function(msg) {
                console.log(msg);
            },
            complete : function(XHR, TS) {
                XHR = null;
            }
        });
        return result;
    }
    //    $(function(){
    //        shipMap()
    //
    //    });
    //    function searchShipName() {
    //        shipMap()
    //    };

    function toDashboard(){
        location.href="dashboard.do";
    }
    //查询该账户所有的船舶
    function shipMap(tid) {
        map.clearOverlays();
        var accuontUrl = 'selectShipAndDeviceByTid.do';
        var accuontData =  {
            tid:tid
        };
        var accuontJson = $.parseJSON(ajaxCustom(accuontUrl,accuontData, 'POST', false));
        var ships = [];
        for (accuont in accuontJson) {
            console.log(accuontJson);
            ships.push({
                lat:accuontJson[accuont]['deviceInfo']['latitude'],
                lng:accuontJson[accuont]['deviceInfo']['longitude'],
                isEmpty:accuontJson[accuont]['isemtiy'],
                shipName:accuontJson[accuont]['shipname'],
                shipownername:accuontJson[accuont]['shipownername'],
                shipownerphone:accuontJson[accuont]['shipownerphone'],
                devicenumber:accuontJson[accuont]['deviceInfo']['devicenumber'],
                shiplong:accuontJson[accuont]['shiplong'],
                shipwide:accuontJson[accuont]['shipwide'],
                cabinlong:accuontJson[accuont]['cabinlong'],
                cabinwide:accuontJson[accuont]['cabinwide'],
                cabinheight:accuontJson[accuont]['cabinheight'],
                ton:accuontJson[accuont]['ton'],
                uptime:accuontJson[accuont]['deviceInfo']['uptime'],
                ship_id:accuontJson[accuont]['shipid']
            });
        }
        var shipid = "";
        var shipMainObj = {};
        for (var i = 0; i < ships.length; i++) {
            var mySquare = new SquareOverlay(ships[i], 40);
            map.addOverlay(mySquare);
            shipid = ships[i].ship_id;
            (function (i) {
                var point= new BMap.Point(ships[i].lng,ships[i].lat);
                map.centerAndZoom(point,8);
                shipMainObj = document.getElementById(ships[i].ship_id);
                var dates=timetrans(ships[i].uptime);
                var opts = {
                    width : 380,     // 信息窗口宽度
                    height: 220,     // 信息窗口高度
                    title : "<span style='font-size:14px;color:#006bee;margin-bottom:40px;'>"+"数据最后上传时间："+ dates +"</span>", // 信息窗口标题
                    enableMessage:true,//设置允许信息窗发送短息
                }
                var sContent =
                        "<div><table style='font-size: 13px'><tr><td>船名：</td><td>"+ships[i].shipName+"</td><td>船主电话：</td><td>"+ships[i].shipownerphone
                        +"</td></tr><tr><td>船长：</td><td>" +ships[i].shiplong+"</td><td>船舱长度：</td><td>"+ships[i].cabinlong
                        +"</td></tr><tr><td>船宽：</td><td>" +ships[i].shipwide+"</td><td>船舱宽度：</td><td>"+ships[i].cabinwide
                        +"</td></tr><tr><td>吃水深度：</td><td>" +ships[i].cabinheight+"</td><td>船舶吨位：</td><td>"+ships[i].ton
                        +"</td></tr><tr><td>经度：</td><td>" +ships[i].lng+"</td><td>纬度：</td><td>"+ships[i].lat
                        +"</td></tr>"

                        +"</td></tr><tr><td style='color:white'>.</td><td>" +"</td><td></td><td>"
                        +"</td></tr></table>"

                        +"<table><tr><td><a href='shipvideo.do?shipid="+ships[i].ship_id+"' target='_blank'><img src='http://download2.huizhaochuan.com.cn/shipin2gl.png'></a></td>"
                        +"<td><span onclick='had("+ships[i].ship_id+")' style='cursor: pointer'><img src='http://download2.huizhaochuan.com.cn/hangxing2.png'></span></td>"
                        +"</tr></table></div></br>"
                var infoWindow = new BMap.InfoWindow(sContent, opts);
                shipMainObj.addEventListener('click',function(){
                    // window.open("../personal/shipDetail.xhtml?
                    // ="+ships[i].ship_id);
                    map.openInfoWindow(infoWindow,point);
                });
            })(i);
        }
    };

    //添加承运船舶
    function searchShips(page,shipname) {
        $.ajax({
            type : 'post',
            url : 'searchShipInfoByQuery.do?page='+page+'&shipname='+shipname,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                var array =data.shipInfo;
                var str ="";
                $.each(array,function (i) {
                    if(array[i].isemtiy==0){
                        var emptystatu="空载";
                    }
                    else{
                        var emptystatu="满载";
                    }

                    str+="<tr><td><input type='checkbox' name='ck' id='ck"+array[i].shipid+"'onclick='onck("+array[i].shipid+")' value='"+array[i].shipid +"'></td><td>"+ array[i].shipname+"</td><td>"+array[i].deviceInfo.cname+"</td><td>"+array[i].ton+"</td><td>"+emptystatu+"</td><td>"+array[i].deviceInfo.shipspeed+"</td><td><a href='javascript:;' class='btn btn-sm btn-info' onclick='openShipDetail(" +array[i].shipid+")' >视频详情</a></td></tr>"

                })

                $("#tbody").html(str);
                getchecked();
                displayFooter(data.pageInfo);

            }

        });
        $('#myModal7').modal('show');
    }
    function displayFooter(pageInfo) {
        <%--当前第 ${pageInfo.pageNum} 页.总共 ${pageInfo.pages} 页.一共 ${pageInfo.total} 条记录--%>
        var newText = '共' + pageInfo.total + '条记录，' + '当前第' + pageInfo.pageNum + '页，' + '总共' + pageInfo.pages + '页';
        $("#summary").text(newText);
        $("#pagenum").val(pageInfo.pageNum);
        $("#pagesize").val(pageInfo.pages);
    };
    $("#01").click(function(){
        var pagenum = 1;
        searchShips(pagenum,$('#shipnames').val());
    });

    $("#04").click(function(){
        var pagenum =$("#pagenum").val();
        var pagesize =$("#pagesize").val();
        pagenum = pagesize;
        searchShips(pagenum, $('#shipnames').val());
    });

    $("#02").click(function(){
        var pagenum =$("#pagenum").val();
        if(pagenum == 1){
            return false;
        } else{
            pagenum--;
            searchShips(pagenum, $('#shipnames').val());
        }

    });

    $("#03").click(function(){
        var pagenum =$("#pagenum").val();
        var pagesize =$("#pagesize").val();
        if(pagenum == pagesize){
            return false;
        } else{
            pagenum++;
            searchShips(pagenum, $('#shipnames').val());
        }
    });



    function addShip() {
//        var ids = [];
//        $('input[name="checks"]:checked').each(function(){
//            ids.push($(this).val());
//        });
//        $('table input:checked').parents('tr').remove();
        $.ajax({
            type : 'post',
            url : 'searchShipidListByQuery.do?ids='+ids,
            dataType : 'json',
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                var array =data;
                var str ="";
                $.each(array,function (i) {
                    if(array[i].isemtiy==0){
                        var emptystatu="空载";
                    }
                    else{
                        var emptystatu="满载";
                    }
                    str+="<tr id=dd"+ array[i].shipid+"><td>"+ array[i].shipid+"</td><td>"+ array[i].shipname+"</td><td>"+array[i].ton+"</td><td>"+array[i].shipownername+"</td><td>"+array[i].shipownerphone+"</td><td>"+array[i].deviceInfo.cname+"</td><td>"+emptystatu+"</td><td><a href='javascript:;' class='btn btn-sm btn-info'onclick='openShipDetail(" +array[i].shipid+")' >视频详情</a><a href='javascript:;' class='btn  btn-sm btn-info' style='margin-left: 3px' onclick='deleteShip(" +array[i].shipid+")'>删除</a></td><td contenteditable='true'></td></tr>"
                })
                $("#thisBody").append(str);
            }

        })
        $('#myModal7').modal('hide');
    }

    //修改承运船舶
    function searchShipsEdit(page,shipname) {
        var tab=document.getElementById("thisBodyEdit");
        var jsonT ="";
        if ( tab.rows.length!=0){
            jsonT = "[";
            for (var i = 0; i < tab.rows.length; i++) {
                jsonT += '{"shipid":' + tab.rows[i].cells[0].innerHTML + '},'
            }
            jsonT= jsonT.substr(0, jsonT.length - 1);
            jsonT += "]"
        }


        $.ajax({
            type : 'post',
            url : 'searchShipInfoNotByShipid.do',
            data:{
                page:page,
                shipname:shipname,
                shipid:jsonT
            },
            dataType : 'json',
            contentType: "application/x-www-form-urlencoded",
            success : function(data) {
                var array =data.shipInfo;
                var str ="";
                $.each(array,function (i) {
                    if(array[i].isemtiy==0){
                        var emptystatu="空载";
                    }
                    else{
                        var emptystatu="满载";
                    }

                    str+="<tr><td><input type='checkbox' name='ck' id='ck"+array[i].shipid+"' onclick='onck("+array[i].shipid+")' value='"+array[i].shipid +"'></td><td>"+ array[i].shipname+"</td><td>"+array[i].deviceInfo.cname+"</td><td>"+array[i].ton+"</td><td>"+emptystatu+"</td><td>"+array[i].deviceInfo.shipspeed+"</td><td><a href='javascript:;' class='btn btn-sm btn-info' onclick='openShipDetail(" +array[i].shipid+")' >视频详情</a></td></tr>"

                })

                $("#tbodyEdit").html(str);
                getchecked();

                displayFooterEdit(data.pageInfo);

            }

        });
        $('#myModal8').modal('show');

    }

    function displayFooterEdit(pageInfo) {
        <%--当前第 ${pageInfo.pageNum} 页.总共 ${pageInfo.pages} 页.一共 ${pageInfo.total} 条记录--%>
        var newText = '共' + pageInfo.total + '条记录，' + '当前第' + pageInfo.pageNum + '页，' + '总共' + pageInfo.pages + '页';
        $("#summary1").text(newText);
        $("#pagenumEdit").val(pageInfo.pageNum);
        $("#pagesizeEdit").val(pageInfo.pages);

    };

    $("#edit01").click(function(){
        var pagenum = 1;
        searchShipsEdit(pagenum,$('#shipnamesEdit').val());
    });

    $("#edit04").click(function(){
        var pagenum =$("#pagenumEdit").val();
        var pagesize =$("#pagesizeEdit").val();
        pagenum = pagesize;
        searchShipsEdit(pagenum, $('#shipnamesEdit').val());
    });

    $("#edit02").click(function(){
        var pagenum =$("#pagenumEdit").val();
        if(pagenum == 1){
            return false;
        } else{
            pagenum--;
            searchShipsEdit(pagenum, $('#shipnamesEdit').val());
        }
    });

    $("#edit03").click(function(){
        var pagenum =$("#pagenumEdit").val();
        var pagesize =$("#pagesizeEdit").val();
        if(pagenum == pagesize){
            return false;
        } else{
            pagenum++;
            searchShipsEdit(pagenum, $('#shipnamesEdit').val());
        }
    });


    function addShipEdit() {
//        var ids = [];
//        $('input[name="checks"]:checked').each(function(){
//            ids.push($(this).val());
//        });
//        $('table input:checked').parents('tr').remove();
        $.ajax({
            type : 'post',
            url : 'searchShipidListByQuery.do?ids='+ids,
            dataType : 'json',
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                var array =data;
                var str ="";
                $.each(array,function (i) {
                    if(array[i].isemtiy==0){
                        var emptystatu="空载";
                    }
                    else{
                        var emptystatu="满载";
                    }
                    str+="<tr id=dd"+ array[i].shipid+"><td>"+ array[i].shipid+"</td><td>"+ array[i].shipname+"</td><td>"+array[i].ton+"</td><td>"+array[i].shipownername+"</td><td>"+array[i].shipownerphone+"</td><td>"+array[i].deviceInfo.cname+"</td><td>"+emptystatu+"</td><td><a href='javascript:;' class='btn btn-sm btn-info'onclick='openShipDetail(" +array[i].shipid+")' >视频详情</a><a href='javascript:;' class='btn  btn-sm btn-info' style='margin-left: 5px' onclick='deleteShip(" +array[i].shipid+")'>删除</a></td><td contenteditable='true'></td></tr>"
                })
                $("#thisBodyEdit").append(str);
            }
        })
        $('#myModal8').modal('hide');

//       $("input[name='checks']:checked").each(function() { // 遍历选中的checkbox
//           n = $(this).parents("tr").index();  // 获取checkbox所在行的顺序
//           alert(n);
//           $("#table_test").find("tr:eq("+n+")").remove();
//       });
    }

    function submitTransport(tid) {
        var tab="";
        var goodsownername="";
        var goodsphone="";
        var goodsname="";
        var totaltonnage="";
        var startdate="";
        var enddate="";
        var originating="";
        var destination="";
        var scheduler="";
        var remarkes="";
        var ordernumber="";
        if(tid != null && tid !="") {
            goodsownername=$("#goodsownernameEdit").val();
            goodsphone=$("#goodsphoneEdit").val();
            goodsname=$("#goodsnameEdit").val();
            totaltonnage=$("#totaltonnageEdit").val();
            startdate=$("#startdateEdit").val();
            enddate=$("#enddateEdit").val();
            originating=$("#originatingEdit").val();
            destination=$("#destinationEdit").val();
            scheduler=$("#schedulerEdit").val();
            remarkes=$("#remarkesEdit").val();
            ordernumber=$("#ordernumberEdit").val();
            tab=document.getElementById("thisBodyEdit");

            var jsonT = "[";
            for (var i = 0; i < tab.rows.length; i++) {
                jsonT += '{"shipid":' + tab.rows[i].cells[0].innerHTML + ',"agentname":"' + tab.rows[i].cells[8].innerHTML + '"},'
            }
            jsonT= jsonT.substr(0, jsonT.length - 1);
            jsonT += "]";


            if (goodsownername=='') {
                alert("货主姓名不能为空！");
                $("#goodsownernameEdit").focus();
                return false;
            };
            if (goodsphone =='') {
                alert("请核对电话号码！");
                $("#goodsphoneEdit").focus();
                return false;
            };
            if (goodsname =='') {
                alert("货物类型不能为空！");
                $("#goodsnameEdit").focus();
                return false;
            };
            if (totaltonnage =='') {
                alert("总吨位不能为空！");
                $("#totaltonnageEdit").focus();
                return false;
            };
            if (originating =='') {
                alert("发货地不能为空！");
                $("#originatingEdit").focus();
                return false;
            };
            if (startdate =='') {
                alert("计划开始时间不能为空！");
                $("#startdateEdit").focus();
                return false;
            };
            if (destination =='') {
                alert("目的地不能为空！");
                $("#destinationEdit").focus();
                return false;
            };
            if (enddate =='') {
                alert("计划完成时间不能为空！");
                $("#enddateEdit").focus();
                return false;
            };
            if (ordernumber =='') {
                alert("计划编号不能为空！");
                $("#ordernumberEdit").focus();
                return false;
            };
            if (scheduler =='') {
                alert("计划创建人不能为空");
                $("#schedulerEdit").focus();
                return false;
            };
            if (tab.rows.length ==0) {
                alert("请至少选择一搜承运船舶！");
                return false;
            };
            if (startdate > enddate) {
                alert("计划完成时间不可早于计划开始时间！");
                return false;
            };

        }else{
            goodsownername=$("#goodsownernameAdd").val();
            goodsphone=$("#goodsphoneAdd").val();
            goodsname=$("#goodsnameAdd").val();
            totaltonnage=$("#totaltonnageAdd").val();
            startdate=$("#startdateAdd").val();
            enddate=$("#enddateAdd").val();
            originating=$("#originatingAdd").val();
            destination=$("#destinationAdd").val();
            scheduler=$("#schedulerAdd").val();
            remarkes=$("#remarkesAdd").val();
            ordernumber=$("#ordernumberAdd").val();
            tab=document.getElementById("thisBody");

            var jsonT = "[";
            for (var i = 0; i < tab.rows.length; i++) {
                jsonT += '{"shipid":' + tab.rows[i].cells[0].innerHTML + ',"agentname":"' + tab.rows[i].cells[8].innerHTML + '"},'
            }
            jsonT= jsonT.substr(0, jsonT.length - 1);
            jsonT += "]";


            if (goodsownername=='') {
                alert("货主姓名不能为空！");
                $("#username").focus();
                return false;
            };
            if (goodsphone =='') {
                alert("货主电话不能为空！");
                $("#goodsphoneAdd").focus();
                return false;
            };
            if (goodsname =='') {
                alert("货物类型不能为空！");
                $("#goodsnameAdd").focus();
                return false;
            };
            if (totaltonnage =='') {
                alert("总吨位不能为空！");
                $("#totaltonnageAdd").focus();
                return false;
            };
            if (originating =='') {
                alert("发货地不能为空！");
                $("#originatingAdd").focus();
                return false;
            };
            if (startdate =='') {
                alert("计划开始时间不能为空！");
                $("#startdateAdd").focus();
                return false;
            };
            if (destination =='') {
                alert("目的地不能为空！");
                $("#destinationAdd").focus();
                return false;
            };
            if (enddate =='') {
                alert("计划完成时间不能为空！");
                $("#enddateAdd").focus();
                return false;
            };
            if (ordernumber =='') {
                alert("计划编号不能为空！");
                $("#ordernumberAdd").focus();
                return false;
            };
            if (scheduler =='') {
                alert("计划创建人不能为空");
                $("#schedulerAdd").focus();
                return false;
            };
            if (tab.rows.length ==0) {
                alert("请至少选择一搜承运船舶！");
                return false;
            };
            if (startdate > enddate) {
                alert("计划完成时间不可早于计划开始时间！");
                return false;
            };

        }
        $.ajax({
            type : 'post',
            url : 'addShipInfoByQuery.do',
            data:{
                tid:tid,
                goodsownername:goodsownername,
                goodsphone:goodsphone,
                goodsname:goodsname,
                totaltonnage:totaltonnage,
                startdate:startdate,
                enddate:enddate,
                originating:originating,
                destination:destination,
                scheduler:scheduler,
                remarkes:remarkes,
                ordernumber:ordernumber,
                tabData:jsonT
            },
            dataType : 'json',
            traditional: true,
            contentType: "application/x-www-form-urlencoded",
            success : function(data) {
                if(data==true){
                    alert("恭喜，计划保存成功！")
                    $('#myModal5').modal('hide');
                    window.location.reload();

                }else{
                    var msg =data;
                    alert(msg);
                }
            }
        })
    };
    //编辑运输计划
    function tansprotEdit(tid) {
        $.post("addTransport.do");
        $.ajax({
            type : 'post',
            url : 'selectTransportPlanByTid.do?tid='+tid,
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function (data) {
                var array = data.shipInfoList;
                $("#goodsownernameEdit").val(data.goodsInfo.goodsownername);
                $("#goodsphoneEdit").val(data.goodsInfo.goodsphone);
                $("#goodsnameEdit").val(data.goodsname);
                $("#totaltonnageEdit").val(data.totaltonnage);
                $("#startdateEdit").val(formatDate1(data.startdate));
                $("#enddateEdit").val(formatDate1(data.enddate));
                $("#originatingEdit").val(data.originating);
                $("#destinationEdit").val(data.destination);
                $("#schedulerEdit").val(data.scheduler);
                $("#remarkesEdit").val(data.remarkes);
                $("#ordernumberEdit").val(data.ordernumber);
                $("#tidEdit").val(tid);
                var str ="";
                $.each(array,function (i) {
                    if(array[i].isemtiy==0){
                        var emptystatu="空载";
                    }
                    else{
                        var emptystatu="满载";
                    }
                    str+="<tr id=dd"+ array[i].shipid+"><td>"+ array[i].shipid+"</td><td>"+ array[i].shipname+"</td><td>"+array[i].ton+"</td><td>"+array[i].shipownername+"</td><td>"+array[i].shipownerphone+"</td><td>"+array[i].deviceInfo.cname+"</td><td>"+emptystatu+"</td><td><a href='javascript:;' class='btn btn-sm btn-info'onclick='openShipDetail(" +array[i].shipid+")' >视频详情</a><a href='javascript:;' class='btn  btn-sm btn-info' style='margin-left: 5px' onclick='deleteShip(" +array[i].shipid+")'>删除</a></td><td contenteditable='true'>"+array[i].shipAndTransport.agentname +"</td></tr>"
                })
                $("#thisBodyEdit").html(str);
                $('#myModal5').modal('show');
            }
        })
    };

    function openShipDetail(shipid) {
//        location.href="shipInfoDeail.do?shipid="+shipid;
        window.open("shipvideo.do?shipid="+shipid);
    };

    function deleteShip (shipid) {

        $("#dd"+shipid).remove();

        $.ajax({
            type : 'post',
            url : 'updateShipidByQuery.do?shipid='+shipid,
            dataType : 'json',
            traditional: true,
            contentType: "application/json; charset=utf-8",
        })
    };

    //修改后提交
    function selectOredernumber(){
        $.ajax({
            type: 'post',
            url: 'selectTransportOrdernumber.do?ordernumber='+$("#ordernumberAdd").val(),
            dataType: 'json',
            traditional: true,
            contentType: "application/json; charset=utf-8",
            success:function (data) {
                if (data==true){
                    $("#msg").text("该计划编号已存在");
                }else{
                    $("#msg").text("");
                }
            }
        })
    };
    //       筛选船舶判断有没有数据
    function shaixuanOutpu(page) {
        var oBtn = document.getElementById('execlTimeName');
        if (page == 0) {
            oBtn.disabled = 'disabled';
        }
        return false;

    };
    function autoload(page) {
        var outpubutton= document.getElementById("execlTimeName");
        if(page==0){
            document.getElementById( "noneData").style.display= "block";
            outpubutton.disabled=true;
        }else {
            document.getElementById( "noneData").style.display= "none";
            outpubutton.disabled=false;
        }

    };

    function transportOver(tid) {
        var result = confirm('真的要中止计划么？取消后可到回收站恢复！');
        if(result){
            $.ajax({
                type : 'post',
                url : 'updateTransportByStats.do?stats=1&tid='+tid ,
                dataType : 'json',
                contentType: "application/json; charset=utf-8",
                success : function (data) {
                    if(data ==true){
                        alert("删除成功");
                        window.location.reload();
                    }
                }
            })

        }else{
            return false;
        }
    };


    var ids =[];
function onck(x) {
    if($("#ck"+x).is(':checked')){
        ids.push(x);
        console.log(ids);
    }else{
        for(var i=0; i<ids.length; i++){
            if(x== ids[i]){
                ids.splice(i, 1);
                console.log(ids);
                break;
            }
        }
    }
}



    function getchecked() {
    debugger;
        var onechs = document.getElementsByName("ck");
        if(ids==""){
            return;
        }
        for(var i =0;i<onechs.length;i++){
            for(var j =0;j<ids.length;j++){
                if(onechs[i].value==ids[j]){
                    onechs[i].checked =true;
                }
            }
        }
        debugger;
    };


</script>


</html>
