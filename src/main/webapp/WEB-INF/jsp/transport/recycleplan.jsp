<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/23
  Time: 9:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>易航oTMS</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

    <!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
    <link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- layui -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">



</head>
<body>

<div class="wrapper">

    <%@include file="../common/left.jsp"%>

    <div class="main-panel">

       <nav class="navbar navbar-default navbar-fixed">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <p class="navbar-brand"><a href="searchTransport.do">运输计划</a>>回收站</p>

        </div>
        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a>
                        ${sessionScope.username}
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        设置
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#" data-toggle="modal" data-target="#myModal3">密码修改</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#myModal99">退出登录</a></li>
                        <li class="divider"></li>
                        <li><a href="totmslog.do">更新日志</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>

        <div class="content">
            <div class="container-fluid">


                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12">
                        <div class="card" >

                            <div class="panel-group" id="accordion">
                                <c:forEach items="${list}" var="plan">
                                    <div class="panel panel-default  in" style="margin-top: 20px;margin-left: 5px;margin-right: 5px; box-shadow: 0px 4px 12px #d6d6d6;">
                                        <div class="panel-heading">
                                            <div class="row" style="font-weight:bold;font-size: 10px">
                                                <div class="col-md-3">计划编号：${plan.ordernumber}</div>
                                                <div class="col-md-3">货主名称:${plan.goodsInfo.goodsownername}</div>
                                                <div class="col-md-2 ">货主手机：${plan.goodsInfo.goodsphone}</div>
                                                <div class="col-md-2">计划开始时间:<fmt:formatDate pattern="yyyy-MM-dd" value="${plan.startdate}" /></div>
                                                <div class="col-md-2">计划完成时间:<fmt:formatDate pattern="yyyy-MM-dd" value="${plan.enddate}" /></div>
                                            </div>
                                            <h1 class="page-header" style="padding-bottom:10px;margin:10 0 10"></h1>
                                            <div class="row">
                                                <div class="col-md-3">航程：${plan.originating}-${plan.destination}</div>
                                                <div class="col-md-3">货物类型：${plan.goodsname}</div>
                                                <div class="col-md-2">总吨位：${plan.totaltonnage}吨</div>
                                                <c:set var="nowDate" value="<%=System.currentTimeMillis()%>"></c:set>
                                                <c:choose>
                                                    <c:when test="${nowDate-plan.enddate.getTime() > 0}">
                                                        <div class="col-md-2 pull-right"><span style="background-color:#acacac;color: white;border-radius: 3px;font-size: 12px">&nbsp;&nbsp;已完成&nbsp;&nbsp;</span></div>
                                                    </c:when>
                                                    <c:when test="${nowDate-plan.startdate.getTime() > 0}">
                                                        <div class="col-md-2 pull-right"><span style="background-color: green;color: white;border-radius: 3px;font-size: 12px">&nbsp;&nbsp;运输中&nbsp;&nbsp;</span></div>
                                                    </c:when>
                                                    <c:when test="${nowDate-plan.startdate.getTime() < 0}">
                                                        <div class="col-md-2 pull-right"><span style="background-color: #ff7300;color: white;border-radius: 3px;font-size: 12px">&nbsp;&nbsp;待装船&nbsp;&nbsp;</span></div>
                                                    </c:when>
                                                </c:choose>
                                                <div class="col-md-2"></div>
                                            </div>
                                            <h1 class="page-header" style="padding-bottom:10px;margin:10 0 10"></h1>
                                            <div class="row" style="margin-top: 5px;">
                                                <div class="col-md-3 pull-left">系统流水号：${plan.tanordernumber}</div>
                                                <div class="col-md-3">创建时间：<fmt:formatDate pattern="yyyy-MM-dd" value="${plan.creationdate}" /></div>
                                                <div class="col-md-2">创建人：<span id="${plan.transportid}">${plan.scheduler}</span></div>
                                                <c:choose>
                                                    <c:when test="${plan.stats ==1}">
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-1"><input type="button" class="btn btn-primary btn-sm btn-xs pull-right" id="overPlan${plan.ordernumber}" onclick="transportBack(${plan.transportid})" value="还原计划"></div>
                                                    </c:when>
                                                </c:choose>

                                            </div>

                                        </div>
                                        <div id="collapseOne${plan.transportid}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <table class="table" id="shipinfo">
                                                    <thead>
                                                    <tr>
                                                        <th>船名</th>
                                                        <th>联系电话</th>
                                                        <th>受载/计划吨位</th>
                                                        <th>装满时间</th>
                                                        <th>卸空时间</th>
                                                        <th>实收吨位</th>
                                                        <th>实时位置</th>
                                                        <th>船舶状态</th>
                                                        <th>航速(节)</th>
                                                        <th>操作</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbody${plan.transportid}"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--文字信息-->
                        <div class="col-md-6" style="margin-top:30px;">
                            当前第 ${pageInfo.pageNum} 页.总共 ${pageInfo.pages} 页.一共 ${pageInfo.total} 条记录
                        </div>

                        <!--点击分页-->
                        <div class="col-md-6">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">

                                    <li><a href="javascript:void(0);" onclick="chickPage(1)">首页</a></li>

                                    <!--上一页-->
                                    <li>
                                        <c:if test="${pageInfo.hasPreviousPage}">
                                            <a href="javascript:void(0);" onclick="chickPage(${pageInfo.pageNum-1})" aria-label="Previous">
                                                <span aria-hidden="true">«</span>
                                            </a>
                                        </c:if>
                                    </li>

                                    <!--循环遍历连续显示的页面，若是当前页就高亮显示，并且没有链接-->
                                    <c:forEach items="${pageInfo.navigatepageNums}" var="page_num">
                                        <c:if test="${page_num == pageInfo.pageNum}">
                                            <li class="active"><a href="#">${page_num}</a></li>
                                        </c:if>
                                        <c:if test="${page_num != pageInfo.pageNum}">
                                            <li><a href="javascript:void(0);" onclick="chickPage(${page_num})">${page_num}</a></li>
                                        </c:if>
                                    </c:forEach>

                                    <!--下一页-->
                                    <li>
                                        <c:if test="${pageInfo.hasNextPage}">
                                            <a href="javascript:void(0);" onclick="chickPage(${pageInfo.pageNum+1})"
                                               aria-label="Next">
                                                <span aria-hidden="true">»</span>
                                            </a>
                                        </c:if>
                                    </li>

                                    <li> <a href="javascript:void(0);" onclick="chickPage(${pageInfo.pages})">尾页</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>






<%@include file="../common/modal.jsp"%>

</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>

<%--<!--  Checkbox, Radio & Switch Plugins -->--%>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>

<script type="text/javascript">

    document.getElementById("active3").className = "active";
    document.getElementById("crumbs").innerHTML="回收站";

    function formatDate(date){
        date = new Date(date);
        var y=date.getFullYear();
        var m=date.getMonth()+1;
        var d=date.getDate();
        var h=date.getHours();
        var m1=date.getMinutes();
        var s=date.getSeconds();
        m = m<10?("0"+m):m;
        d = d<10?("0"+d):d;
//        return y+"-"+m+"-"+d+" "+h+":"+m1+":"+s;
        return y+"-"+m+"-"+d;
    }

    function  chickPage(pn) {
        location.href='torecycleplan.do?pn='+pn;
    }

    function transportBack(tid) {
        var result = confirm('真的要还原计划么？');
        if(result){
            $.ajax({
                type : 'post',
                url : 'updateTransportByStats.do?stats=0&tid='+tid ,
                dataType : 'json',
                contentType: "application/json; charset=utf-8",
                success : function (data) {
                    if(data ==true){
                        alert("还原成功");
                        window.location.reload();
                    }
                }
            })
        }else{
            return false;
        }

    }

</script>

</html>
