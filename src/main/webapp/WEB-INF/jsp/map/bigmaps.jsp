<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/21
  Time: 13:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="cn">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>船舶地图</title>

    <style type="text/css">
        #allmap {width: 100%;height:100%;overflow: hidden;margin:0;font-family:"微软雅黑";}
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=WnAUIrAmQK8oUf0a5tgKxqRfpAA59iCc"></script>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>

    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

    <!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
    <link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>


<div class="map-topbar">
    <input type="hidden" id="username" value="${sessionScope.username}">
    <a href="maps.do" class="logo">
        <%--<img src="http://download2.huizhaochuan.com.cn/bigmaplogo_${sessionScope.username}.png" style="margin-top:8px;float: left">--%>
        <img src="http://d.yihangotms.com/images/bigmaplogo_${sessionScope.username}.png" style="margin-top:8px;float: left">
    </a>

    <a class="menu-btn">
        <button type="button" class="btn btn-primary" data-toggle="button"  onclick="toDashboard()" style="background-color: #006bee;border: none;float:right;margin-top:8px;"> <a class="caidan-top"><img src="assets/img/closebigmap.png" style="width:25px;height:25px;"></a></button>
    </a>

</div>
<div class="img-map" style="top: 70px">
    <div class="input-group">
        <input type="text" class="form-control" id="shipname" placeholder="请输入船名" style="width: 250px">
        <span class="input-group-btn" style="float: left">
             <button class="btn btn-default" id="searchButton" type="button" style="background-color:#006bee;height:40px;color: white" onclick="searchShipName()">找船</button>
            </span>
    </div>
</div>
<div class="change-pass-right3" style="top: 70px">
    <label class="shipmaptest shipmaptest-click" id="allship" onclick="allship()">&nbsp;&nbsp;<i class="pe-7s-box2"></i> 全部船舶&nbsp;&nbsp;</label>
    <label class="shipmaptest" id="emptyship" onclick="emptyship(0)">&nbsp;&nbsp;<i class="pe-7s-ball"></i>空载&nbsp;&nbsp;</label>
    <label class="shipmaptest" id="fullship" onclick="emptyship(1)">&nbsp;&nbsp;<i class="pe-7s-portfolio"></i>满载&nbsp;&nbsp;</label>
    <%--<label class="shipmaptest" id="unlineship" onclick="emptyship(2)">&nbsp;&nbsp;<i class="pe-7s-plug"></i>离线&nbsp;&nbsp;</label>--%>
</div>
<div id="allmap"></div>


</body>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<script>
    $(function() {
        $(".shipmaptest").click(function () {
            $(".shipmaptest").removeClass("shipmaptest-click");
            $(this).addClass("shipmaptest-click");
        })
    })

    function allship() {
//        $(".form-control").val('');
        shipMap();
    }
    function emptyship(emtiy) {
        map.clearOverlays();
        var accuontUrl = 'selectShipAndDeviceShowMap.do';
        var accuontData =  {
            emtiy:emtiy
        };
        var accuontJson = $.parseJSON(ajaxCustom(accuontUrl,accuontData, 'POST', false));
        var ships = [];
        if (accuontJson == false){
            alert("没有相关船舶！");
        }else{
            for (accuont in accuontJson) {
                // alert(accuont);
                ships.push({
                    lat:accuontJson[accuont]['deviceInfo']['latitude'],
                    lng:accuontJson[accuont]['deviceInfo']['longitude'],
                    isEmpty:accuontJson[accuont]['isemtiy'],
                    shipName:accuontJson[accuont]['shipname'],
                    shipownername:accuontJson[accuont]['shipownername'],
                    shipownerphone:accuontJson[accuont]['shipownerphone'],
                    devicenumber:accuontJson[accuont]['deviceInfo']['devicenumber'],
                    isonline:accuontJson[accuont]['deviceInfo']['isonline'],
                    shiplong:accuontJson[accuont]['shiplong'],
                    shipwide:accuontJson[accuont]['shipwide'],
                    cabinlong:accuontJson[accuont]['cabinlong'],
                    cabinwide:accuontJson[accuont]['cabinwide'],
                    cabinheight:accuontJson[accuont]['cabinheight'],
                    loaddraft:accuontJson[accuont]['loaddraft'],
                    ton:accuontJson[accuont]['ton'],
                    uptime:accuontJson[accuont]['deviceInfo']['uptime'],
                    ship_id:accuontJson[accuont]['shipid']

                });
            }
            var shipid = "";
            var shipMainObj = {};
            for (var i = 0; i < ships.length; i++) {
                var mySquare = new SquareOverlay(ships[i], 40);
                map.addOverlay(mySquare);
                shipid = ships[i].ship_id;
                (function (i) {
                    var point= new BMap.Point(ships[i].lng,ships[i].lat);
                    map.centerAndZoom(point,9);
                    shipMainObj = document.getElementById(ships[i].ship_id);
                    console.log(ships[i].uptime);
                    var dates=timetrans(ships[i].uptime);
                    var opts = {
                        width : 400,     // 信息窗口宽度
                        height: 220,     // 信息窗口高度
                        title : "<span style='font-size:14px;color:#006bee;margin-bottom:40px;'>"+"数据最后上传时间："+ dates +"</span>", // 信息窗口标题
                        enableMessage:true,//设置允许信息窗发送短息
                    }
                    var sContent =
                            "<div><table style='font-size: 13px'><tr><td>船名：</td><td>"+ships[i].shipName+"</td><td>&nbsp;&nbsp;&nbsp;船主电话：</td><td>"+ships[i].shipownerphone
                            +"</td></tr><tr><td>船长：</td><td>" +ships[i].shiplong+ "米"+"</td><td>&nbsp;&nbsp;&nbsp;船舱长度：</td><td>"+ships[i].cabinlong+ "米"
                            +"</td></tr><tr><td>船宽：</td><td>" +ships[i].shipwide+ "米"+ "</td><td>&nbsp;&nbsp;&nbsp;船舱宽度：</td><td>"+ships[i].cabinwide+ "米"
                            +"</td></tr><tr><td>船舱深度：</td><td>" +ships[i].cabinheight+ "米"+ "</td><td>&nbsp;&nbsp;&nbsp;船舶吨位：</td><td>"+ships[i].ton+ "吨"
                            +"</td></tr><tr><td>重载吃水深度：</td><td>" +ships[i].loaddraft+"米"
                            +"</td></tr><tr><td>经度：</td><td>" +ships[i].lng+"</td><td>&nbsp;&nbsp;&nbsp;纬度：</td><td>"+ships[i].lat
                            +"</td></tr>"
                            +"</td></tr><tr><td style='color:white'>.</td><td>" +"</td><td></td><td>"
                            +"</td></tr></table>"

                            +"<table><tr><td><a href='shipvideo.do?shipid="+ships[i].ship_id+"' target='_blank'><img src='http://download2.huizhaochuan.com.cn/shipin2gl.png'></a></td>"
                            +"<td><span onclick='had("+ships[i].ship_id+")' style='cursor: pointer'><img src='http://download2.huizhaochuan.com.cn/hangxing2.png'></span></td>"


                            +"</tr></table></div></br>";
                    var infoWindow = new BMap.InfoWindow(sContent, opts);
                    shipMainObj.addEventListener('click',function(){
                        // window.open("../personal/shipDetail.xhtml?ship_id="+ships[i].ship_id);
                        map.openInfoWindow(infoWindow,point);
                    });
                })(i);
            }
        }
    }
</script>
<script type="text/javascript">

    function timetrans(date) {
        var newTime = new Date(date);
        var year = newTime.getFullYear();
        var mon = newTime.getMonth() + 1;
        var day = newTime.getDate();
        var hour = newTime.getHours();
        var min = newTime.getMinutes();
        var sec = newTime.getSeconds();
        var newTimes = year + "-" + (mon < 10 ? ('0' + mon) : mon) + "-"
                + (day < 10 ? ('0' + day) : day) + " "
                +"\n"+ (hour < 10 ? ('0' + hour) : hour) + ":"
                + (min < 10 ? ('0' + min) : min) + ":"
                + (sec < 10 ? ('0' + sec) : sec);
        return newTimes;
    }
    // 百度地图API功能
    var map = new BMap.Map("allmap");    // 创建Map实例
    var point = new BMap.Point(118.352619,31.31511);
    map.setCurrentCity("芜湖");          // 设置地图显示的城市 此项是必须设置的
    //    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放

    map.centerAndZoom(point,9);  // 编写自定义函数，创建标注
    map.addControl(new BMap.NavigationControl({
        anchor: BMAP_ANCHOR_BOTTOM_RIGHT, //表示控件定位于地图的有下角。
    }));

    //    去除公路网
    map.setMapStyle({
        styleJson:[
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": {
                    "color": "#ffffff",
                    "visibility": "off"
                }
            },

        ]
    });
    map.enableScrollWheelZoom();//启动鼠标滚轮缩放地图
    map.enableKeyboard();//启动键盘操作地图
    var size = new BMap.Size(20, 30);//添加城市
    // 定义自定义覆盖物的构造函数
    function SquareOverlay(center, length){
        this._center = center;
        this._length = length;
    }
    //监听放大倍数
    function addClickHandler(target,poi, window) {
        map.addEventListener("zoomend", function () {
            var DiTu = this.getZoom();
            var allOverlay = map.getOverlays();
            $('a.islabel').parent()[DiTu>=13?'hide':'show']();
        });
    };
    // 继承API的BMap.Overlay
    SquareOverlay.prototype = new BMap.Overlay();

    // 实现初始化方法
    SquareOverlay.prototype.initialize = function(map){
        // 保存map对象实例
        this._map = map;
        // 外层容器
        var shipMainWrapper = document.createElement("div");
        shipMainWrapper.style.height    = (this._length + 20) + "px";
        shipMainWrapper.style.width     = (this._length + 20) + "px";
        shipMainWrapper.style.textAlign = "center";
        shipMainWrapper.style.position  = "absolute";
        shipMainWrapper.id              = this._center.ship_id;
        //船舶图片容器
        var shipImgWrapper = document.createElement("div");
        var shipImg = document.createElement("img");
        if(this._center.isonline=="1"){
            shipImg.src = "${pageContext.request.contextPath}/assets/img/unline.png";
        }else{
            if (this._center.isEmpty == "1"){
                shipImg.src = "${pageContext.request.contextPath}/assets/img/man.png";
            }else{
                shipImg.src = "${pageContext.request.contextPath}/assets/img/kong.png";
            }
        }

        shipImgWrapper.appendChild(shipImg);
        //船舶名
        var shipName = document.createElement("div");
        shipName.innerText = this._center.shipName;
        //组装
        shipMainWrapper.appendChild(shipImgWrapper);
        shipMainWrapper.appendChild(shipName);
        // 将div添加到覆盖物容器中
        map.getPanes().markerPane.appendChild(shipMainWrapper);
        // 保存div实例
        this._div = shipMainWrapper;
        // 需要将div元素作为方法的返回值，当调用该覆盖物的show、
        // hide方法，或者对覆盖物进行移除时，API都将操作此元素。
        return shipMainWrapper;
    };

    // 实现绘制方法
    SquareOverlay.prototype.draw = function(){
        // 根据地理坐标转换为像素坐标，并设置给容器
        var position = this._map.pointToOverlayPixel(this._center);
        this._div.style.left = position.x - this._length / 2 + "px";
        this._div.style.top = position.y - this._length / 2 + "px";
    };

    // 实现显示方法
    SquareOverlay.prototype.show = function(){
        if (this._div){
            this._div.style.display = "";
        }
    };
    //    // 实现隐藏方法
    //    SquareOverlay.prototype.hide = function(){
    //        if (this._div){
    //            this._div.style.display = "none";
    //        }
    //    }
    //自定义覆盖物添加事件方法
    SquareOverlay.prototype.addEventListener = function(event,fun){
        console.log(this._div);
        this._div['on'+event] = fun;
    };
    //公用的ajax
    function ajaxCustom(url, data, method, async) {
        var i;
        var result = null;
        $.ajax({
            url : url,
            data : data,
            type : method,
            async : async,
            contentType : "application/x-www-form-urlencoded; charset=UTF-8",
            success : function(responseText) {
                result = responseText;
            },
            error : function(msg) {
                console.log(msg);
            },
            complete : function(XHR, TS) {
                XHR = null;
            }
        });
        return result;
    }
    $(function(){
        shipMap()

    });
    function searchShipName() {
        $(".shipmaptest").removeClass("shipmaptest-click");
        shipMap();
        $("#shipname").val("");
    };

    function toDashboard(){
        location.href="maps.do";
    }
    //查询该账户所有的船舶
    function shipMap() {
        map.clearOverlays();
        var accuontUrl = 'selectShipAndDevice.do';
        var accuontData =  {
            shipname:$("#shipname").val()
        };
        var accuontJson = $.parseJSON(ajaxCustom(accuontUrl,accuontData, 'POST', false));
        var ships = [];
        if (accuontJson == false){
            alert("没有相关船舶！");
        }else{
            for (accuont in accuontJson) {
                // alert(accuont);
                ships.push({
                    lat:accuontJson[accuont]['deviceInfo']['latitude'],
                    lng:accuontJson[accuont]['deviceInfo']['longitude'],
                    isEmpty:accuontJson[accuont]['isemtiy'],
                    shipName:accuontJson[accuont]['shipname'],
                    shipownername:accuontJson[accuont]['shipownername'],
                    shipownerphone:accuontJson[accuont]['shipownerphone'],
                    devicenumber:accuontJson[accuont]['deviceInfo']['devicenumber'],
                    isonline:accuontJson[accuont]['deviceInfo']['isonline'],
                    shiplong:accuontJson[accuont]['shiplong'],
                    shipwide:accuontJson[accuont]['shipwide'],
                    cabinlong:accuontJson[accuont]['cabinlong'],
                    cabinwide:accuontJson[accuont]['cabinwide'],
                    cabinheight:accuontJson[accuont]['cabinheight'],
                    loaddraft:accuontJson[accuont]['loaddraft'],
                    ton:accuontJson[accuont]['ton'],
                    uptime:accuontJson[accuont]['deviceInfo']['uptime'],
                    ship_id:accuontJson[accuont]['shipid']

                });
            }
            var shipid = "";
            var shipMainObj = {};
            for (var i = 0; i < ships.length; i++) {
                var mySquare = new SquareOverlay(ships[i], 40);
                map.addOverlay(mySquare);
                shipid = ships[i].ship_id;
                (function (i) {
                    var point= new BMap.Point(ships[i].lng,ships[i].lat);
                    map.centerAndZoom(point,9);
                    shipMainObj = document.getElementById(ships[i].ship_id);
                    console.log(ships[i].uptime);
                    var dates=timetrans(ships[i].uptime);
                    var opts = {
                        width : 400,     // 信息窗口宽度
                        height: 220,     // 信息窗口高度
                        title : "<span style='font-size:14px;color:#006bee;margin-bottom:40px;'>"+"数据最后上传时间："+ dates +"</span>", // 信息窗口标题
                        enableMessage:true,//设置允许信息窗发送短息
                    }
                    var sContent =
                            "<div><table style='font-size: 13px'><tr><td>船名：</td><td>"+ships[i].shipName+"</td><td>&nbsp;&nbsp;&nbsp;船主电话：</td><td>"+ships[i].shipownerphone
                            +"</td></tr><tr><td>船长：</td><td>" +ships[i].shiplong+ "米"+"</td><td>&nbsp;&nbsp;&nbsp;船舱长度：</td><td>"+ships[i].cabinlong+ "米"
                            +"</td></tr><tr><td>船宽：</td><td>" +ships[i].shipwide+ "米"+ "</td><td>&nbsp;&nbsp;&nbsp;船舱宽度：</td><td>"+ships[i].cabinwide+ "米"
                            +"</td></tr><tr><td>船舱深度：</td><td>" +ships[i].cabinheight+ "米"+ "</td><td>&nbsp;&nbsp;&nbsp;船舶吨位：</td><td>"+ships[i].ton+ "吨"
                            +"</td></tr><tr><td>重载吃水深度：</td><td>" +ships[i].loaddraft+"米"
                            +"</td></tr><tr><td>经度：</td><td>" +ships[i].lng+"</td><td>&nbsp;&nbsp;&nbsp;纬度：</td><td>"+ships[i].lat
                            +"</td></tr>"
                            +"</td></tr><tr><td style='color:white'>.</td><td>" +"</td><td></td><td>"
                            +"</td></tr></table>"

                            +"<table><tr><td><a href='shipvideo.do?shipid="+ships[i].ship_id+"' target='_blank'><img src='http://download2.huizhaochuan.com.cn/shipin2gl.png'></a></td>"
                            +"<td><span onclick='had("+ships[i].ship_id+")' style='cursor: pointer'><img src='http://download2.huizhaochuan.com.cn/hangxing2.png'></span></td>"


                            +"</tr></table></div></br>";
                    var infoWindow = new BMap.InfoWindow(sContent, opts);
                    shipMainObj.addEventListener('click',function(){
                        // window.open("../personal/shipDetail.xhtml?ship_id="+ships[i].ship_id);
                        map.openInfoWindow(infoWindow,point);
                    });
                })(i);
            }
        }
    };
</script>
<%--enter键登录--%>
<script>
    function had(shipid) {
        window.open("shipvideo.do?shipid="+shipid+"#allmap");

//        var openUrl = "shipmap.do?shipid="+shipid;//弹出窗口的url
//        var iWidth=1200; //弹出窗口的宽度;
//        var iHeight=600; //弹出窗口的高度;
//        var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
//        var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;
//        window.open(openUrl,"","height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft );

    }
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            $("#searchButton").click();
        }
    });

</script>





</html>
