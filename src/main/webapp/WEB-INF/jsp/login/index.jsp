<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2018/7/18
  Time: 9:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>易航oTMS-航运 先人一步</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link href="${pageContext.request.contextPath}/assets/index/resources/css/jquery-ui-themes.css" type="text/css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/assets/index/resources/css/axure_rp_page.css" type="text/css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/assets/index/data/styles.css" type="text/css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/assets/index/files/index/styles.css" type="text/css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/assets/index/files/index/lrtk.css" type="text/css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/assets/index/resources/scripts/jquery-1.7.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/index/resources/scripts/jquery-ui-1.8.10.custom.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/index/resources/scripts/prototypePre.js"></script>
    <script src="${pageContext.request.contextPath}/assets/index/data/document.js"></script>
    <script src="${pageContext.request.contextPath}/assets/index/resources/scripts/prototypePost.js"></script>
    <script src="${pageContext.request.contextPath}/assets/index/files/index/data.js"></script>
    <script src="${pageContext.request.contextPath}/assets/index/files/index/lrtk.js"></script>
</head>
<body>
<div id="base" class="">


    <div id="u0" class="ax_default _图片">
        <img id="u0_img" class="img " src="assets/index/images/index/u0.png"/>
    </div>

>
    <div id="u1" class="ax_default label">
        <div id="u1_div" class=""></div>
        <div id="u1_text" class="text ">
            <p><span>关于我们</span></p>
        </div>
    </div>


    <div id="u2" class="ax_default label">
        <div id="u2_div" class=""></div>
        <div id="u2_text" class="text ">
            <p><span>功能说明</span></p>
        </div>
    </div>


    <div id="u3" class="ax_default label">
        <div id="u3_div" class=""></div>
        <div id="u3_text" class="text ">
            <p><span>产品</span></p>
        </div>
    </div>


    <div id="u4" class="ax_default flow_shape" onclick="openLogin()">
        <div id="u4_div" class=""></div>
        <div id="u4_text" class="text ">
            <p>
                <%--<span>登录</span>--%>
                <span>管理控制台</span>
            </p>
        </div>
    </div>


    <div id="u5" class="ax_default label">
        <div id="u5_div" class=""></div>
        <div id="u5_text" class="text ">
            <p><span><b>航运，先人一步</b></span></p>
        </div>
    </div>


    <div id="u6" class="ax_default label">
        <div id="u6_div" class=""></div>
        <div id="u6_text" class="text ">
            <p><span>物联网+长江航运物流管理系统</span></p>
        </div>
    </div>


    <div id="u7" class="ax_default flow_shape">
        <div id="u7_div" class=""></div>
        <div id="u7_text" class="text ">
            <p><span>咨询客服</span></p>
        </div>
    </div>


    <div id="u8" class="ax_default box_1">
        <div id="u8_div" class=""></div>
    </div>


    <div id="u9" class="ax_default box_1">
        <div id="u9_div" class=""></div>
    </div>


    <div id="u10" class="ax_default box_1">
        <div id="u10_div" class=""></div>
    </div>


    <div id="u11" class="ax_default box_1">
        <div id="u11_div" class=""></div>
    </div>


    <div id="u12" class="ax_default _图片">
        <img id="u12_img" class="img " src="assets/index/images/index/u12.png"/>
    </div>


    <div id="u13" class="ax_default label">
        <div id="u13_div" class=""></div>
        <div id="u13_text" class="text ">
            <p><span>物流可视化</span></p>
        </div>
    </div>


    <div id="u14" class="ax_default label">
        <div id="u14_div" class=""></div>
        <div id="u14_text" class="text ">
            <p><span>在线视频 远程监控</span></p>
        </div>
    </div>


    <div id="u15" class="ax_default _图片">
        <img id="u15_img" class="img " src="assets/index/images/index/u15.png"/>
    </div>


    <div id="u16" class="ax_default label">
        <div id="u16_div" class=""></div>
        <div id="u16_text" class="text ">
            <p><span>云端备份</span></p>
        </div>
    </div>


    <div id="u17" class="ax_default label">
        <div id="u17_div" class=""></div>
        <div id="u17_text" class="text ">
            <p><span>远程调阅 数据安全</span></p>
        </div>
    </div>


    <div id="u18" class="ax_default _图片">
        <img id="u18_img" class="img " src="assets/index/images/index/u18.png"/>
    </div>


    <div id="u19" class="ax_default label">
        <div id="u19_div" class=""></div>
        <div id="u19_text" class="text ">
            <p><span>一对一专属服务</span></p>
        </div>
    </div>


    <div id="u20" class="ax_default label">
        <div id="u20_div" class=""></div>
        <div id="u20_text" class="text ">
            <p><span>专人提供技术支持</span></p>
        </div>
    </div>


    <div id="u21" class="ax_default _图片">
        <img id="u21_img" class="img " src="assets/index/images/index/u21.png"/>
    </div>


    <div id="u22" class="ax_default label">
        <div id="u22_div" class=""></div>
        <div id="u22_text" class="text ">
            <p><span>OEM定制</span></p>
        </div>
    </div>


    <div id="u23" class="ax_default label">
        <div id="u23_div" class=""></div>
        <div id="u23_text" class="text ">
            <p><span>个性化定制解决方案</span></p>
        </div>
    </div>


    <div id="u24" class="ax_default label">
        <div id="u24_div" class=""></div>
        <div id="u24_text" class="text ">
            <p><span>我们的组成</span></p>
        </div>
    </div>


    <div id="u25" class="ax_default label">
        <div id="u25_div" class=""></div>
        <div id="u25_text" class="text ">
            <p><span>易航oTMS—航运在线管理平台</span></p>
        </div>
    </div>

    <!-- Unnamed (水平线) -->
    <div id="u26" class="ax_default line">
        <img id="u26_img" class="img " src="assets/index/images/index/u26.png"/>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u27" class="ax_default _图片">
        <img id="u27_img" class="img " src="assets/index/images/index/u27.png"/>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u28" class="ax_default _图片">
        <img id="u28_img" class="img " src="assets/index/images/index/u28.png"/>
    </div>

    <!-- Unnamed (垂直线) -->
    <div id="u29" class="ax_default line">
        <img id="u29_img" class="img " src="assets/index/images/index/u29.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u30" class="ax_default label">
        <div id="u30_div" class=""></div>
        <div id="u30_text" class="text ">
            <p><span>易航软件服务平台</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u31" class="ax_default label">
        <div id="u31_div" class=""></div>
        <div id="u31_text" class="text ">
            <p><span>船舶数据监测（行舟仪）</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u32" class="ax_default label">
        <div id="u32_div" class=""></div>
        <div id="u32_text" class="text ">
            <p><span>产品功能说明</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u33" class="ax_default label">
        <div id="u33_div" class=""></div>
        <div id="u33_text" class="text ">
            <p><span>易航oTMS—航运在线管理平台</span></p>
        </div>
    </div>

    <!-- Unnamed (水平线) -->
    <div id="u34" class="ax_default line">
        <img id="u34_img" class="img " src="assets/index/images/index/u26.png"/>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u35" class="ax_default _图片">
        <img id="u35_img" class="img " src="assets/index/images/index/u35.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u36" class="ax_default label">
        <div id="u36_div" class=""></div>
        <div id="u36_text" class="text ">
            <p><span>常用运力管理</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u37" class="ax_default label">
        <div id="u37_div" class=""></div>
        <div id="u37_text" class="text ">
            <p><span>船舶基础数据及实时状态地图</span></p><p><span>及列表双模式实时可见。</span></p>
        </div>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u38" class="ax_default _图片">
        <img id="u38_img" class="img " src="assets/index/images/index/u38.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u39" class="ax_default label">
        <div id="u39_div" class=""></div>
        <div id="u39_text" class="text ">
            <p><span>运单管理</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u40" class="ax_default label">
        <div id="u40_div" class=""></div>
        <div id="u40_text" class="text ">
            <p><span>船舶基础数据及实时状态地图</span></p><p><span>及列表双模式实时可见。</span></p>
        </div>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u41" class="ax_default _图片">
        <img id="u41_img" class="img " src="assets/index/images/index/u41.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u42" class="ax_default label">
        <div id="u42_div" class=""></div>
        <div id="u42_text" class="text ">
            <p><span>运输管理</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u43" class="ax_default label">
        <div id="u43_div" class=""></div>
        <div id="u43_text" class="text ">
            <p><span>运输承运船舶批量管理，运输过程</span></p><p><span>实时跟踪（装卸过程，随时查看实</span></p><p><span>时视频，拍照记录）。</span></p>
        </div>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u44" class="ax_default _图片">
        <img id="u44_img" class="img " src="assets/index/images/index/u44.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u45" class="ax_default label">
        <div id="u45_div" class=""></div>
        <div id="u45_text" class="text ">
            <p><span>货物监管</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u46" class="ax_default label">
        <div id="u46_div" class=""></div>
        <div id="u46_text" class="text ">
            <p><span>从装前到卸后，船舱24小时视</span></p><p><span>频监控。</span></p>
        </div>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u47" class="ax_default _图片">
        <img id="u47_img" class="img " src="assets/index/images/index/u47.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u48" class="ax_default label">
        <div id="u48_div" class=""></div>
        <div id="u48_text" class="text ">
            <p><span>经营分析</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u49" class="ax_default label">
        <div id="u49_div" class=""></div>
        <div id="u49_text" class="text ">
            <p><span>多项经营数据，实时汇总，图表显</span></p><p><span>示，经营状况，一目了然。</span></p>
        </div>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u50" class="ax_default _图片">
        <img id="u50_img" class="img " src="assets/index/images/index/u41.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u51" class="ax_default label">
        <div id="u51_div" class=""></div>
        <div id="u51_text" class="text ">
            <p><span>链接货主</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u52" class="ax_default label">
        <div id="u52_div" class=""></div>
        <div id="u52_text" class="text ">
            <p><span>可开放货主端口，运单承运船舶，货主可同步了解实时状态，查看实时视频。</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u53" class="ax_default label">
        <div id="u53_div" class=""></div>
        <div id="u53_text" class="text ">
            <p><span>关于我们</span></p>
        </div>
    </div>

    <!-- Unnamed (水平线) -->
    <div id="u54" class="ax_default line">
        <img id="u54_img" class="img " src="assets/index/images/index/u26.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u55" class="ax_default label">
        <div id="u55_div" class=""></div>
        <div id="u55_text" class="text ">
            <p><span>关于易航</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u56" class="ax_default label">
        <div id="u56_div" class=""></div>
        <div id="u56_text" class="text ">
            <p><span>易航oTMS,是基于SaaS模式的航运在线管理平台，根据货代企业需求进行</span></p><p><span>深度定制化的研发而推出，通过行舟仪监测船舶数据，提供覆盖货代企业所</span></p><p><span>需经营管理服务及数据服务。</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u57" class="ax_default label">
        <div id="u57_div" class=""></div>
        <div id="u57_text" class="text ">
            <p><span>关于彦思</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u58" class="ax_default label">
        <div id="u58_div" class=""></div>
        <div id="u58_text" class="text ">
            <p><span>彦思科技，航运创新型科技企业，深耕行业20余年，将人工智能和船联网技</span></p><p><span>术相融合，推出首个基于SaaS模式的航运在线管理平台—易航oTMS</span></p>
        </div>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u59" class="ax_default _图片">
        <img id="u59_img" class="img " src="assets/index/images/index/u59.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u60" class="ax_default label">
        <div id="u60_div" class=""></div>
        <div id="u60_text" class="text ">
            <p><span>联系我们</span></p>
        </div>
    </div>

    <!-- Unnamed (水平线) -->
    <div id="u61" class="ax_default line">
        <img id="u61_img" class="img " src="assets/index/images/index/u61.png"/>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u62" class="ax_default _图片">
        <img id="u62_img" class="img " src="assets/index/images/index/u62.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u63" class="ax_default label">
        <div id="u63_div" class=""></div>
        <div id="u63_text" class="text ">
            <p><span>官方热线</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u64" class="ax_default label">
        <div id="u64_div" class=""></div>
        <div id="u64_text" class="text ">
            <p><span>400-135-9981</span></p>
        </div>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u65" class="ax_default _图片">
        <img id="u65_img" class="img " src="assets/index/images/index/u65.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u66" class="ax_default label">
        <div id="u66_div" class=""></div>
        <div id="u66_text" class="text ">
            <p><span>联系地址</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u67" class="ax_default label">
        <div id="u67_div" class=""></div>
        <div id="u67_text" class="text ">
            <p><span>安徽省芜湖市高新区天井山路13号东汇大厦3层</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u68" class="ax_default label">
        <div id="u68_div" class=""></div>
        <div id="u68_text" class="text ">
            <p><span>COPYRIGHT © 2015-2018 芜湖市彦思科技有限公司 版权所有&nbsp; 皖ICP备15023113号-2</span></p>
        </div>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u69" class="ax_default _图片">
        <img id="u69_img" class="img " src="assets/index/images/index/u69.png"/>
    </div>

    <!-- Unnamed (图片) -->
    <div id="u70" class="ax_default _图片">
        <img id="u70_img" class="img " src="assets/index/images/index/u70.png"/>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u71" class="ax_default _文本段落">
        <div id="u71_div" class=""></div>
        <div id="u71_text" class="text ">
            <p><span>扫一扫关注</span></p><p><span>易航微信公众号</span></p>
        </div>
    </div>

    <!-- Unnamed (矩形) -->
    <div id="u72" class="ax_default _文本段落">
        <div id="u72_div" class=""></div>
        <div id="u72_text" class="text ">
            <p><span>扫一扫了解</span></p><p><span>更多易航详解</span></p>
        </div>
    </div>

    <!-- Unnamed (水平线) -->
    <div id="u73" class="ax_default line">
        <img id="u73_img" class="img " src="assets/index/images/index/u73.png"/>
    </div>

    <!-- Unnamed (动态面板) -->
    <div id="u74" class="ax_default">
        <div id="u74_state0" class="panel_state" data-label="State1" style="">
            <div id="u74_state0_content" class="panel_state_content">
            </div>
        </div>
    </div>

    <!-- Unnamed (动态面板) -->
    <div id="u75" class="ax_default">
        <div id="u75_state0" class="panel_state" data-label="State1" style="">
            <div id="u75_state0_content" class="panel_state_content">
            </div>
        </div>
    </div>

    <!-- Unnamed (动态面板) -->
    <div id="u76" class="ax_default">
        <div id="u76_state0" class="panel_state" data-label="State1" style="">
            <div id="u76_state0_content" class="panel_state_content">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/index/files/index/customer.js"></script>


<!-- 客服代码 开始 -->
<a href="#0" class="cd-top">Top</a>
<!-- 客服代码 结束 -->
<script>
    function openLogin() {
        window.open("maps.do");
    }
</script>
</body>
</html>
