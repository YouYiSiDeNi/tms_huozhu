<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/16
  Time: 16:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
    <meta name="applicable-device" content="pc,mobile">
    <title>易航oTMS-登录</title>
    <link rel="shortcut icon" type="image/x-icon" href="../resource/web/images/home/hzc_web.ico" media="screen" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/login.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css">
    <style type="text/css">
        @font-face {
            font-family:'Microsoft YaHei', Arial, sans-serif;
        }
    </style>
</head>
<body>
<div class="modal fade in" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style="display: block; padding-left: 0px;">
    <div style="display:table; width:100%; height:100%;"><div style="vertical-align:middle; display:table-cell;">
        <div class="modal-dialog modal-sm" style="width:540px;">
    <div class="modal-content" style="border: none;">
        <div class="col-left"></div>
        <div class="col-right">
            <div class="modal-header">

                <h4 class="modal-title" id="loginModalLabel" style="font-size: 18px;color: #04b5ff">登录</h4>
            </div>
            <div class="modal-body">
                <section class="box-login v5-input-txt" id="box-login">
                    <form id="login_form" action="" method="post" >
                        <ul>
                            <li class="form-group"><input class="form-control" id="id_account_l" maxlength="50" name="account_l" placeholder="请输入账号" type="text"></li>
                            <li class="form-group"><input class="form-control" id="id_password_l" name="password_l" placeholder="请输入密码" type="password"></li>
                        </ul>

                    </form>
                    <div class="login-box marginB10">
                        <button id="login_btn" type="submit" class="btn btn-micv5 btn-block globalLogin">登录</button>

                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
    </div>
    </div>
</div>

</div>
<div class="copyright" style="position: absolute;bottom: 2%;text-align: center;width: 100%;color: white">copyright©彦思科技产品中心出品</div>
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
</body>
<script type="text/javascript">
//    var count = 0;
    $("#login_btn").click(function() {
        $.ajax({
            type : 'post',
            url : 'login.do?accountname='+$("#id_account_l").val()+'&password='+$("#id_password_l").val(),
            dataType : 'json',
            contentType: "application/json; charset=utf-8",
            success : function(data) {
                if(data==false){
                    alert("该账号密码不正确！");
                }
                else{
                    location.href="welcome.do";

                }
            }
        })
    })

</script>
<%--enter键登录--%>
<script>

    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            $("#login_btn").click();
        }
    });

</script>
</html>
