<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2017/3/5
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<link rel="icon" type="image/png" href="assets/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>充值续时|TMS</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

<!--  Light Bootstrap Table core CSS    -->
<link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>


<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

<!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
<link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />


<!-- layui -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">
<body onload="goPage(1,6);">


<div class="wrapper">
    <%@include file="../../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../../common/top.jsp"%>
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="main-center">
                                <div class="main-wrap">
                                    <!--中间右边内容-->
                                    <div class="main-right">
                                        <div class="main-right-head">
                                            <i class="minicolor" style="width: 3px;height: 10px;background-color: #00AAEE">&nbsp;&nbsp;</i>
                                            <span><a href="toReCharge.do">充值汇总表</a> </span> |  <span  style="color:dodgerblue">观看记录表</span>
                                            <div class="change-pass-right">
                                                <span class="add-tip-box">剩余<b>${resecond}</b>秒</span>
                                                <span class="add-tip-box2" onclick="torecharge()">充值</span>
                                            </div>
                                        </div>
                                        <div class="deposit-out" id="recahrgeTable">
                                            <table cellpadding="0" cellspacing="0" class="deposit-box" width="100%">
                                                <tr class="deposit-title-list">
                                                    <%--<th class="deposit-title">序号</th>--%>
                                                    <th class="deposit-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;船舶名称</th>
                                                    <th class="deposit-title">观看开始日期</th>
                                                    <th class="deposit-title">观看结束日期</th>
                                                    <th class="deposit-title">观看时长</th>
                                                </tr>
                                                <tbody id="adminTbody">
                                                <c:forEach var="item" items="${jiShiEntiys}" varStatus="status">

                                                    <tr class="deposit-main-list">
                                                        <%--<td class="deposit-item">${status.index+1}</td>--%>
                                                        <td class="deposit-item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${item.shipInfo.shipname}</td>
                                                        <td class="deposit-item"><fmt:formatDate type="time" value="${item.begindt}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>
                                                        <td class="deposit-item"><fmt:formatDate type="time" value="${item.enddate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>
                                                        <td class="deposit-item">${item.jsecond}秒</td>
                                                    </tr>

                                                </c:forEach>
                                                </tbody>
                                            </table>
                                            <div id="barcon" class="barcon" >
                                                <div id="barcon1" class="barcon1"></div>
                                                <div id="barcon2" class="barcon2">
                                                    <ul>
                                                        <li><a href="###" id="firstPage">首页</a></li>
                                                        <li><a href="###" id="prePage">上一页</a></li>
                                                        <li><a href="###" id="nextPage">下一页</a></li>
                                                        <li><a href="###" id="lastPage">尾页</a></li>
                                                        <li><select id="jumpWhere">
                                                        </select></li>
                                                        <li><a href="###" id="jumpPage" onclick="jumpPage()">跳转</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div style="height: 100px;text-align: center;display:none;"id="noneData">
                                            <div class="row" ><h3 style="margin-top: 40px"><i class="pe-7s-info" style="color: #01AAED"></i> 暂无充值记录</h3></div>
                                        </div>

                                    </div>
                                    <!--中间右边内容 end-->
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<%@include file="../../common/modal.jsp"%>



<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>
<script>
    /**
     * 分页函数
     * pno--页数
     * psize--每页显示记录数
     * 分页部分是从真实数据行开始，因而存在加减某个常数，以确定真正的记录数
     * 纯js分页实质是数据行全部加载，通过是否显示属性完成分页功能
     **/

    var pageSize=0;//每页显示行数
    var currentPage_=1;//当前页全局变量，用于跳转时判断是否在相同页，在就不跳，否则跳转。
    function goPage(pno,psize){
        var itable = document.getElementById("adminTbody");
        var num = itable.rows.length;//表格所有行数(所有记录数)



        if(num==0){
            document.getElementById( "recahrgeTable").style.display= "none";
            document.getElementById( "noneData").style.display= "block";
        }else {
            document.getElementById( "noneData").style.display= "none";
            document.getElementById( "recahrgeTable").style.display= "block";
        }



        pageSize = psize;//每页显示行数
        //总共分几页
        if(num/pageSize > parseInt(num/pageSize)){
            totalPage=parseInt(num/pageSize)+1;
        }else{
            totalPage=parseInt(num/pageSize);
        }
        var currentPage = pno;//当前页数
        currentPage_=currentPage;
        var startRow = (currentPage - 1) * pageSize+1;
        var endRow = currentPage * pageSize;
        endRow = (endRow > num)? num : endRow;
        //遍历显示数据实现分页
        for(var i=1;i<(num+1);i++){
            var irow = itable.rows[i-1];
            if(i>=startRow && i<=endRow){
                irow.style.display = "";
            }else{
                irow.style.display = "none";
            }
        }
        var tempStr = "共"+num+"条观看记录 分"+totalPage+"页 当前第"+currentPage+"页";
        document.getElementById("barcon1").innerHTML = tempStr;

        if(currentPage>1){
            $("#firstPage").on("click",function(){
                goPage(1,psize);
            }).removeClass("ban");
            $("#prePage").on("click",function(){
                goPage(currentPage-1,psize);
            }).removeClass("ban");
        }else{
            $("#firstPage").off("click").addClass("ban");
            $("#prePage").off("click").addClass("ban");
        }

        if(currentPage<totalPage){
            $("#nextPage").on("click",function(){
                goPage(currentPage+1,psize);
            }).removeClass("ban")
            $("#lastPage").on("click",function(){
                goPage(totalPage,psize);
            }).removeClass("ban")
        }else{
            $("#nextPage").off("click").addClass("ban");
            $("#lastPage").off("click").addClass("ban");
        }
        var tempOption="";
        for(var i=1;i<=totalPage;i++)
        {
            tempOption+='<option value='+i+'>'+i+'</option>'
        }
        $("#jumpWhere").html(tempOption);
        $("#jumpWhere").val(currentPage);
    }


    function jumpPage()
    {
        var num=parseInt($("#jumpWhere").val());
        console.log(pageSize);
        if(num!=currentPage_)
        {
            goPage(num,pageSize);
        }
    }

</script>

<script>
    document.getElementById("active4").className = "active";
    document.getElementById("crumbs").innerHTML="观看记录表";
    $(function(){
        $(".number-name").click(function(){
            $(".alert-shadow").show();
        })
        $(".alert-close").click(function(){
            $(".alert-shadow").hide();
        })
    })
</script>


<div class="alert-shadow">
    <!--点击立即充值后弹窗-->
    <div class="alert-login-box3 alert-view-pay" style="display: none;">
        <div class="alert-close"></div>
        <p class="alert-login-title">请在打开新页面完成付款！</p>
        <p class="alert-king-icon"></p>
        <p class="go-add-table">
            <span class="go-add-money" onclick="window.location.reload();">我已付款</span>
            <span class="go-add-money go-cancel-money"><a href="toReChargePay.do"> 付款遇到问题</a></span>
        </p>
    </div>
</div>
<script>
    document.getElementById("active4").className = "active";
    document.getElementById("crumbs").innerHTML="充值汇总表";
    $(function(){
        $(".number-name").click(function(){
            $(".alert-shadow").show();
        })
        $(".alert-close").click(function(){
            $(".alert-shadow").hide();
        })

    })
    function torecharge() {
        $(".alert-shadow").show();
        $(".alert-view-pay").show();
        window.open("toReChargePay.do");
    }
    $(".go-cancel-money").click(function(){
        $(".alert-shadow").hide();
    })
    function formatDate(date){
        date = new Date(date);
        var y=date.getFullYear();
        var m=date.getMonth()+1;
        var d=date.getDate();
        var h=date.getHours();
        var m1=date.getMinutes();
        var s=date.getSeconds();
        m = m<10?("0"+m):m;
        d = d<10?("0"+d):d;
//        return y+"-"+m+"-"+d+" "+h+":"+m1+":"+s;
        return y+"-"+m+"-"+d;
    }
</script>

</body>
</html>

