<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2017/3/5
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<link rel="icon" type="image/png" href="assets/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>支付成功！TMS</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

<!--  Light Bootstrap Table core CSS    -->
<link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>


<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

<!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
<link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />


<!-- layui -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">
<body>


<div class="wrapper">
    <%@include file="../../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../../common/top.jsp"%>
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="main-center">
                                <div class="main-wrap deposit-wrap">
                                    <!--中间左边菜单列表-->
                                    <div class="go-deposit">
                                        <div class="go-deposit-box">
                                            <h5 class="go-deposit-title2">支付宝支付结果</h5>
                                            <div class="go-deposit-submit2">
                                                <img src="../resource/web/images/home/alipaySucess.png">
                                            </div>
                                        </div>
                                        <div class="go-deposit-box">
                                            <p class="explan-head">客服及其他付款方式</p>
                                            <p class="explan-cont">
                                                客服联系电话：400-992-2061<br/>
                                                开户行名称：芜湖市彦思科技有限公司<br />
                                                开户银行：徽商银行股份有限公司芜湖三山支行<br />
                                                开户号：1101 7010 2100 0438 761</p>
                                        </div>
                                    </div>
                                    <!--中间左边菜单列表 end-->
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>

<!-- layui -->

<script type="text/javascript">
    document.getElementById("active4").className = "active";
    document.getElementById("crumbs").innerHTML="支付成功！";
    //计时
    function closewin() {
        self.opener=null;
         self.close();
     }
    function clock()
    {
        i=i-1
        if(i>0)setTimeout("clock();",1000);
        else closewin();
    }
    var i=3
    clock();

</script>

</body>
</html>

