<%--
  Created by IntelliJ IDEA.
  User: haojianlei
  Date: 2017/3/5
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<link rel="icon" type="image/png" href="assets/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>充值续时|TMS</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="${pageContext.request.contextPath}/assets/css/animate.min.css" rel="stylesheet"/>

<!--  Light Bootstrap Table core CSS    -->
<link href="${pageContext.request.contextPath}/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css" type="text/css"/>


<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="${pageContext.request.contextPath}/assets/css/demo.css" rel="stylesheet" />

<!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>-->
<link href="${pageContext.request.contextPath}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />


<!-- layui -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">
<body>


<div class="wrapper">
    <%@include file="../../common/left.jsp"%>

    <div class="main-panel">

        <%@include file="../../common/top.jsp"%>
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="main-center">
                                <div class="main-wrap deposit-wrap">
                                    <!--中间左边菜单列表-->
                                    <div class="go-deposit">

                                        <div class="go-deposit-box2">
                                            <h5 class="go-deposit-title">①充值金额</h5>
                                            <div class="number-inout">
                                                <input type="radio" name="payfee" id="pay_fee1" checked="checked" style="display:none"/><label class="pay-fee pay_fee1 pay-fee-click"><i class="check-active2"></i></label>
                                                <input type="radio" name="payfee" id="pay_fee5" style="display:none"/><label class="pay-fee pay_fee5"><i class="check-active2"></i></label>
                                                <input type="radio" name="payfee" id="pay_fee20" style="display:none"/><label class="pay-fee pay_fee20"><i class="check-active2"></i></label>
                                                <input type="radio" name="payfee" id="pay_fee30"  style="display:none"/><label class="pay-fee pay_fee30"><i class="check-active2"></i></label>

                                            </div>

                                            <div class="number-inout">
                                                <input type="radio" name="payfee" id="pay_fee50"  style="display:none"/><label class="pay-fee pay_fee50"><i class="check-active2"></i></label>
                                                <input type="radio" name="payfee" id="pay_fee100"  style="display:none"/><label class="pay-fee pay_fee100"><i class="check-active2"></i></label>
                                            </div>

                                            <div class="number-inout">
                                                <span class="number-inout-box">
                                                    <input class="dethr-inp" type="text" value="1" placeholder="自定义"><p style="margin-top: 8px;margin-left: 5px">元（1 元 = 观看视频5分钟）</p>
                                                </span>
                                            </div>

                                        </div>
                                        <div class="go-deposit-box">
                                            <h5 class="go-deposit-title">②选择支付方式</h5>
                                            <p class="go-deposit-number">

                                                <input type="radio" id="wxpay" name="payment"  checked="checked" style="display:none"/><label class="pay-left wechat-pay pay-left-click" for="wxpay"><i class="check-active"></i></label>
                                                <%--<input type="radio" id="alipay" name="payment" style="display:none"/><label class="pay-left geld-pay pay-left-click" for="alipay"><i class="check-active"></i></label>--%>

                                            </p>
                                            <div class="go-deposit-submit">
                                                <div class="go-deposit-btn" type="button" id="payBtn" value="提交" onclick="topay()"/>提交订单</div>
                                        </div>
                                    </div>
                                    <%--<div class="go-deposit-box">--%>
                                        <%--<p class="explan-head">客服及其他付款方式</p>--%>
                                        <%--<p class="explan-cont">--%>
                                            <%--客服联系电话：400-992-2061<br/>--%>
                                            <%--开户行名称：芜湖市彦思科技有限公司<br />--%>
                                            <%--开户银行：徽商银行股份有限公司芜湖三山支行<br />--%>
                                            <%--开户号：1101 7010 2100 0438 761</p>--%>
                                    <%--</div>--%>
                                </div>
                                <!--中间左边菜单列表 end-->
                                <!--中间右边内容-->
                                <!--中间右边内容 end-->
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>




<%@include file="../../common/modal.jsp"%>

<!--   Core JS Files   -->
<script src="${pageContext.request.contextPath}/js/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<!--  Checkbox, Radio & Switch Plugins -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-checkbox-radio-switch.js"></script>
<!--  Charts Plugin -->
<script src="${pageContext.request.contextPath}/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${pageContext.request.contextPath}/assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${pageContext.request.contextPath}/assets/js/demo.js"></script>

<!-- layui -->

<script type="text/javascript">
    document.getElementById("active4").className = "active";
    document.getElementById("crumbs").innerHTML="支付详情";
    $(function(){
        $(".pay-left").click(function(){
            $(".pay-left").removeClass("pay-left-click");
            $(this).addClass("pay-left-click");
        })

        $(".pay_fee1").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('1');
        })

        $(".pay_fee5").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('5');
        })
        $(".pay_fee20").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('20');
        })
        $(".pay_fee30").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('30');
        })
        $(".pay_fee50").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('50');
        })
        $(".pay_fee100").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
            $(this).addClass("pay-fee-click");
            $(".dethr-inp").val('100');
        })



        $(".dethr-inp").click(function(){
            $(".pay-fee").removeClass("pay-fee-click");
        })

        //	限定输入必须为数字
        $(".dethr-inp").keypress(function(b) {
            var keyCode = b.keyCode ? b.keyCode : b.charCode;
            if (keyCode != 0 && (keyCode < 48 || keyCode > 57) && keyCode != 8 && keyCode != 37 && keyCode != 39)
            {return false;
            }
            else {
                return true;
            }
        }).keyup(function(e) {
            var keyCode = e.keyCode ? e.keyCode : e.charCode;
            console.log(keyCode);
            if (keyCode != 8) {
                var numVal = parseInt($(".dethr-inp").val()) || 0;
                numVal = numVal < 1 ? 1 : numVal;
                $(".dethr-inp").val(numVal);
            }
        }).blur(function() {
            var numVal = parseInt($(".dethr-inp").val()) || 0;
            numVal = numVal < 1 ? 1 : numVal;
            $(".dethr-inp").val(numVal);
        });

    })


    //支付提交操作
    function topay() {
        var radios = document.getElementsByName("payment");
        var num = $(".dethr-inp").val();
        document.location.href = "towxpay.do?minute=" + num;

//        if (alipay.checked==true) {
//            document.location.href = "toaliPay.do?minute=" + num;
//        }else {
//            document.location.href = "towxpay.do?minute=" + num;
//        }
    }

</script>

</body>
</html>

