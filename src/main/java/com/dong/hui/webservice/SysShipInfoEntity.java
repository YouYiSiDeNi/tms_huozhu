
package com.dong.hui.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>sysShipInfoEntity complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="sysShipInfoEntity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="adddt" type="{http://webservice.hui.dong.com/}timestamp" minOccurs="0"/>
 *         &lt;element name="cblength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cbwidth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ckdepth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cklength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ckwidth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="curretlocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="devicenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="devicenumbertwo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hatchesreqest" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="host_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isbad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ischecked" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="isdangerous" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="isdel" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="isempty" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="isemptytwo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="isverified" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="loaddraft" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="remark" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="remarktwo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rollanglel" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="sailingarea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sailingareaname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ship_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ship_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipimage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipimage1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipimage2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipimage3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipimage4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipspeed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shiptype" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tonnage" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="updatedt" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="windspeed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sysShipInfoEntity", propOrder = {
    "adddt",
    "cblength",
    "cbwidth",
    "ckdepth",
    "cklength",
    "ckwidth",
    "curretlocation",
    "devicenumber",
    "devicenumbertwo",
    "hatchesreqest",
    "hostId",
    "isbad",
    "ischecked",
    "isdangerous",
    "isdel",
    "isempty",
    "isemptytwo",
    "isverified",
    "latitude",
    "loaddraft",
    "longitude",
    "remark",
    "remarktwo",
    "rollanglel",
    "sailingarea",
    "sailingareaname",
    "shipId",
    "shipName",
    "shipimage",
    "shipimage1",
    "shipimage2",
    "shipimage3",
    "shipimage4",
    "shipspeed",
    "shiptype",
    "tonnage",
    "updatedt",
    "windspeed"
})
public class SysShipInfoEntity {

    protected Timestamp adddt;
    protected String cblength;
    protected String cbwidth;
    protected String ckdepth;
    protected String cklength;
    protected String ckwidth;
    protected String curretlocation;
    protected String devicenumber;
    protected String devicenumbertwo;
    protected int hatchesreqest;
    @XmlElement(name = "host_id")
    protected String hostId;
    protected int isbad;
    protected int ischecked;
    protected int isdangerous;
    protected int isdel;
    protected int isempty;
    protected int isemptytwo;
    protected int isverified;
    protected double latitude;
    protected String loaddraft;
    protected double longitude;
    protected int remark;
    protected int remarktwo;
    protected double rollanglel;
    protected String sailingarea;
    protected String sailingareaname;
    @XmlElement(name = "ship_id")
    protected String shipId;
    @XmlElement(name = "ship_name")
    protected String shipName;
    protected String shipimage;
    protected String shipimage1;
    protected String shipimage2;
    protected String shipimage3;
    protected String shipimage4;
    protected String shipspeed;
    protected int shiptype;
    protected double tonnage;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updatedt;
    protected String windspeed;

    /**
     * 获取adddt属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Timestamp }
     *     
     */
    public Timestamp getAdddt() {
        return adddt;
    }

    /**
     * 设置adddt属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Timestamp }
     *     
     */
    public void setAdddt(Timestamp value) {
        this.adddt = value;
    }

    /**
     * 获取cblength属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCblength() {
        return cblength;
    }

    /**
     * 设置cblength属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCblength(String value) {
        this.cblength = value;
    }

    /**
     * 获取cbwidth属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCbwidth() {
        return cbwidth;
    }

    /**
     * 设置cbwidth属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCbwidth(String value) {
        this.cbwidth = value;
    }

    /**
     * 获取ckdepth属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCkdepth() {
        return ckdepth;
    }

    /**
     * 设置ckdepth属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCkdepth(String value) {
        this.ckdepth = value;
    }

    /**
     * 获取cklength属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCklength() {
        return cklength;
    }

    /**
     * 设置cklength属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCklength(String value) {
        this.cklength = value;
    }

    /**
     * 获取ckwidth属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCkwidth() {
        return ckwidth;
    }

    /**
     * 设置ckwidth属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCkwidth(String value) {
        this.ckwidth = value;
    }

    /**
     * 获取curretlocation属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurretlocation() {
        return curretlocation;
    }

    /**
     * 设置curretlocation属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurretlocation(String value) {
        this.curretlocation = value;
    }

    /**
     * 获取devicenumber属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDevicenumber() {
        return devicenumber;
    }

    /**
     * 设置devicenumber属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDevicenumber(String value) {
        this.devicenumber = value;
    }

    /**
     * 获取devicenumbertwo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDevicenumbertwo() {
        return devicenumbertwo;
    }

    /**
     * 设置devicenumbertwo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDevicenumbertwo(String value) {
        this.devicenumbertwo = value;
    }

    /**
     * 获取hatchesreqest属性的值。
     * 
     */
    public int getHatchesreqest() {
        return hatchesreqest;
    }

    /**
     * 设置hatchesreqest属性的值。
     * 
     */
    public void setHatchesreqest(int value) {
        this.hatchesreqest = value;
    }

    /**
     * 获取hostId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostId() {
        return hostId;
    }

    /**
     * 设置hostId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostId(String value) {
        this.hostId = value;
    }

    /**
     * 获取isbad属性的值。
     * 
     */
    public int getIsbad() {
        return isbad;
    }

    /**
     * 设置isbad属性的值。
     * 
     */
    public void setIsbad(int value) {
        this.isbad = value;
    }

    /**
     * 获取ischecked属性的值。
     * 
     */
    public int getIschecked() {
        return ischecked;
    }

    /**
     * 设置ischecked属性的值。
     * 
     */
    public void setIschecked(int value) {
        this.ischecked = value;
    }

    /**
     * 获取isdangerous属性的值。
     * 
     */
    public int getIsdangerous() {
        return isdangerous;
    }

    /**
     * 设置isdangerous属性的值。
     * 
     */
    public void setIsdangerous(int value) {
        this.isdangerous = value;
    }

    /**
     * 获取isdel属性的值。
     * 
     */
    public int getIsdel() {
        return isdel;
    }

    /**
     * 设置isdel属性的值。
     * 
     */
    public void setIsdel(int value) {
        this.isdel = value;
    }

    /**
     * 获取isempty属性的值。
     * 
     */
    public int getIsempty() {
        return isempty;
    }

    /**
     * 设置isempty属性的值。
     * 
     */
    public void setIsempty(int value) {
        this.isempty = value;
    }

    /**
     * 获取isemptytwo属性的值。
     * 
     */
    public int getIsemptytwo() {
        return isemptytwo;
    }

    /**
     * 设置isemptytwo属性的值。
     * 
     */
    public void setIsemptytwo(int value) {
        this.isemptytwo = value;
    }

    /**
     * 获取isverified属性的值。
     * 
     */
    public int getIsverified() {
        return isverified;
    }

    /**
     * 设置isverified属性的值。
     * 
     */
    public void setIsverified(int value) {
        this.isverified = value;
    }

    /**
     * 获取latitude属性的值。
     * 
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * 设置latitude属性的值。
     * 
     */
    public void setLatitude(double value) {
        this.latitude = value;
    }

    /**
     * 获取loaddraft属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoaddraft() {
        return loaddraft;
    }

    /**
     * 设置loaddraft属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoaddraft(String value) {
        this.loaddraft = value;
    }

    /**
     * 获取longitude属性的值。
     * 
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * 设置longitude属性的值。
     * 
     */
    public void setLongitude(double value) {
        this.longitude = value;
    }

    /**
     * 获取remark属性的值。
     * 
     */
    public int getRemark() {
        return remark;
    }

    /**
     * 设置remark属性的值。
     * 
     */
    public void setRemark(int value) {
        this.remark = value;
    }

    /**
     * 获取remarktwo属性的值。
     * 
     */
    public int getRemarktwo() {
        return remarktwo;
    }

    /**
     * 设置remarktwo属性的值。
     * 
     */
    public void setRemarktwo(int value) {
        this.remarktwo = value;
    }

    /**
     * 获取rollanglel属性的值。
     * 
     */
    public double getRollanglel() {
        return rollanglel;
    }

    /**
     * 设置rollanglel属性的值。
     * 
     */
    public void setRollanglel(double value) {
        this.rollanglel = value;
    }

    /**
     * 获取sailingarea属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSailingarea() {
        return sailingarea;
    }

    /**
     * 设置sailingarea属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSailingarea(String value) {
        this.sailingarea = value;
    }

    /**
     * 获取sailingareaname属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSailingareaname() {
        return sailingareaname;
    }

    /**
     * 设置sailingareaname属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSailingareaname(String value) {
        this.sailingareaname = value;
    }

    /**
     * 获取shipId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipId() {
        return shipId;
    }

    /**
     * 设置shipId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipId(String value) {
        this.shipId = value;
    }

    /**
     * 获取shipName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * 设置shipName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipName(String value) {
        this.shipName = value;
    }

    /**
     * 获取shipimage属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipimage() {
        return shipimage;
    }

    /**
     * 设置shipimage属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipimage(String value) {
        this.shipimage = value;
    }

    /**
     * 获取shipimage1属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipimage1() {
        return shipimage1;
    }

    /**
     * 设置shipimage1属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipimage1(String value) {
        this.shipimage1 = value;
    }

    /**
     * 获取shipimage2属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipimage2() {
        return shipimage2;
    }

    /**
     * 设置shipimage2属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipimage2(String value) {
        this.shipimage2 = value;
    }

    /**
     * 获取shipimage3属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipimage3() {
        return shipimage3;
    }

    /**
     * 设置shipimage3属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipimage3(String value) {
        this.shipimage3 = value;
    }

    /**
     * 获取shipimage4属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipimage4() {
        return shipimage4;
    }

    /**
     * 设置shipimage4属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipimage4(String value) {
        this.shipimage4 = value;
    }

    /**
     * 获取shipspeed属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipspeed() {
        return shipspeed;
    }

    /**
     * 设置shipspeed属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipspeed(String value) {
        this.shipspeed = value;
    }

    /**
     * 获取shiptype属性的值。
     * 
     */
    public int getShiptype() {
        return shiptype;
    }

    /**
     * 设置shiptype属性的值。
     * 
     */
    public void setShiptype(int value) {
        this.shiptype = value;
    }

    /**
     * 获取tonnage属性的值。
     * 
     */
    public double getTonnage() {
        return tonnage;
    }

    /**
     * 设置tonnage属性的值。
     * 
     */
    public void setTonnage(double value) {
        this.tonnage = value;
    }

    /**
     * 获取updatedt属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUpdatedt() {
        return updatedt;
    }

    /**
     * 设置updatedt属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUpdatedt(XMLGregorianCalendar value) {
        this.updatedt = value;
    }

    /**
     * 获取windspeed属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWindspeed() {
        return windspeed;
    }

    /**
     * 设置windspeed属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWindspeed(String value) {
        this.windspeed = value;
    }

}
