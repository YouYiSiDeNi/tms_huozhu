
package com.dong.hui.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.dong.hui.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://webservice.hui.dong.com/", "Exception");
    private final static QName _SelectAllShipInfoEntiy_QNAME = new QName("http://webservice.hui.dong.com/", "selectAllShipInfoEntiy");
    private final static QName _SelectAllShipInfoEntiyResponse_QNAME = new QName("http://webservice.hui.dong.com/", "selectAllShipInfoEntiyResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.dong.hui.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SelectAllShipInfoEntiy }
     * 
     */
    public SelectAllShipInfoEntiy createSelectAllShipInfoEntiy() {
        return new SelectAllShipInfoEntiy();
    }

    /**
     * Create an instance of {@link SelectAllShipInfoEntiyResponse }
     * 
     */
    public SelectAllShipInfoEntiyResponse createSelectAllShipInfoEntiyResponse() {
        return new SelectAllShipInfoEntiyResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link SysShipInfoEntity }
     * 
     */
    public SysShipInfoEntity createSysShipInfoEntity() {
        return new SysShipInfoEntity();
    }

    /**
     * Create an instance of {@link Timestamp }
     * 
     */
    public Timestamp createTimestamp() {
        return new Timestamp();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.hui.dong.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectAllShipInfoEntiy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.hui.dong.com/", name = "selectAllShipInfoEntiy")
    public JAXBElement<SelectAllShipInfoEntiy> createSelectAllShipInfoEntiy(SelectAllShipInfoEntiy value) {
        return new JAXBElement<SelectAllShipInfoEntiy>(_SelectAllShipInfoEntiy_QNAME, SelectAllShipInfoEntiy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectAllShipInfoEntiyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.hui.dong.com/", name = "selectAllShipInfoEntiyResponse")
    public JAXBElement<SelectAllShipInfoEntiyResponse> createSelectAllShipInfoEntiyResponse(SelectAllShipInfoEntiyResponse value) {
        return new JAXBElement<SelectAllShipInfoEntiyResponse>(_SelectAllShipInfoEntiyResponse_QNAME, SelectAllShipInfoEntiyResponse.class, null, value);
    }

}
