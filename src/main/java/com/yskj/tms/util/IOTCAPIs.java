package com.yskj.tms.util;

/**
 * Created by Administrator on 2018/6/21.
 */
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

public interface IOTCAPIs extends Library {
    IOTCAPIs IOTCAPIs  = (IOTCAPIs)Native.loadLibrary("IOTCAPIs",IOTCAPIs.class);
    int IOTC_Get_SessionID();
    int IOTC_Initialize2	(char nUDPPort);
    int IOTC_Connect_ByUID_Parallel	(byte[] cszUID,int SID);
    int IOTC_DeInitialize();
}
