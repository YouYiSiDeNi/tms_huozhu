package com.yskj.tms.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by Administrator on 2018/4/20.
 */
public class StringUtil {

    public static String toJson(Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }

    public static boolean isEmpty(String str){
        return str == null || str.trim().length() == 0;
    }
}

