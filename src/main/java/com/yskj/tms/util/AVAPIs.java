package com.yskj.tms.util;

/**
 * Created by Administrator on 2018/6/21.
 */
import com.sun.jna.Library;
import com.sun.jna.Native;

public interface AVAPIs extends Library{
    AVAPIs AVAPIs  = (AVAPIs)Native.loadLibrary("AVAPIs",AVAPIs.class);
     int avInitialize (int nMaxChannelNum);
     int avClientStart(int nIOTCSessionID,String cszViewAccount,String cszViewPassword,int nTimeout,int[] pnServType,int nIOTCChannelID);
     void avClientStop(int nAVChannelID);
     int avSendIOCtrl(int nAVChannelID,int nIOCtrlType,byte[] cabIOCtrlData,	int	nIOCtrlDataSize);
     int avRecvIOCtrl(int nAVChannelID,int[] pnIOCtrlType,byte[] abIOCtrlData,int nIOCtrlMaxDataSize,int nTimeout);
     int avDeInitialize();
}
