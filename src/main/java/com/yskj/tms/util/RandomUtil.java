package com.yskj.tms.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2018/5/9.
 */
public class RandomUtil {
    public static String getOrdernumber(int userid, int goodsid)throws Exception{

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyMMdd");
        String date = simpleDateFormat.format(new Date());
        String ornum ="";
        if (goodsid<10){
            ornum = "00"+goodsid;
        }else if(goodsid<100&&goodsid>9){
            ornum = "0"+goodsid;
        }
        return userid+ornum+date+((int)(Math.random()*9000)+1000)+"";
    }
}
