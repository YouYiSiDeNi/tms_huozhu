package com.yskj.tms.util;

public class Decv {
	private static final String user = "admin";
	private static final char id = (char) 0;
	
	private static void end(int session_ID) {
		AVAPIs.AVAPIs.avClientStop(session_ID);
		AVAPIs.AVAPIs.avDeInitialize();
		IOTCAPIs.IOTCAPIs.IOTC_DeInitialize();
	}
	
	
	
    public static int online(String UID, String password) {
        IOTCAPIs.IOTCAPIs.IOTC_Initialize2(id);
        AVAPIs.AVAPIs.avInitialize(3);
        byte[] bytUID = UID.getBytes();
        int session_ID = IOTCAPIs.IOTCAPIs.IOTC_Get_SessionID();
        int ret = IOTCAPIs.IOTCAPIs.IOTC_Connect_ByUID_Parallel(bytUID, session_ID);

        if (ret < 0) {
            end(session_ID);
            return ret;
        }

        int nTimeout = 1000;
        int[] pnServType = new int[1];
        int nIOTCChannelID = 0;
        int AV_channel_ID = AVAPIs.AVAPIs.avClientStart(session_ID, user, password, nTimeout, pnServType,
                nIOTCChannelID);
        if (AV_channel_ID < 0) {
            end(session_ID);

            return AV_channel_ID;
        }

        end(session_ID);
        return 0;
    }

}
