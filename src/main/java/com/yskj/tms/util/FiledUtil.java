package com.yskj.tms.util;



import com.yskj.tms.entity.ShipInfo;
import com.yskj.tms.mapper.ShipInfoDetailMapper;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Math.PI;

/**
 * Created by Administrator on 2018/4/25.
 */
public class FiledUtil {

    public static String getAdd(String log, String lat ){
        //lat 小  log  大
        //参数解释: 纬度,经度 type 001 (100代表道路，010代表POI，001代表门址，111可以同时显示前三项)
        //String urlString = "http://gc.ditu.aliyun.com/regeocoding?l="+lat+","+log+"&type=010";
        String urlString ="http://api.map.baidu.com/geocoder/v2/?ak=WnAUIrAmQK8oUf0a5tgKxqRfpAA59iCc&pois=1&output=json&location=" + lat + "," + log;

        String cname =null;
        try {
            URL url = new URL(urlString);
            java.net.HttpURLConnection conn = (java.net.HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            String res = "";
            while ((line = in.readLine()) != null) {
                res += line+"\n";
            }
            JSONObject jsonObject = JSONObject.fromObject(res).getJSONObject("result").getJSONObject("addressComponent");
            cname = jsonObject.getString("province")+" "+jsonObject.getString("city")+" "+jsonObject.getString("district");
            in.close();
        } catch (Exception e) {
            System.out.println("error in wapaction,and e is " + e.getMessage());
        }

        return cname;
    }

    public static String getDeviceid(String devicenumber){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("devicenumber",devicenumber);
        String urlString ="http://a.huizhaochuan.cn/DeviceController/getdeviceinfo.xhtml";
        String deviceid="";
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式
            OutputStreamWriter paramout = new OutputStreamWriter(
                    conn.getOutputStream(),"UTF-8");
            paramout.write(jsonObject.toString());
            paramout.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            String res = "";
            while ((line = in.readLine()) != null) {
                res += line;
            }
            JSONObject jsonres = JSONObject.fromObject(res);
            deviceid= jsonres.getString("msg");
            in.close();
        } catch (Exception e) {
            System.out.println("error in wapaction,and e is " + e.getMessage());
        }
        return deviceid;
    }



    public static String gethtml5url(String deviceid){
        JSONObject jsonObject = new JSONObject();
        jsonObject.element("device_serial",deviceid);
        jsonObject.element("chan_no",1);
        jsonObject.element("wrapper",1);
        String urlString ="http://47.100.188.228:18081/openapi/preview/html5";
        String res = "";
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式
            conn.setRequestProperty("Accept", "application/json"); // 设置发送数据的格式
            conn.setRequestProperty("Authorization", "abf0b2be-63bd-11e8-aeb9-00163e1af2e0"); // 设置发送数据的格式
            OutputStreamWriter paramout = new OutputStreamWriter(
                    conn.getOutputStream(),"UTF-8");
            paramout.write(jsonObject.toString());
            paramout.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res += line;
            }
            in.close();
        } catch (Exception e) {
            System.out.println("error in wapaction,and e is " + e.getMessage());
        }
        return res;
    }


    public static String getdevicechnno(String deviceid){
        JSONObject jsonObject = new JSONObject();
        jsonObject.element("device_serial",deviceid);
        String urlString ="http://47.100.188.228:18081/openapi/v2/device/info";
        String res = "";
        String devicenum="";
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式
            conn.setRequestProperty("Accept", "application/json"); // 设置发送数据的格式
            conn.setRequestProperty("Authorization", "abf0b2be-63bd-11e8-aeb9-00163e1af2e0"); // 设置发送数据的格式
            OutputStreamWriter paramout = new OutputStreamWriter(
                    conn.getOutputStream(),"UTF-8");
            paramout.write(jsonObject.toString());
            paramout.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res += line;
            }
            JSONObject jsonDevicenum = JSONObject.fromObject(res);
            JSONArray jsonArray = jsonDevicenum.getJSONArray("chan_list");
            for (int i =0;i<jsonArray.size();i++){
                devicenum = JSONObject.fromObject(jsonArray.get(i).toString()).getString("dpid");
            }
            in.close();
        } catch (Exception e) {
            System.out.println("error in wapaction,and e is " + e.getMessage());
        }
        return devicenum;
    }








    public  static Map parseXmlToList2(String xml) {
        HashMap retMap = new HashMap();

        try {
            StringReader read = new StringReader(xml);
            InputSource source = new InputSource(read);
            SAXBuilder sb = new SAXBuilder();
            Document doc = sb.build(source);
            Element root = doc.getRootElement();
            List<Element> es = root.getChildren();
            if(es != null && es.size() != 0) {
                Iterator var8 = es.iterator();

                while(var8.hasNext()) {
                    Element element = (Element)var8.next();
                    retMap.put(element.getName(), element.getValue());
                }
            }
        } catch (Exception var10) {
            var10.printStackTrace();
        }

        return retMap;
    }


    private static final Double PI = Math.PI;
    private static final Double PK = 180 / PI;
    public static double getDistanceFromTwoPoints(double lat_a, double lng_a, double lat_b, double lng_b) {
        double t1 = Math.cos(lat_a / PK) * Math.cos(lng_a / PK) * Math.cos(lat_b / PK) * Math.cos(lng_b / PK);
        double t2 = Math.cos(lat_a / PK) * Math.sin(lng_a / PK) * Math.cos(lat_b / PK) * Math.sin(lng_b / PK);
        double t3 = Math.sin(lat_a / PK) * Math.sin(lat_b / PK);
        double tt = Math.acos(t1 + t2 + t3);
        return 6366000 * tt;

    }

}
