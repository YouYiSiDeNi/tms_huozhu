package com.yskj.tms.wxconfig;

public class WxPayConfig {
	/**
	 * 基本配置--AppID(应用ID)
	 */
	public static String appid = "wx58519e3ec84a9952";
	
	/**
	 * 基本配置--AppSecret(应用密钥)
	 */
	public static String appsecret = "e93747b9259a61c7c3b12ae9ca235da6";
	
	/**
	 * 商户号（mch_id）
	 */
	public static String partner ="1505464211";
	
	/**
	 * 商户32位密钥
	 */
	public static String partnerkey = "gDpfgDncjURfjBPBLKcxHeZntVtlzwUR";
	
	/**
	 * 交易类型
	 */
	public static String trade_type = "JSAPI";
	
	public static String signType = "MD5";
	
	
	/**
	 * 域名加项目名（Auth/），如果没有项目名去掉（Auth/）
	 */
	public static String url = "http://tms.huizhaochuan.com.cn/";
//	public static String url = "http://localhost:8080/huodai/";
	
	/**
	 * 支付成功后的回调地址
	 */
	public static String callback = WxPayConfig.url + "notifyUrl.do";
	
	
	public static String INPUT_CHARSET = "UTF-8";
	
	
	public static String PREPAY_ID_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	
}
