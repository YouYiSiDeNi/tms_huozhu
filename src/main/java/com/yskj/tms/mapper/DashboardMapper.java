package com.yskj.tms.mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by 御天川 on 2018/5/26.
 */
public interface DashboardMapper {
    List<Map> selectGoodsTypeSumton(Map map);

    List<Map> selectShipSumton(Map map);

    List<Map> selectTonChange(Map map);


}
