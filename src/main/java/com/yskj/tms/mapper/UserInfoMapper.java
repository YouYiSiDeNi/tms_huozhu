package com.yskj.tms.mapper;

import com.yskj.tms.entity.ShipInfo;
import com.yskj.tms.entity.UserInfo;

import java.util.List;

/**
 * Created by Administrator on 2018/4/18.
 */
public interface UserInfoMapper {
    UserInfo findAdminAndPwd(String adminnumber,String password);

    void updatePassword(String pwd, String username);

    String selectPasswordByUsername(String username);

    UserInfo selectUserinfoByUsername(String username);

    void resetPassword(String username);
}
