package com.yskj.tms.mapper;

import com.yskj.tms.entity.*;


import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/11.
 */
public interface ShipInfoDetailMapper {
    ShipInfo selectShipInfoDetail(int shipid) ;

    List<ShipInfo> selectShipInfoByUserid(int userid);

    List<ShipInfo> selectShipInfosAndDviecInfos();

    void addTrackhis(List<Trackhis> trackhisList);

    void deleteTrackhis(Date time);

    List<Map<String,Object>> drawTrackhis(Map map);

    ShipInfo selectShipInfosByShipBid(String shipbid);

    DeviceInfo selectDeviceInfoByShipbid(String shipId);

    void addShipInfos(ShipInfo shipInfo);

    void updateShipInfos(ShipInfo shipInfo);

    void addDeviceInfo(DeviceInfo deviceInfo);

    void updateDeviceinfo(DeviceInfo deviceInfo);

    int selectShipInfosManagerCount(Map map);

   List<ShipInfo> selectShipInfosManagerList(Map map);

    void updateShipownerPhoneByShipid(int shipid ,String shipownerphone);

    void updateIsonlineByShipid(Map map);

    JiShiDetail selectJiShiDetailBuShipidAndUserid(int shipid, int userid);

    void saveJishiDetail(JiShiDetail jiShiDetail);

    void saveJishientiy(JiShiEntiy jiShiEntiy);

    void updateJishiDetail(JiShiDetail jiShiDetail);

    List<JiShiEntiy> selectJishientiyByUserid(int userid);

    void updateShipownerNameByShipid(int shipid, String shipownername);

    DeviceInfo selectDeviceInfoByShipid(int shipid);

//    List<ShipInfo> selectShipInfosManagerList(Map map, int page, int size);
}
