package com.yskj.tms.mapper;

import com.yskj.tms.entity.ReCharge;
import com.yskj.tms.entity.ReChargeDetail;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/23.
 */
public interface ReChargeMapper {
    void addReCharge(ReCharge reCharge);

    ReCharge selectReChargeByOrdernumber(Object[] objects);

    void updateReChargeStatusByOrdernumber(Map map
    );

    ReCharge selectReChargeByReordernumber(String reordernumber);

    void updateReChargeRestatus(Map map);

    ReChargeDetail selectReChargeDetailByUserid(int userid);

    void saveReChargeDetail(ReChargeDetail reChargeDetail1);

    void updateReChargeDetail(ReChargeDetail reChargeDetail);

    List<ReCharge> selectReChargeByRestatusAndUserid(Map map);

    void updateReChargeDetailByUserid(Map map);
}
