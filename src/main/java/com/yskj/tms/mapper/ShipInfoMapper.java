package com.yskj.tms.mapper;

import com.yskj.tms.entity.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/19.
 */
public interface ShipInfoMapper {
    List<ShipInfo> selectShipInfoListByQuery(int userid,int ustats);

    List<ShipInfo> selectShipAndDeviceByQuery(Map map);

    List<ShipInfo> selectShipInfoList(int userid,int page,int size,int ustats);

    int selectShipInfoCount(int userid,int ustats);

    List<ShipInfo> selectshipInfoAndName(Map map);

    List<ShipInfo> searchByShipidList(Integer[] shipidList);

    void updateByShipidList(Map map);

    void updateShipByShipid(Map map);

    GoodsInfo selectGoodsInfo(String goodsownername, String goodsphone);

    void addGoodsInfo(GoodsInfo goodsInfo);

    void addTransportPlan(TransportPlan transportPlan);

    void addShipAndTranport(List<ShipAndTransport> stList);

    void updateShipInfoByList(List<ShipInfo> shipInfoList);

    int selectShipinfoByIsemityCount(int userid, int isemtiy,int isonline,int ustats);

    void updateTransportPlan(TransportPlan transportPlan);

    void deleteShipAndTranport(Map map);

    List<ShipInfo> selectShipListIsemtiy(int userid, int page, int size, int isemtiy,int isonline,int ustats);

    List<ShipInfo> searchShipInfoNotByUserid(Map map);

    int selectShipInfoByIsonlneCount(int userid, int isonline,int ustats);

    List<ShipInfo> selectShipListIsonline(int userid, int page, int size, int isonline,int ustats);


    List<Integer> selectShipAndTranportByShipid(int tid);

    void addUserAndShipInfo(UserAndShip userAndShip);

    List<ShipInfo> searchUserAndShipsinfos(Map map);

    List<UserAndShip> selectUserinfoAndShipinfo(Map maps);

    void updateUserAndShipByUstats(Map map);

    List<ShipInfo> selectShipinfoAndDeviceByIsonlineOrIsemtiy(Map map);
}
