package com.yskj.tms.mapper;

import com.yskj.tms.entity.ShipAndTransport;
import com.yskj.tms.entity.ShipInfo;
import com.yskj.tms.entity.TransportPlan;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/26.
 */
public interface TransportMapper {
    List<TransportPlan> selectByUseridToTransport(Map map);

    TransportPlan selectShipInfosByTid(int tid);
    ShipAndTransport selectShipAndTransport(int tid, int shipid);

    void updateShipAndTransport(Map map);

    List<Map<String,Object>> exportShipInfoAndTransportList(Map map);

    void updateTransportByStats(int tid,int stats);

    List<Map<String,Object>> selectGoodsTypePouplationCount(Date creationdate);

    int selectTransportOrdernumber(String ordernumber);


//    TransportPlan selectByTidToShipInfo(int tid);
}
