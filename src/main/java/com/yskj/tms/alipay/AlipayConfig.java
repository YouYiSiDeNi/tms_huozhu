package com.yskj.tms.alipay;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Administrator on 2018/5/21.
 */
public class AlipayConfig {
    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2018051860101857";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCBz9dLbtz7NMia1+f/Lk9cg+e5iVoVNKzYp9K7vIzVBUU0hWbgwO1e0WWW+AqzJ5HEYvp/fLdkm3PjKMZI41pwu2VCfQUvjg2XrDIeDQOPjgIc1ClfRnwaOaudX2GCNIqnKTCCy8gFHrULYdN/XtLNiF6dVKAe3Axo3pJZdY2CZTsEUENln16RBbgrPgR7FEpSSXXYqcJfyKiVb3Bz4i4XDtCnRazLW6K+7hxugQetYnIK4G52YAnJr0uMqXWxtgHSBAgMICfgLJPqgkYUee+pLwgs3fdyHGdD/x5HamyU1O5h3adZn2I5Td6GEDZNgegBLrtlTrp+7y1CfU9rJFPpAgMBAAECggEARp0K3mvGJZhG74Q3HSbR4N+X8+N1b0eiJpDUH2+npFEjnjhbCDOqbAO4IXF4UJBbyNqWrTbog//UAGOzyP5zuhMWOB7N7Vp2pmTzNkmaAEMdfxUiFEPBSnrIkejKoAt4yQms69lySuazUSe75vNr9IYUAb2BBIhq77jzH1rEyYL+/v6XeWRnaK0coSZFDRZf/N9PVWy/kVzQJokEsPNYP470Los5vEt3XIOV5jgIaSRLmQpqpuJHe9H6VnJWpQWIEJQUkgtwzycz0ut4JNH9L/k4HbUySkGxXCIIXcCdeOaCxzTJDXD5l2tp7AIu+32BpVLaWpJfNNvNz1MfyLAMoQKBgQDqbNyx7JTZiYqlOnLmZRVs4D4DPg1Triag1F+L7bydKmxo2mi7p2WkS0eGF4XQR1WsogYzIYgPJ5DwugvJdnD0lNFl1l25h4X53zRZrjWh1cQoLhYk0ec+0XycMeMMaD7LuIUL+gIj4zylQHAt4ZmmHy7mMDl1VxEzGdSzL9EfpQKBgQCNwkCx+UsVs+fIqMMlL289ISnvuBhpP/wgLImsIA/8apRGeFGv+/Rb8pLv8dwpW9G1uzBY/mDuVryxa/AsfMmIMKiqWi5arj9RksjpIyN5Bs0frvKttJZVQe2SePaVxqzFoKGxSb2t9CQV5yNDrkxv/iUeV4+Iv4VfxnISA77v9QKBgQDPkbbUgEPIXqX56UgFwul2rxOZsV8BgklHzOQRNV711rOJICLuTamNEO7yx28/3Xn50rkkmwMrUABhdoqrRwl7Ny42OtpgRaSWfFGROtTYSDxYy33l+koEcF8iunVs7fz0S5za3vL4LePr+qOZrJFs612OipLF2GMtWSjhxGwp1QKBgCFuANG3p2RL08o3OiXnxMTWsEwOzvrrEusHdcP4n6x3+dWQhDsLILoaP1qEX9YFCDRiG2mxO2yZWdBBUxc6qlAKoYviznGmC9qR0f1gwvnEh9UD7cjXAGjErmDPU99IRfApDFiBulBudaAQrPlF/lKezWQVZAzslWhbpQToizuNAoGBAOmoyWoa2BQnaes/uvp+aQBYfHqRXue6xRIpwe7LzhKmZRmGyFnJ9WkSsNhA+G2sk7Qb7KydfZKz8f1Y1bw0TzCOGpN9BukQ2xHA3Zxly2H0WRnZUJfWEGmJljhDyVJXYnkxlllync3CfSLhwDobq9BYgVIDaUa6RkveafF0XoeF";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnEehQZPzjCxWbRp3eVVBBqtjCkS+vd4XiqkODXvSIcIuDPuJkEhDuymRUa0XPYLWSZMdGEpMDuKT41n3bwfZRsooAcMIQbNWOBGRzfShk8Qt/F8QvxiU0JHPmSr0byBBZL8rKtLNB76OOb/JRz9yKKYgt39dFUG3D2eByMs0xIhWplY3HFraj5oHdOEWbdnt/lAiayp8wJIeAjdPK0MaLUGuBa3JxYaLCmiXvzyiLiTlJtqifW7G7m2xA9V2mJeKpnvQx74NLq4b2X8HlWW3azT3QLVOvJeel94PTc2VQlmVBMtVem3TmRO8B30gL9ZH8zf1HL/uQlJiD+GKRAR4AwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://tms.huizhaochuan.com.cn/znotify_url.do";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://tms.huizhaochuan.com.cn/return_url.do";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipay.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
