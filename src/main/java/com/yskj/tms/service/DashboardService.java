package com.yskj.tms.service;

import com.yskj.tms.mapper.DashboardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by 御天川 on 2018/5/28.
 */
@Service
public class DashboardService {
    @Autowired
    private DashboardMapper dashboardMapper;

    public List<Map> selectGoodsTypeSumton(Map map) throws Exception{
        return dashboardMapper.selectGoodsTypeSumton(map);
    }

    public List<Map> selectShipSumton(Map map) throws Exception{
        return dashboardMapper.selectShipSumton(map);
    }

    public List<Map> selectTonChange(Map map) throws Exception{
        return dashboardMapper.selectTonChange(map);
    }
}
