package com.yskj.tms.service;

import com.yskj.tms.entity.UserInfo;
import com.yskj.tms.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2018/4/18.
 */
@Service
public class UserInfoService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    public UserInfo findByAdminAndPasswordQuery(String adminnumber,String password) throws Exception{
        return userInfoMapper.findAdminAndPwd(adminnumber ,password);
    }

    public void updatePassword(String pwd, String username) {
        userInfoMapper.updatePassword(pwd,username);
    }

    public String selectPasswordByUsername(String username) {
        return userInfoMapper.selectPasswordByUsername(username);
    }

    public  UserInfo selectUserinfoByUsername(String username) {
        return userInfoMapper.selectUserinfoByUsername(username);
    }

}
