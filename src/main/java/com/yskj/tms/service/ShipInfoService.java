package com.yskj.tms.service;

import com.yskj.tms.entity.*;
import com.yskj.tms.mapper.ShipInfoMapper;
import com.yskj.tms.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/19.
 */
@Service
public class ShipInfoService {
    @Autowired
    private ShipInfoMapper shipInfoMapper;


    public List<ShipInfo> selectShipInfoListByQuery(int userid,int ustats) throws Exception{
//        List<ShipInfo> shipInfoList =shipInfoMapper.selectShipInfoListByQuery(userid);
        return shipInfoMapper.selectShipInfoListByQuery(userid,ustats);
    }
    public List<ShipInfo> selectShipAndDeviceByQuery(Map map) throws Exception{
        return shipInfoMapper.selectShipAndDeviceByQuery(map);
    }

    public List<ShipInfo> selectShipList(int userid,int page,int size,int ustats) throws Exception{
        if(page<=0){
            page =1;
        }
        if (size<=0){
            size=10;
        }
         page =(page-1)*size;
        return shipInfoMapper.selectShipInfoList(userid,page,size,ustats);
    }
    public int selectShipinfoCount(int userid,int ustats){
        return  shipInfoMapper.selectShipInfoCount(userid,ustats);
    }

    public List<ShipInfo> selectShipInfoListAndName(Map map) throws Exception{
        return shipInfoMapper.selectshipInfoAndName(map);
    }

    public List<ShipInfo> searchByShipidList(Integer[] shipidList) {
        return shipInfoMapper.searchByShipidList(shipidList);
    }

    public void updateByShipidList(Map map) {
        shipInfoMapper.updateByShipidList(map);
    }

    public void updateShipByQuery(Map map) {
        shipInfoMapper.updateShipByShipid(map);
    }

    public GoodsInfo selectGoodsInfoByQuery(String goodsownername, String goodsphone) {
        return shipInfoMapper.selectGoodsInfo(goodsownername,goodsphone);
    }

    public void addGoodsInfoByQuery(GoodsInfo goodsInfo) {
        shipInfoMapper.addGoodsInfo(goodsInfo);
    }

    public void addTransportPlanByQuery(TransportPlan transportPlan) {
        shipInfoMapper.addTransportPlan(transportPlan);
    }

    public void addShipAndTranportByQuery(List<ShipAndTransport> stList) {
        shipInfoMapper.addShipAndTranport(stList);
    }

    public void updateShipInfoByList(List<ShipInfo> shipInfoList) {
        shipInfoMapper.updateShipInfoByList(shipInfoList);
    }

    public int selectShipinfoByIsemityCount(int userid, int isemtiy,int isonline,int ustats) {
        return shipInfoMapper.selectShipinfoByIsemityCount(userid,isemtiy,isonline,ustats);
    }

    public void updateTransportPlanByQuery(TransportPlan transportPlan) {
        shipInfoMapper.updateTransportPlan(transportPlan);
    }

    public void deleteShipAndTranportByQuery(Map map) {
        shipInfoMapper.deleteShipAndTranport(map);
    }

    public List<ShipInfo> selectShipListIsemtiy(int userid, int page, int size, int isemtiy,int isonline,int ustats) {
        if(page<=0){
            page =1;
        }
        if (size<=0){
            size=10;
        }
        page =(page-1)*size;

        return shipInfoMapper.selectShipListIsemtiy(userid,page,size,isemtiy,isonline,ustats);
    }

    public List<ShipInfo> searchShipInfoNotByUserid(Map map) {
        return shipInfoMapper.searchShipInfoNotByUserid(map);
    }

    public int selectShipInfoByIsonlneCount(int userid, int isonline,int ustats) {
        return shipInfoMapper.selectShipInfoByIsonlneCount(userid,isonline,ustats);
    }

    public List<ShipInfo> selectShipListIsonline(int userid, int page, int size, int isonline,int ustats) {
        if(page<=0){
            page =1;
        }
        if (size<=0){
            size=10;
        }
        page =(page-1)*size;

        return shipInfoMapper.selectShipListIsonline(userid,page,size,isonline,ustats);
    }

    public List<Integer> selectShipAndTranportByShipid(int tid) {
        return shipInfoMapper.selectShipAndTranportByShipid(tid);
    }

    public void addUserAndShipInfo(UserAndShip userAndShip) {
        shipInfoMapper.addUserAndShipInfo(userAndShip);
    }

    public List<ShipInfo> searchUserAndShipsinfo(Map map) {
        return shipInfoMapper.searchUserAndShipsinfos(map);
    }

    public List<UserAndShip> selectUserinfoAndShipinfo(Map maps) {
        return shipInfoMapper.selectUserinfoAndShipinfo(maps);
    }

    public void updateUserAndShipByUstats(Map map) {
        shipInfoMapper.updateUserAndShipByUstats(map);
    }

    public List<ShipInfo> selectShipinfoAndDeviceByIsonlineOrIsemtiy(Map map) {
        return  shipInfoMapper.selectShipinfoAndDeviceByIsonlineOrIsemtiy(map);
    }
}
