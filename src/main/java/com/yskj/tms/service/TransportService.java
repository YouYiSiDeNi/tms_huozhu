package com.yskj.tms.service;

import com.yskj.tms.entity.ShipAndTransport;
import com.yskj.tms.entity.ShipInfo;
import com.yskj.tms.entity.TransportPlan;
import com.yskj.tms.mapper.TransportMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/26.
 */
@Service
public class TransportService {
    @Autowired
    private TransportMapper transportMapper;

    public List<TransportPlan> selectByUseridToTransportQuery(Map map) throws Exception{
        return  transportMapper.selectByUseridToTransport(map);
    }

    public TransportPlan selectShipInfosByTidQuery(int tid)throws Exception{
        return transportMapper.selectShipInfosByTid(tid);
    }

//    public TransportPlan selectByTidToShipInfoQuery(int tid)throws Exception{
//        return  transportMapper.selectByTidToShipInfo(tid);
//    }

    public ShipAndTransport selectShipAndTransportByQuery(int tid,int shipid) throws Exception{
        return transportMapper.selectShipAndTransport(tid,shipid);
    }

    public void updateShipAndTransportByQuery(Map map) throws Exception{
        transportMapper.updateShipAndTransport(map);
    }

    public List<Map<String,Object>> exportShipInfoAndTransportList(Map map) {
        return transportMapper.exportShipInfoAndTransportList(map);
    }

    public void updateTransportByStatsByQuery(int tid,int stats) throws Exception {
        transportMapper.updateTransportByStats(tid,stats);
    }

    public  List<Map<String,Object>> selectGoodsTypePopulationCount(Date creationdate) throws Exception {
        return transportMapper.selectGoodsTypePouplationCount(creationdate);
    }

    public  int selectTransportOredernumber(String ordernumber) throws Exception {
        return transportMapper.selectTransportOrdernumber(ordernumber);
    }
}
