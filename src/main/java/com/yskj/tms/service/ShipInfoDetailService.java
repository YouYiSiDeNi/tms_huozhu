package com.yskj.tms.service;


import com.dong.hui.webservice.Exception_Exception;
import com.dong.hui.webservice.ShipInfoCXF;
import com.dong.hui.webservice.SysShipInfoEntity;
import com.yskj.tms.entity.*;

import com.yskj.tms.mapper.ShipInfoDetailMapper;
import com.yskj.tms.util.StringUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by Administrator on 2018/5/11.
 */
@Service
public class ShipInfoDetailService {

    @Autowired
    private ShipInfoDetailMapper shipInfoDetailMapper;

    @Autowired
    private ShipInfoCXF porxy;


    public ShipInfo selectShipInfoDetail(int shipid) {
        return  shipInfoDetailMapper.selectShipInfoDetail(shipid);
    }


    public void trackhisByQuery(){
        List<ShipInfo> shipInfoList = shipInfoDetailMapper.selectShipInfosAndDviecInfos();
        List<Trackhis> trackhisList = new ArrayList<Trackhis>();
        System.out.println("记录开始");
        for (int i=0;i<shipInfoList.size();i++){
            Trackhis trackhis = new Trackhis();
            trackhis.setShipid(shipInfoList.get(i).getShipid());
            trackhis.setIsemtiy(shipInfoList.get(i).getIsemtiy());
            trackhis.setLongitude(shipInfoList.get(i).getDeviceInfo().getLongitude());
            trackhis.setLatitude(shipInfoList.get(i).getDeviceInfo().getLatitude());
            trackhis.setAdddate(new Timestamp((new Date()).getTime()));
            trackhisList.add(trackhis);
        }
        shipInfoDetailMapper.addTrackhis(trackhisList);
        System.out.println("记录成功");
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -30);
        shipInfoDetailMapper.deleteTrackhis(calendar.getTime());
    }

    public void trackhisByShipInfos() throws Exception_Exception {
        System.out.println("调用开始");
        List<SysShipInfoEntity> sysShipInfoEntityList = porxy.selectAllShipInfoEntiy();
        for (int i=0;i<sysShipInfoEntityList.size();i++){
            if((sysShipInfoEntityList.get(i).getLatitude() !=0.0 && sysShipInfoEntityList.get(i).getLongitude()!=0.0) && (sysShipInfoEntityList.get(i).getLatitude()!=-1.0 &&sysShipInfoEntityList.get(i).getLongitude()!=-1.0) && (sysShipInfoEntityList.get(i).getLatitude()!=-2.0 &&sysShipInfoEntityList.get(i).getLongitude()!=-2.0)){
                ShipInfo shipInfo = shipInfoDetailMapper.selectShipInfosByShipBid(sysShipInfoEntityList.get(i).getShipId());
                if (shipInfo == null){
                    shipInfo = new ShipInfo();
                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getShipId()) && sysShipInfoEntityList.get(i).getShipId() !=null){
                        shipInfo.setShipbid(sysShipInfoEntityList.get(i).getShipId());
                    }else {
                        shipInfo.setShipbid("111111111111111");
                    }
                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getShipName()) && sysShipInfoEntityList.get(i).getShipName() !=null){
                        shipInfo.setShipname(sysShipInfoEntityList.get(i).getShipName());
                    }else {
                        shipInfo.setShipname("未填写");
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCkdepth()) && sysShipInfoEntityList.get(i).getCkdepth() !=null){
                        shipInfo.setCabinheight(Double.parseDouble(sysShipInfoEntityList.get(i).getCkdepth()));
                    }else{
                        shipInfo.setCabinheight(0.0);
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCklength()) && sysShipInfoEntityList.get(i).getCklength() !=null){
                        shipInfo.setCabinlong(Double.parseDouble(sysShipInfoEntityList.get(i).getCklength()));
                    }else{
                        shipInfo.setCabinlong(0.0);
                    }
                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getLoaddraft()) && sysShipInfoEntityList.get(i).getLoaddraft() !=null){
                        shipInfo.setLoaddraft(Double.parseDouble(sysShipInfoEntityList.get(i).getLoaddraft()));
                    }else{
                        shipInfo.setLoaddraft(0.0);
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCkwidth()) && sysShipInfoEntityList.get(i).getCkwidth() !=null){
                        shipInfo.setCabinwide(Double.parseDouble(sysShipInfoEntityList.get(i).getCkwidth()));
                    }else{
                        shipInfo.setCabinwide(0.0);
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCblength()) && sysShipInfoEntityList.get(i).getCblength() !=null){
                        shipInfo.setShiplong(Double.parseDouble(sysShipInfoEntityList.get(i).getCblength().trim()));
                    }else{
                        shipInfo.setShiplong(0.0);
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCbwidth()) && sysShipInfoEntityList.get(i).getCbwidth() !=null){
                        shipInfo.setShipwide(Double.parseDouble(sysShipInfoEntityList.get(i).getCbwidth().trim()));
                    }else{
                        shipInfo.setShipwide(0.0);
                    }
                    shipInfo.setTon(sysShipInfoEntityList.get(i).getTonnage());

                    shipInfo.setIsemtiy(sysShipInfoEntityList.get(i).getIsempty());

                    shipInfo.setSealingdevice(sysShipInfoEntityList.get(i).getHatchesreqest());

                    shipInfo.setShiptypeid(sysShipInfoEntityList.get(i).getShiptype());
                    shipInfo.setStats(0);
                    shipInfo.setShipownername("暂无");
                    shipInfo.setShipownerphone("数据收集中");
                    shipInfoDetailMapper.addShipInfos(shipInfo);
                    DeviceInfo deviceInfo = new DeviceInfo();
                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getDevicenumber()) && sysShipInfoEntityList.get(i).getDevicenumber() !=null){
                        deviceInfo.setDevicenumber(sysShipInfoEntityList.get(i).getDevicenumber());
                    }else{
                        deviceInfo.setDevicenumber("22222222");
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getDevicenumbertwo()) && sysShipInfoEntityList.get(i).getDevicenumbertwo() !=null){
                        deviceInfo.setDevicenumbertwo(sysShipInfoEntityList.get(i).getDevicenumbertwo());
                    }else{
                        deviceInfo.setDevicenumbertwo("333333333");
                    }

                    deviceInfo.setShipid(shipInfo.getShipid());
                    deviceInfo.setLatitude(String.valueOf(sysShipInfoEntityList.get(i).getLatitude()));
                    deviceInfo.setLongitude(String.valueOf(sysShipInfoEntityList.get(i).getLongitude()));
                    deviceInfo.setRollanglel(sysShipInfoEntityList.get(i).getRollanglel());
                    deviceInfo.setShipspeed(sysShipInfoEntityList.get(i).getShipspeed());
                    deviceInfo.setUptime(new Timestamp((new Date()).getTime()));
                    deviceInfo.setIsonline(0);
                    shipInfoDetailMapper.addDeviceInfo(deviceInfo);
                }else{
                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getShipName()) && sysShipInfoEntityList.get(i).getShipName() !=null){
                        shipInfo.setShipname(sysShipInfoEntityList.get(i).getShipName());
                    }else {
                        shipInfo.setShipname("未填写");
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCkdepth()) && sysShipInfoEntityList.get(i).getCkdepth() !=null){
                        shipInfo.setCabinheight(Double.parseDouble(sysShipInfoEntityList.get(i).getCkdepth()));
                    }else{
                        shipInfo.setCabinheight(0.0);
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCklength()) && sysShipInfoEntityList.get(i).getCklength() !=null){
                        shipInfo.setCabinlong(Double.parseDouble(sysShipInfoEntityList.get(i).getCklength()));
                    }else{
                        shipInfo.setCabinlong(0.0);
                    }
                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getLoaddraft()) && sysShipInfoEntityList.get(i).getLoaddraft() !=null){
                        shipInfo.setLoaddraft(Double.parseDouble(sysShipInfoEntityList.get(i).getLoaddraft()));
                    }else{
                        shipInfo.setLoaddraft(0.0);
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCkwidth()) && sysShipInfoEntityList.get(i).getCkwidth() !=null){
                        shipInfo.setCabinwide(Double.parseDouble(sysShipInfoEntityList.get(i).getCkwidth()));
                    }else{
                        shipInfo.setCabinwide(0.0);
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCblength()) && sysShipInfoEntityList.get(i).getCblength() !=null){
                        shipInfo.setShiplong(Double.parseDouble(sysShipInfoEntityList.get(i).getCblength().trim()));
                    }else{
                        shipInfo.setShiplong(0.0);
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getCbwidth()) && sysShipInfoEntityList.get(i).getCbwidth() !=null){
                        shipInfo.setShipwide(Double.parseDouble(sysShipInfoEntityList.get(i).getCbwidth().trim()));
                    }else{
                        shipInfo.setShipwide(0.0);
                    }
                    shipInfo.setTon(sysShipInfoEntityList.get(i).getTonnage());

                    shipInfo.setIsemtiy(sysShipInfoEntityList.get(i).getIsempty());

                    shipInfo.setSealingdevice(sysShipInfoEntityList.get(i).getHatchesreqest());

                    shipInfo.setShiptypeid(sysShipInfoEntityList.get(i).getShiptype());
                    shipInfoDetailMapper.updateShipInfos(shipInfo);
                    DeviceInfo deviceInfo = new DeviceInfo();
                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getDevicenumber()) && sysShipInfoEntityList.get(i).getDevicenumber() !=null){
                        deviceInfo.setDevicenumber(sysShipInfoEntityList.get(i).getDevicenumber());
                    }else{
                        deviceInfo.setDevicenumber("22222222");
                    }

                    if (!StringUtil.isEmpty(sysShipInfoEntityList.get(i).getDevicenumbertwo()) && sysShipInfoEntityList.get(i).getDevicenumbertwo() !=null){
                        deviceInfo.setDevicenumbertwo(sysShipInfoEntityList.get(i).getDevicenumbertwo());
                    }else{
                        deviceInfo.setDevicenumbertwo("333333333");
                    }
                    deviceInfo.setShipid(shipInfo.getShipid());
                    deviceInfo.setLatitude(String.valueOf(sysShipInfoEntityList.get(i).getLatitude()));
                    deviceInfo.setLongitude(String.valueOf(sysShipInfoEntityList.get(i).getLongitude()));
                    deviceInfo.setRollanglel(sysShipInfoEntityList.get(i).getRollanglel());
                    deviceInfo.setShipspeed(sysShipInfoEntityList.get(i).getShipspeed());
                    deviceInfo.setUptime(new Timestamp((new Date()).getTime()));
                    shipInfoDetailMapper.updateDeviceinfo(deviceInfo);

                }
            }
        }
        System.out.println("调用结束");
    }


    public List<Map<String,Object>> drawTrackhisByQuery(Map map) {
        return  shipInfoDetailMapper.drawTrackhis(map);
    }

    public List<ShipInfo> selectShipInfosManagerList(Map map ,int page,int size) {
        if(page<=0){
            page =1;
        }
        if (size<=0){
            size=10;
        }
        page =(page-1)*size;
        map.put("page",page);
        map.put("size",size);
        return shipInfoDetailMapper.selectShipInfosManagerList(map);

    }

    public int selectShipInfosManagerCount(Map map) {

        return shipInfoDetailMapper.selectShipInfosManagerCount(map);
    }

    public  void updateShipownerPhoneByShipid(int shipid,String shipownerphone) {
        shipInfoDetailMapper.updateShipownerPhoneByShipid(shipid,shipownerphone);
    }

    public void updateIsonlineByShipid(Map map) {
        shipInfoDetailMapper.updateIsonlineByShipid(map);
    }

    public JiShiDetail selectJiShiDetailBuShipidAndUserid(int shipid, int userid) {
        return  shipInfoDetailMapper.selectJiShiDetailBuShipidAndUserid(shipid,userid);
    }

    public void saveJishiDetail(JiShiDetail jiShiDetail) {
        shipInfoDetailMapper.saveJishiDetail(jiShiDetail);
    }

    public void saveJishientiy(JiShiEntiy jiShiEntiy) {
        shipInfoDetailMapper.saveJishientiy(jiShiEntiy);
    }

    public void updateJishiDetailByquery(JiShiDetail jiShiDetail) {
        shipInfoDetailMapper.updateJishiDetail(jiShiDetail);
    }

    public List<JiShiEntiy> selectJishientiyByUserid(int userid) {
        return shipInfoDetailMapper.selectJishientiyByUserid(userid);
    }

    public void updateShipownerNameByShipid(int shipid, String shipownername) {
        shipInfoDetailMapper.updateShipownerNameByShipid(shipid,shipownername);
    }

    public DeviceInfo selectDeviceInfoByShipid(int shipid) {
        return shipInfoDetailMapper.selectDeviceInfoByShipid(shipid);
    }
}
