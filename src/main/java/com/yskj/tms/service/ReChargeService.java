package com.yskj.tms.service;

import com.yskj.tms.entity.ReCharge;
import com.yskj.tms.entity.ReChargeDetail;
import com.yskj.tms.mapper.ReChargeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/23.
 */
@Service
public class ReChargeService {
    @Autowired
    private ReChargeMapper reChargeMapper;

    public void addReCharge(ReCharge reCharge) {
        reChargeMapper.addReCharge(reCharge);
    }



    public ReCharge selectReChargeByReordernumber(String reordernumber) {
       return reChargeMapper.selectReChargeByReordernumber(reordernumber);
    }

    public void updateReChargeRestatus(Map map) {
        reChargeMapper.updateReChargeRestatus(map);
    }

    public ReChargeDetail selectReChargeDetailByUserid(int userid) {
        return reChargeMapper.selectReChargeDetailByUserid(userid);
    }

    public void saveReChargeDetail(ReChargeDetail reChargeDetail1) {
        reChargeMapper.saveReChargeDetail(reChargeDetail1);
    }

    public void updateReChargeDetail(ReChargeDetail reChargeDetail) {
        reChargeMapper.updateReChargeDetail(reChargeDetail);
    }

    public List<ReCharge> selectReChargeByRestatusAndUserid(Map map) {
        return reChargeMapper.selectReChargeByRestatusAndUserid(map);
    }

    public void updateReChargeDetailByUserid(Map map) {
        reChargeMapper.updateReChargeDetailByUserid(map);
    }
}
