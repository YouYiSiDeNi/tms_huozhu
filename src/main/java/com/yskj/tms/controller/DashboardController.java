package com.yskj.tms.controller;

import com.yskj.tms.common.UsersById;
import com.yskj.tms.service.DashboardService;
import com.yskj.tms.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 御天川 on 2018/5/26.
 */
@Controller
public class DashboardController {
    @Autowired
    private DashboardService dashboardService;

    private UsersById users = new UsersById();

    @RequestMapping(value = "/goodsType.do")
    @ResponseBody
    public void goodsTypeDashboard(HttpServletRequest request,@RequestParam(value = "selecttime") String selecttime,HttpServletResponse response) throws Exception{
        users.getUserid(request);
        Map map = new HashMap();
        map.put("userid",users.getUserid(request));

        Date starttime = new SimpleDateFormat("yyyy-MM").parse(selecttime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(starttime);
        calendar.add(Calendar.MONTH, 1);
        Date endtime = calendar.getTime();
        map.put("starttime",new Timestamp(starttime.getTime()));
        map.put("endtime",new Timestamp(endtime.getTime()));

        List<Map> list = dashboardService.selectGoodsTypeSumton(map);
        response.setContentType("text/html;charset=utf-8");
        PrintWriter printWriter = response.getWriter();
        printWriter.print(StringUtil.toJson(list));
        printWriter.flush();
        printWriter.close();

    }

    @RequestMapping(value = "/shipTon.do")
    @ResponseBody
    public void shipTonDashboard(HttpServletRequest request,@RequestParam(value = "select") String select,HttpServletResponse response) throws Exception{
        users.getUserid(request);
        Map map = new HashMap();
        map.put("userid",users.getUserid(request));

        Date date=new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        String  datef = sdf.format(date);
        if(select.equals("本月")){
            Date starttime = new SimpleDateFormat("yyyy-MM").parse(datef);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(starttime);
            calendar.add(Calendar.MONTH, 1);
            Date endtime = calendar.getTime();
            map.put("starttime",new Timestamp(starttime.getTime()));
            map.put("endtime",new Timestamp(endtime.getTime()));
        }
        if(select.equals("半年")) {
            Date starttime = new SimpleDateFormat("yyyy-MM").parse(datef);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(starttime);
            calendar.add(Calendar.MONTH, 1);
            Date endtime = calendar.getTime();
            calendar.add(Calendar.MONTH, -7);
            starttime = calendar.getTime();
            map.put("starttime",new Timestamp(starttime.getTime()));
            map.put("endtime",new Timestamp(endtime.getTime()));

        }
        if(select.equals("全年")) {
            Date starttime = new SimpleDateFormat("yyyy-MM").parse(datef);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(starttime);
            calendar.add(Calendar.MONTH, 1);
            Date endtime = calendar.getTime();
            calendar.add(Calendar.MONTH, -13);
            starttime = calendar.getTime();
            map.put("starttime",new Timestamp(starttime.getTime()));
            map.put("endtime",new Timestamp(endtime.getTime()));

        }
        List<Map> list = dashboardService.selectShipSumton(map);
        response.setContentType("text/html;charset=utf-8");
        PrintWriter printWriter = response.getWriter();
        printWriter.print(StringUtil.toJson(list));
        printWriter.flush();
        printWriter.close();

    }

    @RequestMapping(value = "/tonChange.do")
    @ResponseBody
    public void tonChangeDashboard(HttpServletRequest request,@RequestParam(value = "tonselect") String tonselect,HttpServletResponse response) throws Exception{
        users.getUserid(request);
        Map map = new HashMap();
        map.put("userid",users.getUserid(request));

        map.put("tonselect",tonselect);


        List<Map> list = dashboardService.selectTonChange(map);
        response.setContentType("text/html;charset=utf-8");
        PrintWriter printWriter = response.getWriter();
        printWriter.print(StringUtil.toJson(list));
        printWriter.flush();
        printWriter.close();

    }


}
