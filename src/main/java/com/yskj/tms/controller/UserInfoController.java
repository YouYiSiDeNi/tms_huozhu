package com.yskj.tms.controller;

import com.dong.hui.webservice.ShipInfoCXF;
import com.dong.hui.webservice.SysShipInfoEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yskj.tms.entity.UserInfo;
import com.yskj.tms.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.JacksonObjectMapperFactoryBean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Administrator on 2018/4/18.
 */
@Controller

public class UserInfoController {
    @Autowired
    private UserInfoService userInfoService;

    @RequestMapping("/init.do")
    public ModelAndView login() throws Exception {
        return new ModelAndView("/login/login");
    }
//    @RequestMapping("/index.do")
//    public ModelAndView index() throws Exception {
//        return new ModelAndView("/login/index");
//    }

    @RequestMapping("/welcome.do")
    public ModelAndView welcome() throws Exception {
        return new ModelAndView("/login/welcom");
    }

    @RequestMapping("/login.do")
    @ResponseBody
    public String login(@RequestParam(value = "accountname") String adminnumber, @RequestParam(value = "password") String password, HttpServletRequest request) throws Exception {

        UserInfo userInfo = userInfoService.findByAdminAndPasswordQuery(adminnumber,password);
        if (userInfo==null){
            return  new ObjectMapper().writeValueAsString(false);
        }else{
            HttpSession session = request.getSession();
            session.setAttribute("userid",userInfo.getUserid());
            session.setAttribute("username",userInfo.getUsername());
            session.setAttribute("userpassword",userInfo.getPassword());
            return new ObjectMapper().writeValueAsString(true);
        }
    }

    @RequestMapping("/updatePassword.do")
    @ResponseBody
    public String updatePasswordByQuery(HttpServletRequest request,@RequestParam(value = "password") String password,@RequestParam(value = "newpwd") String newpwd) throws Exception{
        HttpSession session = request.getSession();
        String msg = "";
        String username = session.getAttribute("username").toString();
        if (username !=null){
            String pwd = userInfoService.selectPasswordByUsername(username);
            if (pwd.equals(password)){
                userInfoService.updatePassword(newpwd,username);
                msg="密码修改成功";
            }else {
                msg="您输入的密码不正确";
                return new ObjectMapper().writeValueAsString(msg);
            }
        }else{
            msg ="请重新登录！";
            return new ObjectMapper().writeValueAsString(msg);
        }
        return new ObjectMapper().writeValueAsString(msg);
    }

   @RequestMapping("/resetPassword.do")
    @ResponseBody
    public String resetPasswordByUsername(@RequestParam(value = "username")String username) throws Exception{
        UserInfo userInfo = userInfoService.selectUserinfoByUsername(username);
        String msg = "";
        if (userInfo == null){
            msg = "您输入的账号不正确,请重新输入";
        }else{
            String pwd = "123456";
            userInfoService.updatePassword(pwd,username);
            msg ="密码重置成功！";
        }
        return new ObjectMapper().writeValueAsString(msg);
   }
}

