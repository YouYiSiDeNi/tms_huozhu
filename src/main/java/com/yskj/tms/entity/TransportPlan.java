package com.yskj.tms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/4/12.
 */
public class TransportPlan  implements Serializable{

    private static final long serialVersionUID = 1L;
    private int transportid ;
    //运单号
    private String ordernumber;
    //货物名称
    private String goodsname;
    //开始时间
    private Timestamp startdate;
    //结束时间
    private Timestamp enddate;
    //计划创建时间
    private Timestamp creationdate;
    //出发港口
    private String originating;
    //到达港口
    private String destination;
    //总吨位
    private Double totaltonnage;
    private int userid;
    private int goodsid;
    //负责人
    private String scheduler;
    //计划流水号
    private String tanordernumber;
    //状态
    private int stats;

    public Timestamp getStartdate() {
        return startdate;
    }

    public void setStartdate(Timestamp startdate) {
        this.startdate = startdate;
    }

    public Timestamp getEnddate() {
        return enddate;
    }

    public void setEnddate(Timestamp enddate) {
        this.enddate = enddate;
    }

    private List<ShipInfo> shipInfoList;

    public int getStats() {
        return stats;
    }

    public void setStats(int stats) {
        this.stats = stats;
    }

    public String getTanordernumber() {
        return tanordernumber;
    }

    public void setTanordernumber(String tanordernumber) {
        this.tanordernumber = tanordernumber;
    }

    public Timestamp getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Timestamp creationdate) {
        this.creationdate = creationdate;
    }

    public List<ShipInfo> getShipInfoList() {

        return shipInfoList;
    }

    public void setShipInfoList(List<ShipInfo> shipInfoList) {
        this.shipInfoList = shipInfoList;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    //备注
    private String remarkes;

    public String getRemarkes() {
        return remarkes;
    }

    public void setRemarkes(String remarkes) {
        this.remarkes = remarkes;
    }

    private GoodsInfo goodsInfo;

    public int getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(int goodsid) {
        this.goodsid = goodsid;
    }

    public GoodsInfo getGoodsInfo() {
        return goodsInfo;
    }

    public void setGoodsInfo(GoodsInfo goodsInfo) {
        this.goodsInfo = goodsInfo;
    }

    public int getTransportid() {
        return transportid;
    }

    public void setTransportid(int transportid) {
        this.transportid = transportid;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getOriginating() {
        return originating;
    }

    public void setOriginating(String originating) {
        this.originating = originating;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Double getTotaltonnage() {
        return totaltonnage;
    }

    public void setTotaltonnage(Double totaltonnage) {
        this.totaltonnage = totaltonnage;
    }

    public String getScheduler() {
        return scheduler;
    }

    public void setScheduler(String scheduler) {
        this.scheduler = scheduler;
    }

}
