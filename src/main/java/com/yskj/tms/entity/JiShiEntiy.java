package com.yskj.tms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2018/6/5.
 */
public class JiShiEntiy implements Serializable {

    private static final long serialVersionUID = 1L;
    private int jid;
    private int shipid;
    private int userid;
    //开始计时时间
    private Timestamp begindt;
    //观看视频秒数
    private int jsecond;
    //结束计时时间
    private Timestamp enddate;

    private ShipInfo shipInfo;

    public ShipInfo getShipInfo() {
        return shipInfo;
    }

    public void setShipInfo(ShipInfo shipInfo) {
        this.shipInfo = shipInfo;
    }

    public int getJid() {
        return jid;
    }

    public void setJid(int jid) {
        this.jid = jid;
    }

    public int getShipid() {
        return shipid;
    }

    public void setShipid(int shipid) {
        this.shipid = shipid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public Timestamp getBegindt() {
        return begindt;
    }

    public void setBegindt(Timestamp begindt) {
        this.begindt = begindt;
    }


    public Timestamp getEnddate() {
        return enddate;
    }

    public void setEnddate(Timestamp enddate) {
        this.enddate = enddate;
    }

    public int getJsecond() {
        return jsecond;
    }

    public void setJsecond(int jsecond) {
        this.jsecond = jsecond;
    }
}
