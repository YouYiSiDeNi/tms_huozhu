package com.yskj.tms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2018/6/5.
 */
public class JiShiDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    private int jishiid;
    private int shipid;
    private int userid;
    //开始计时时间
    private Timestamp begindt;
    //秒数
    private int jsecond;

    public int getJishiid() {
        return jishiid;
    }

    public void setJishiid(int jishiid) {
        this.jishiid = jishiid;
    }

    public int getShipid() {
        return shipid;
    }

    public void setShipid(int shipid) {
        this.shipid = shipid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public Timestamp getBegindt() {
        return begindt;
    }

    public void setBegindt(Timestamp begindt) {
        this.begindt = begindt;
    }

    public int getJsecond() {
        return jsecond;
    }

    public void setJsecond(int jsecond) {
        this.jsecond = jsecond;
    }
}
