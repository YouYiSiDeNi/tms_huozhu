package com.yskj.tms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Administrator on 2018/5/13.
 */
public class Trackhis implements Serializable {
    private static final long serialVersionUID = 1L;

    private int trackhisid;
    private int shipid;
    private int isemtiy;
    private String longitude;
    private String latitude;
    private Timestamp adddate;

    public int getTrackhisid() {
        return trackhisid;
    }

    public void setTrackhisid(int trackhisid) {
        this.trackhisid = trackhisid;
    }

    public int getShipid() {
        return shipid;
    }

    public void setShipid(int shipid) {
        this.shipid = shipid;
    }

    public int getIsemtiy() {
        return isemtiy;
    }

    public void setIsemtiy(int isemtiy) {
        this.isemtiy = isemtiy;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Date getAdddate() {
        return adddate;
    }

    public void setAdddate(Timestamp adddate) {
        this.adddate = adddate;
    }
}
