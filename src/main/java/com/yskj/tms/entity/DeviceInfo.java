package com.yskj.tms.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Administrator on 2018/4/12.
 */
public class DeviceInfo implements Serializable{

    private static final long serialVersionUID = 1L;
    private int deviceid;
    //设备主序列号
    private String devicenumber;
    //判断设备是否在线
    private int isonline;
    //经度
    private String longitude;
    //纬度
    private String latitude;
    //最后上传时间
    private Timestamp uptime;
    //实时位置
    private String cname;
    //设备辅序列号
    private String devicenumbertwo;
    //倾斜角
    private Double rollanglel;
    //航速
    private String shipspeed;

    private String devicenum;

    public String getDevicenum() {
        return devicenum;
    }

    public void setDevicenum(String devicenum) {
        this.devicenum = devicenum;
    }

    public Timestamp getUptime() {
        return uptime;
    }

    public void setUptime(Timestamp uptime) {
        this.uptime = uptime;
    }

    public Double getRollanglel() {
        return rollanglel;
    }

    public void setRollanglel(Double rollanglel) {
        this.rollanglel = rollanglel;
    }

    public String getShipspeed() {
        return shipspeed;
    }

    public void setShipspeed(String shipspeed) {
        this.shipspeed = shipspeed;
    }

    public String getDevicenumbertwo() {
        return devicenumbertwo;
    }

    public void setDevicenumbertwo(String devicenumbertwo) {
        this.devicenumbertwo = devicenumbertwo;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    private int shipid;

    public int getShipid() {
        return shipid;
    }

    public void setShipid(int shipid) {
        this.shipid = shipid;
    }

    public int getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(int deviceid) {
        this.deviceid = deviceid;
    }

    public String getDevicenumber() {
        return devicenumber;
    }

    public void setDevicenumber(String devicenumber) {
        this.devicenumber = devicenumber;
    }

    public int getIsonline() {
        return isonline;
    }

    public void setIsonline(int isonline) {
        this.isonline = isonline;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
