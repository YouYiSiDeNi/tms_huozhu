package com.yskj.tms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2018/5/4.
 */
public class ShipAndTransport implements Serializable {
    private static final long serialVersionUID = 1L;

    private int stid;
    private int shipid;
    private int transportid;
    //始发港到港时间
    private Timestamp origporttime;
    //抵达港到港时间
    private Timestamp arriveporttime;
    //装满时间
    private Timestamp fulltime;
    //卸空时间
    private Timestamp unloadtime;
    //受载吨位
    private double loadton;
    //实收吨位
    private double realton;
    //调度人
    private String agentname;
    //计划吨位
    private double planton;
    //开始卸货时间
    private Timestamp startunloadingtime;
    //开始装货时间
    private Timestamp startloadingtime;

    private ShipInfo shipInfo;

    private TransportPlan transportPlan;

    public TransportPlan getTransportPlan() {
        return transportPlan;
    }

    public void setTransportPlan(TransportPlan transportPlan) {
        this.transportPlan = transportPlan;
    }

    public ShipInfo getShipInfo() {
        return shipInfo;
    }

    public void setShipInfo(ShipInfo shipInfo) {
        this.shipInfo = shipInfo;
    }

    public double getPlanton() {
        return planton;
    }

    public void setPlanton(double planton) {
        this.planton = planton;
    }

    public Timestamp getStartunloadingtime() {
        return startunloadingtime;
    }

    public void setStartunloadingtime(Timestamp startunloadingtime) {
        this.startunloadingtime = startunloadingtime;
    }

    public Timestamp getStartloadingtime() {
        return startloadingtime;
    }

    public void setStartloadingtime(Timestamp startloadingtime) {
        this.startloadingtime = startloadingtime;
    }

    public String getAgentname() {
        return agentname;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname;
    }

    public int getStid() {
        return stid;
    }

    public void setStid(int stid) {
        this.stid = stid;
    }

    public int getShipid() {
        return shipid;
    }

    public void setShipid(int shipid) {
        this.shipid = shipid;
    }

    public int getTransportid() {
        return transportid;
    }

    public void setTransportid(int transportid) {
        this.transportid = transportid;
    }

    public Timestamp getOrigporttime() {
        return origporttime;
    }

    public void setOrigporttime(Timestamp origporttime) {
        this.origporttime = origporttime;
    }

    public Timestamp getArriveporttime() {
        return arriveporttime;
    }

    public void setArriveporttime(Timestamp arriveporttime) {
        this.arriveporttime = arriveporttime;
    }

    public Timestamp getFulltime() {
        return fulltime;
    }

    public void setFulltime(Timestamp fulltime) {
        this.fulltime = fulltime;
    }

    public Timestamp getUnloadtime() {
        return unloadtime;
    }

    public void setUnloadtime(Timestamp unloadtime) {
        this.unloadtime = unloadtime;
    }

    public double getLoadton() {
        return loadton;
    }

    public void setLoadton(double loadton) {
        this.loadton = loadton;
    }

    public double getRealton() {
        return realton;
    }

    public void setRealton(double realton) {
        this.realton = realton;
    }


}
