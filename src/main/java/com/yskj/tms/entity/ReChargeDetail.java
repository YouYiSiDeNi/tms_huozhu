package com.yskj.tms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2018/5/23.
 */
public class ReChargeDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    private int redetilid;
    //支付开始时间
    private Timestamp readdtime;
    //支付结束时间
    private Timestamp relasttime;
    //支付金额
    private double remoney;
    //支付类型
    private int rechargetype;
    private int userid;
    //时间
    private int resecond;

    public int getResecond() {
        return resecond;
    }

    public void setResecond(int resecond) {
        this.resecond = resecond;
    }

    public int getRedetilid() {
        return redetilid;
    }

    public void setRedetilid(int redetilid) {
        this.redetilid = redetilid;
    }

    public Timestamp getReaddtime() {
        return readdtime;
    }

    public void setReaddtime(Timestamp readdtime) {
        this.readdtime = readdtime;
    }

    public Timestamp getRelasttime() {
        return relasttime;
    }

    public void setRelasttime(Timestamp relasttime) {
        this.relasttime = relasttime;
    }

    public double getRemoney() {
        return remoney;
    }

    public void setRemoney(double remoney) {
        this.remoney = remoney;
    }

    public int getRechargetype() {
        return rechargetype;
    }

    public void setRechargetype(int rechargetype) {
        this.rechargetype = rechargetype;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

}
