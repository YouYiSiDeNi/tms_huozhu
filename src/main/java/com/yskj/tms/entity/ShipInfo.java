package com.yskj.tms.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/4/12.
 */

public class ShipInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private int shipid;
    //船名
    private String shipname;
    //船主姓名
    private String shipownername;
    //船主电话
    private String shipownerphone;
    //船长
    private double shiplong;
    //船宽
    private double shipwide;
    //船舱长
    private double cabinlong;
    //船舱宽
    private double cabinwide;
    //船舱深
    private double cabinheight;
    //是否有峰舱设备
    private int sealingdevice;
    private int isemtiy;
    //船舶吨位
    private double ton;
    //船舶类型
    private int shiptypeid;
    //状态
    private int stats;
    private String shipbid;
    //吃水深度
    private double loaddraft;

    private UserAndShip userAndShip;

    public UserAndShip getUserAndShip() {
        return userAndShip;
    }

    public void setUserAndShip(UserAndShip userAndShip) {
        this.userAndShip = userAndShip;
    }

    public double getLoaddraft() {
        return loaddraft;
    }

    public void setLoaddraft(double loaddraft) {
        this.loaddraft = loaddraft;
    }

    public double getShiplong() {
        return shiplong;
    }

    public void setShiplong(double shiplong) {
        this.shiplong = shiplong;
    }

    public double getShipwide() {
        return shipwide;
    }

    public void setShipwide(double shipwide) {
        this.shipwide = shipwide;
    }

    public String getShipbid() {
        return shipbid;
    }

    public void setShipbid(String shipbid) {
        this.shipbid = shipbid;
    }

    public int getStats() {
        return stats;
    }

    public void setStats(int stats) {
        this.stats = stats;
    }

    private ShipAndTransport shipAndTransport;

    public ShipAndTransport getShipAndTransport() {
        return shipAndTransport;
    }

    public void setShipAndTransport(ShipAndTransport shipAndTransport) {
        this.shipAndTransport = shipAndTransport;
    }

    public int getShiptypeid() {
        return shiptypeid;
    }

    public void setShiptypeid(int shiptypeid) {
        this.shiptypeid = shiptypeid;
    }

    public double getTon() {
        return ton;
    }

    public void setTon(double ton) {
        this.ton = ton;
    }

    private DeviceInfo deviceInfo;

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public int getShipid() {
        return shipid;
    }

    public void setShipid(int shipid) {
        this.shipid = shipid;
    }

    public String getShipname() {
        return shipname;
    }

    public void setShipname(String shipname) {
        this.shipname = shipname;
    }

    public String getShipownername() {
        return shipownername;
    }

    public void setShipownername(String shipownername) {
        this.shipownername = shipownername;
    }

    public String getShipownerphone() {
        return shipownerphone;
    }

    public void setShipownerphone(String shipownerphone) {
        this.shipownerphone = shipownerphone;
    }

    public double getCabinlong() {
        return cabinlong;
    }

    public void setCabinlong(double cabinlong) {
        this.cabinlong = cabinlong;
    }

    public double getCabinwide() {
        return cabinwide;
    }

    public void setCabinwide(double cabinwide) {
        this.cabinwide = cabinwide;
    }

    public double getCabinheight() {
        return cabinheight;
    }

    public void setCabinheight(double cabinheight) {
        this.cabinheight = cabinheight;
    }

    public int getSealingdevice() {
        return sealingdevice;
    }

    public void setSealingdevice(int sealingdevice) {
        this.sealingdevice = sealingdevice;
    }

    public int getIsemtiy() {
        return isemtiy;
    }

    public void setIsemtiy(int isemtiy) {
        this.isemtiy = isemtiy;
    }
}
