package com.yskj.tms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2018/5/22.
 */
public class ReCharge implements Serializable {

    private static final long serialVersionUID = 1L;

    private int rechargeid;
    private int userid;
    //支付金额
    private double remoney;
    //支付时间
    private Timestamp retime;
    //支付订单号
    private String reordernumber;
    //支付类型
    private int retype;
    //支付状态
    private int restatus;
    //购买的时间
    private int resecond;

    public int getResecond() {
        return resecond;
    }

    public void setResecond(int resecond) {
        this.resecond = resecond;
    }

    public String getReordernumber() {
        return reordernumber;
    }

    public void setReordernumber(String reordernumber) {
        this.reordernumber = reordernumber;
    }

    public int getRechargeid() {
        return rechargeid;
    }

    public void setRechargeid(int rechargeid) {
        this.rechargeid = rechargeid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public double getRemoney() {
        return remoney;
    }

    public void setRemoney(double remoney) {
        this.remoney = remoney;
    }

    public Timestamp getRetime() {
        return retime;
    }

    public void setRetime(Timestamp retime) {
        this.retime = retime;
    }

    public int getRetype() {
        return retype;
    }

    public void setRetype(int retype) {
        this.retype = retype;
    }

    public int getRestatus() {
        return restatus;
    }

    public void setRestatus(int restatus) {
        this.restatus = restatus;
    }
}
