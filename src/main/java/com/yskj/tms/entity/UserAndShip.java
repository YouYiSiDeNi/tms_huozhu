package com.yskj.tms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/6/14.
 */
public class UserAndShip implements Serializable {

    private static final long serialVersionUID = 1L;

    private int usid;
    private int shipid;
    private int userid;
    private int ustats;
    private Date addtime;
    public int getUsid() {
        return usid;
    }

    private ShipInfo shipInfo;
    private  UserInfo userInfo;

    public ShipInfo getShipInfo() {
        return shipInfo;
    }

    public void setShipInfo(ShipInfo shipInfo) {
        this.shipInfo = shipInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public void setUsid(int usid) {
        this.usid = usid;
    }

    public int getShipid() {
        return shipid;
    }

    public void setShipid(int shipid) {
        this.shipid = shipid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getUstats() {
        return ustats;
    }

    public void setUstats(int ustats) {
        this.ustats = ustats;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
