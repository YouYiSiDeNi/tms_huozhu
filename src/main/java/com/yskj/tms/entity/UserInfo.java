package com.yskj.tms.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/4/12.
 */
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private int userid;
    private String username;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private List<ShipInfo> shipInfosList;

    public List<ShipInfo> getShipInfosList() {
        return shipInfosList;
    }

    public void setShipInfosList(List<ShipInfo> shipInfosList) {
        this.shipInfosList = shipInfosList;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
