package com.yskj.tms.entity;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/5/3.
 */
public class GoodsInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private int goodsid;
    //货主姓名
    private String goodsownername;

    //货主电话
    private String goodsphone;

    public int getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(int goodsid) {
        this.goodsid = goodsid;
    }

    public String getGoodsownername() {
        return goodsownername;
    }

    public void setGoodsownername(String goodsownername) {
        this.goodsownername = goodsownername;
    }


    public String getGoodsphone() {
        return goodsphone;
    }

    public void setGoodsphone(String goodsphone) {
        this.goodsphone = goodsphone;
    }
}
