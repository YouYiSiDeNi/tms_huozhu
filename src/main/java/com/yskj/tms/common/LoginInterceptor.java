package com.yskj.tms.common;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2018/5/18.
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String url=request.getRequestURI();
        //判断url是否是公开地址(实际使用时将公开地址配置到配置文件中)
        if(url.indexOf("/login.do")>=0){
            //如果要进行登录提交，放行
            return true;
        }

        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String password = (String) session.getAttribute("userpassword");
        if (username != null && password !=null){
            return true;
        }else {
            request.getRequestDispatcher("/WEB-INF/jsp/login/login.jsp").forward(request, response);
            return false;
        }
    }
}
