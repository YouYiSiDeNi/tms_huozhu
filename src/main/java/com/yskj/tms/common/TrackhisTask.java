package com.yskj.tms.common;

import com.dong.hui.webservice.ShipInfoCXF;
import com.yskj.tms.service.ShipInfoDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * Created by Administrator on 2018/5/13.
 */

public class TrackhisTask {
    @Autowired
    private ShipInfoDetailService shipInfoDetailService;

    public void show() throws Exception{
        shipInfoDetailService.trackhisByQuery();
    }
}
