package com.yskj.tms.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2018/5/18.
 */
public class UsersById {
    public int getUserid(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Object userids = session.getAttribute("userid");
        int userid = 0;
        if (userids != null) {
            userid = (int) userids;
        }
        return userid;
    }
}
